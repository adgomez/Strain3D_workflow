% Unwraps harmonic phase volumes.  

%% housekeeping

clc
close all
clear all

% latest: basic function

%% analysis folder path

enable_folders

%% add required routines to path

% preprocessing
mfilesfolder = 'unwrapping';
path(path,mfilesfolder)

%% extract parameters

save_data_raw = PARAM.tag.raw_output;
save_data_nii = PARAM.tag.nii_output;

phase_unwrap = PARAM.tag.unwrap;
phase_rewrap = PARAM.tag.rewrap;
rewrap_f = PARAM.tag.rewrap_f;

show_data = false;

unwrap_data = false;

%% load references

if unwrap_data
    load([filt_folder '/' 'unwrap_refs'])
    steps_n = size(ref_ids,1);
else
    steps_n = PARAM.tag.time_steps
end

%% unwrap

%load([filt_folder '/' 'int_S_vars']) % synth data for checking

for cnt_t = 1:steps_n
    
    cnt_t
    
    % load unwrapped data
    load([filt_folder '/' 'int_W_vars_' num2str(cnt_t) ])

    if unwrap_data
        % faster but possibly inaccurate unwrapping code 
        HP1 = sunwrap3D(HP1,ref_ids(cnt_t)); disp('1')
        HP2 = sunwrap3D(HP2,ref_ids(cnt_t)); disp('2')
        HP3 = sunwrap3D(HP3,ref_ids(cnt_t)); disp('3')

        % save unwrapped data
        m_folder = filt_folder;
        save([m_folder '/' 'int_U_vars_' num2str(cnt_t)],...
                   'Q1','Q2','Q3','Q_res', ...
                   'QM1','QM2','QM3', ...     
                   'HP1','HP2','HP3', ...
                   'HM1','HM2','HM3'); 
        disp(['Urwrapped data saved as: ' 'int_U_vars_' num2str(cnt_t)])

        % re-wrap
        if phase_rewrap
            ref_ph = [HP1(ref_ids(cnt_t)) HP2(ref_ids(cnt_t)) HP3(ref_ids(cnt_t))];
            HP1 = rewrap_f*HP1; HP1 = HP1 - HP1(ref_ids(cnt_t)) + ref_ph(1); HP1 = wrap(HP1);
            HP2 = rewrap_f*HP2; HP2 = HP2 - HP2(ref_ids(cnt_t)) + ref_ph(2); HP2 = wrap(HP2);
            HP3 = rewrap_f*HP3; HP3 = HP3 - HP3(ref_ids(cnt_t)) + ref_ph(3); HP3 = wrap(HP3);
        end
    end
    
    % *** output result image
    if show_data
    % if (cnt_t == 2)
        filtering_view_v2
        disp('****** STOPPED FOR PREVIEW ******')
        return
    end

    % *** save unwrapped data
    % mat format for 3DHARP
    if true

        m_folder = filt_folder;
        save([m_folder '/' 'int_H_vars_' num2str(cnt_t)],...
                   'Q1','Q2','Q3','Q_res', ...
                   'QM1','QM2','QM3', ...     
                   'HP1','HP2','HP3', ...
                   'HM1','HM2','HM3'); 

        disp(['HFE saved as: ' 'int_H_vars_' num2str(cnt_t)])
    end

    % *.raw format for HARP-FE
    if save_data_raw

        raw_folder = filt_folder;
        for cnt_d = 1:3
            % define tagged images
            eval_line = ['TEMP = QM' num2str(cnt_d) ';' ];
            eval(eval_line)

            % save
            disp(['RAW saved as:' 'TAG' num2str(cnt_d) 't' num2str(cnt_t) '.raw'])
            s_filename = [raw_folder '/' 'TAG' num2str(cnt_d) 't' num2str(cnt_t) '.raw'];
            fid = fopen(s_filename,'w');
            fwrite(fid,uint8(TEMP),'uint8');
            fclose(fid); 

            clear TEMP

            % *** define phase images 
            eval_line = ['TEMP = HP' num2str(cnt_d) ';' ];
            eval(eval_line)

            % adjust to 8-bit range (waiting FEBIO fix for better range)
            TEMP = TEMP - 4*(pi/256); TEMP = (TEMP/pi)*128 + 128;

            disp(['RAW saved as:' 'PHI' num2str(cnt_d) 't' num2str(cnt_t) '.raw'])
            s_filename = [raw_folder '/' 'PHI' num2str(cnt_d) 't' num2str(cnt_t) '.raw'];
            fid = fopen(s_filename,'w');
            fwrite(fid,uint8(TEMP),'uint8');
            fclose(fid); 

            clear TEMP
        end
    end

    % *.ni format for PIVIRA
    if save_data_nii

        out_folder = filt_folder;
        disp(['saving as: ' out_folder '/' 'HM_' num2str(cnt_t) '.nii'])

        % save magnitude image
        temp = (HM1 + HM2 + HM3)/3;
        save_nii(make_nii(temp/max(temp(:))),[out_folder '/' 'HM_' num2str(cnt_t) '.nii']);

        % save phase images
        save_nii(make_nii(HP1),[out_folder '/' 'HP1_' num2str(cnt_t) '.nii']);
        save_nii(make_nii(HP2),[out_folder '/' 'HP2_' num2str(cnt_t) '.nii']);
        save_nii(make_nii(HP3),[out_folder '/' 'HP3_' num2str(cnt_t) '.nii']);

    end
end
% Performs HARP filter in tagged volumes.

%% housekeeping

clc
% close all force
clear all

% latest: harmonic phase filtering 
% NOTE: using implicit directions 

%% analysis folder path

enable_folders

%% add required routines to path

% preprocessing
mfilesfolder = 'HARP_filter';
path(path,mfilesfolder)

%% extract parameters

tag_sp = PARAM.tag.filt_sp; % tag spacing in px in each direction
steps_n = PARAM.tag.time_steps; % total time steps

%% load data and define directions

% load image variables
im_folder = interp_folder;

% display and storage controls
show_data = false;
save_data_m = true;

%% filter

H = waitbar(0,'Filtering ...');
% pick a time point
for cnt_t = 1:steps_n
    
    % load all directions
    load([im_folder '/' 'TAG1_H_' num2str(cnt_t)]);
    load([im_folder '/' 'TAG2_H_' num2str(cnt_t)]);
    load([im_folder '/' 'TAG3_H_' num2str(cnt_t)]);

    % initialization
    if (cnt_t == 1)
        % *** calculate resolution and size
        Q_res = [Q1(2,1,1) Q2(1,2,1) Q3(1,1,2)] - [Q1(1,1,1) Q2(1,1,1) Q3(1,1,1)];
        Q_dim = size(QM1);
        Q_box = [min([Q1(:) Q2(:) Q3(:)])
                 max([Q1(:) Q2(:) Q3(:)])];

        % *** generate frequency domain
        % approximate frequency bin size
        F_s = 0.5./Q_res;
        F_f = F_s./size(Q1);

        % get freq vectors
        f1 = 2*freq_vec_v1(F_f(1),Q_dim(1));
        f2 = 2*freq_vec_v1(F_f(2),Q_dim(2));
        f3 = 2*freq_vec_v1(F_f(3),Q_dim(3));

        % define frew volumes
        [F1,F2,F3] = ndgrid(f1,f2,f3);

        % ***  build filters
        % find location of spectral peaks 
        [hp1,lp11,lp12] = get_HARP_peak_v2(F1,F2,F3,QM1,[1 tag_sp(1)]);
        [hp2,lp21,lp22] = get_HARP_peak_v2(F1,F2,F3,QM2,[2 tag_sp(2)]);
        [hp3,lp31,lp32] = get_HARP_peak_v2(F1,F2,F3,QM3,[3 tag_sp(3)]);

        % generate filters
        options.scale = 0.5;
        
        options.type = 'slab'; % 'slab'; % 'hi-pass'/ 'cylindrical' / 'slab'
        options.radius = 1.0*(2*F_s./tag_sp); % 0 / 0.5*(2*F_s./tag_sp) / 1.0*(2*F_s./tag_sp)' 
        options.direction = 1; % 1/ 3 / 1 (rot) 
        options.edges = 1;
        options.center = lp11; F11 = get_filter_v3(F1,F2,F3,options);
        options.center = lp12; F12 = get_filter_v3(F1,F2,F3,options);
        
        options.type = 'slab'; % 'cylindrical'
        options.radius = 1.0*(2*F_s./tag_sp);
        options.direction = 2; % 3 (rot)
        options.edges = 1;
        options.center = lp21; F21 = get_filter_v3(F1,F2,F3,options);
        options.center = lp22; F22 = get_filter_v3(F1,F2,F3,options);

        options.type = 'slab'; % 'cylindrical'
        options.radius = 1.0*(2*F_s./tag_sp);
        options.direction = 3; % 1 (rot)
        options.edges = 1;
        options.center = lp31; F31 = get_filter_v3(F1,F2,F3,options);
        options.center = lp32; F32 = get_filter_v3(F1,F2,F3,options);
    end

    % *** filter data
    % calculate fft
    ft1 = fftn(QM1); ft2 = fftn(QM2); ft3 = fftn(QM3);

    % apply filters 
    ft11 = ifftn( ft1.*ifftshift(F11) );
    ft21 = ifftn( ft2.*ifftshift(F21) );
    ft31 = ifftn( ft3.*ifftshift(F31) );
    ft12 = ifftn( ft1.*ifftshift(F12) );
    ft22 = ifftn( ft2.*ifftshift(F22) );
    ft32 = ifftn( ft3.*ifftshift(F32) );

    % HARP images
    HP1 = angle(ft11 + conj(ft12)); 
    HP2 = angle(ft21 + conj(ft22)); 
    HP3 = angle(ft31 + conj(ft32)); 

    HM1 = 0.5*(abs(ft11) + abs(ft12)); 
    HM2 = 0.5*(abs(ft21) + abs(ft22)); 
    HM3 = 0.5*(abs(ft31) + abs(ft32)); 

    if show_data
        filtering_view_v2, return
    end
    
    % save wrapped data
    m_folder = filt_folder;
    save([m_folder '/' 'int_W_vars_' num2str(cnt_t)],...
                   'Q1','Q2','Q3','Q_res', ...
                   'QM1','QM2','QM3', ...     
                   'HP1','HP2','HP3', ...
                   'HM1','HM2','HM3'); 
       
    disp(['Wrapped data saved as: ' 'int_W_vars_' num2str(cnt_t)])
    
    % *** synth data
    if (cnt_t == 1)
        
        % tag definition
        SPAMM = 1;
        if SPAMM
            % SPAMM simulation
            TAG1 = 0.5*( 1 + cos(Q1*(2*pi)/tag_sp(1)) );
            TAG2 = 0.5*( 1 + cos(Q2*(2*pi)/tag_sp(2)) );
            TAG3 = 0.5*( 1 + cos(Q3*(2*pi)/tag_sp(2)) );
        else
            % CSPAM simulation
            TAG1 = cos(Q1*(2*pi)/tag_sp(1));
            TAG2 = cos(Q2*(2*pi)/tag_sp(2));
            TAG3 = cos(Q3*(2*pi)/tag_sp(2));
        end
        
        % *** filter synth data
        % calculate fft
        ft1 = fftn(HM1.*TAG1); ft2 = fftn(HM2.*TAG2); ft3 = fftn(HM3.*TAG3);

        % apply filters 
        ft11 = ifftn( ft1.*ifftshift(F11) );
        ft21 = ifftn( ft2.*ifftshift(F21) );
        ft31 = ifftn( ft3.*ifftshift(F31) );
        ft12 = ifftn( ft1.*ifftshift(F12) );
        ft22 = ifftn( ft2.*ifftshift(F22) );
        ft32 = ifftn( ft3.*ifftshift(F32) );

        % HARP images
        SHP1 = angle(ft11 + conj(ft12)); 
        SHP2 = angle(ft21 + conj(ft22)); 
        SHP3 = angle(ft31 + conj(ft32)); 

        % save wrapped data
        m_folder = filt_folder;
        save([m_folder '/' 'int_S_vars'],'SHP1','SHP2','SHP3')
        disp(['Synthetic data saved as: ' 'int_S_vars'])

    end
    % update
    waitbar(cnt_t/steps_n,H)
end
close(H)
    
%% remove filtering library from path
% filtering_view_v2 % (vis)
% return
rmpath(mfilesfolder)
















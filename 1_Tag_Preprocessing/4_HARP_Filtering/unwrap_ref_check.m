% checks the location of an initial reference and obtains the
% reference for subsequent times. use "check origin" first. Then, track
% below and save the reference points, or IDS. Tracking of the reference
% points should be good for 4 decimal places.

% TODO: use centroid as reference to automate things

%% housekeeping

clc
close all
clear all

%% analysis folder path

enable_folders

%% reference location (must be near center of rotation)

% ref_nodes = [1 -9 -18 -40
%              2 1.5 33 -50];
ref_nodes = [1 -9 -18 -40];
% number of frames minus 1 (TODO: place in parameters struct)
t_steps = 19;
  
%% check origin

if false

    % load data
    for cnt_t = 1:t_steps
        cnt_t

        load([filt_folder '/' 'int_W_vars_' num2str(cnt_t) ])
        % load([interp_folder '/' 'TAG2_H_' num2str(cnt_t) ])
        
        if (cnt_t == 1)
            TMP = QM1 + QM2;
        else
            TMP = TMP + QM1 + QM2;
        end
        
    end
    
    figure
    hold on
    ortho_slice_v4(Q1,Q2,Q3,TMP)
    scatter3(ref_nodes(2),ref_nodes(3),ref_nodes(4),'*r')
    hold off
    view(90,90)
    return
end

%% add 3DHARP library to search path

HARPfolder = [engine_folder '/' '3_Tag_Tracking/3DHARP' '/' 'M_files'];
path(path,HARPfolder);

%% track seed over all timeponts
% TODO: absolute v local tracking

load([filt_folder '/' 'int_S_vars']) % synth data for checking

for cnt_t = 1:(t_steps-1)
    cnt_t
    % load *** images
    load([filt_folder '/' 'int_W_vars_' num2str(cnt_t)])
    PHI1 = (HP1); 
    PHI2 = (HP2);
    PHI3 = (HP3); 
    load([filt_folder '/' 'int_W_vars_' num2str(cnt_t + 1)])
    phi1 = (HP1); 
    phi2 = (HP2); 
    phi3 = (HP3); 
    
%     [~,Iy] = min(sum(abs([PHI1(:)-phi1(:) PHI2(:)-phi2(:) PHI3(:)-phi3(:)]),2));
%     ref_nodes = [1 Q1(Iy) Q2(Iy) Q3(Iy)]
    
    % *** initialize mesh
    if cnt_t == 1
        % refrence nodes
        nodes = ref_nodes;   
    else
        % refrence nodes
        Ip = 1:numel(nodes(:,1));
        nodes(Ip,2:4) = ref_nodes(Ip,2:4) + nod_u(Ip,:,cnt_t);
    end
    % *** perform HARP tracking 
    % construct 
    H3D = HARP3D_MPT_v4(Q1,Q2,Q3,PHI1,PHI2,PHI3,phi1,phi2,phi3);
    % check
    In = 1:numel(nodes(:,1));
    target = [H3D.FP1.winterp(nodes(In,2),nodes(In,3),nodes(In,4)), ...
              H3D.FP2.winterp(nodes(In,2),nodes(In,3),nodes(In,4)), ...
              H3D.FP3.winterp(nodes(In,2),nodes(In,3),nodes(In,4)),]
    % total HARP iterations
    itr = 10;
    % conventional tracking
    [xn(:,1),xn(:,2),xn(:,3)] = H3D.track(nodes(:,2),nodes(:,3),nodes(:,4),itr);
    % check
    def_template = [H3D.Fp1.winterp(xn(In,1),xn(In,2),xn(In,3)), ...
                    H3D.Fp2.winterp(xn(In,1),xn(In,2),xn(In,3)), ...
                    H3D.Fp3.winterp(xn(In,1),xn(In,2),xn(In,3)),]
    % transfer info
    nod_u(:,:,cnt_t + 1) = xn - ref_nodes(:,2:4);   
end


%% find reference points

% tracked locations
ref_trk = repmat(ref_nodes(:,2:4),[1 1 size(nod_u,3)]) + nod_u;

% tracked IDs
ref_ids = zeros(size(ref_trk,3),1);
for cnt_t = 1:size(ref_trk,3)
    [~,ref_ids(cnt_t)] = min( sum(([Q1(:) Q2(:) Q3(:)] - ...
        repmat(ref_trk(1,:,cnt_t),[numel(Q1(:)) 1])).^2,2).^0.5 );
  
% load([filt_folder '/' 'int_U_vars_' num2str(cnt_t)])
% [HP1(ref_ids(cnt_t)), ...
% HP2(ref_ids(cnt_t)), ...
% HP3(ref_ids(cnt_t))]  

% [Q1(ref_ids(cnt_t)), ...
% Q2(ref_ids(cnt_t)), ...
% Q3(ref_ids(cnt_t))]  
end

% save
save([filt_folder '/' 'unwrap_refs'],'ref_ids')

%% check

if false

    % load data
    for cnt_t = 1:t_steps
        cnt_t

        load([filt_folder '/' 'int_H_vars_' num2str(cnt_t) ])
        % load([interp_folder '/' 'TAG2_H_' num2str(cnt_t) ])
        
        if (cnt_t == 1)
            TMP = QM1 + QM2;
        else
            TMP = TMP + QM1 + QM2;
        end
        
    end
    
    figure
    hold on
    ortho_slice_v4(Q1,Q2,Q3,TMP)
    scatter3(ref_nodes(:,2),ref_nodes(:,3),ref_nodes(:,4),'*r')
    plot3(squeeze(ref_trk(:,1,:))', ...
          squeeze(ref_trk(:,2,:))', ...
          squeeze(ref_trk(:,3,:))','-s')
    hold off
    view(90,90)

end

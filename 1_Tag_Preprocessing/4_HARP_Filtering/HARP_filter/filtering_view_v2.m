
%% filtered image plots

    % view angles (spherical coor)
    view_v1 = [90 90];
    view_v2 = [90 90]; % [0 90];
    view_v3 = [90 0];
    
    scrsz = get(0,'ScreenSize');
    xp_offset = 150;
    figure('Position',[1 scrsz(4) scrsz(3) scrsz(4)].*[1 0.2 0.7 0.95] + xp_offset*[1 -1 1 -1])

    % *** tagged images (magnitude)
    subplot(351)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,QM1,view_v1)
        hold off
        title('Tagged Dir 1')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(356)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,QM2,view_v2)
        hold off
        title('Tagged Dir 2')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(3,5,11)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,QM3,view_v3)
        hold off
        title('Tagged Dir 3')    
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')

    % *** tagged images (freq.)
    subplot(352)
        hold on
        ortho_slice_v4(F1,F2,F3,log(abs(fftshift(ft1))),view_v1)
        scatter3(hp1(1),hp1(2),hp1(3),'*r')
        scatter3(lp11(1),lp11(2),lp11(3),'og')
        scatter3(lp12(1),lp12(2),lp12(3),'og')
        hold off
        title('Freq Dir 1')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    subplot(357)
        hold on
        ortho_slice_v4(F1,F2,F3,log(abs(fftshift(ft2))),view_v2)
        scatter3(hp2(1),hp2(2),hp2(3),'*r')
        scatter3(lp21(1),lp21(2),lp21(3),'og')
        scatter3(lp22(1),lp22(2),lp22(3),'og')
        hold off
        title('Freq Dir 2')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    subplot(3,5,12)
        hold on
        ortho_slice_v4(F1,F2,F3,log(abs(fftshift(ft3))),view_v3)
        scatter3(hp3(1),hp3(2),hp3(3),'*r')
        scatter3(lp31(1),lp31(2),lp31(3),'og')
        scatter3(lp32(1),lp32(2),lp32(3),'og')
        hold off
        title('Freq Dir 3')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')

    % *** filters
    subplot(353)
        hold on
        ortho_slice_v4(F1,F2,F3,F11 + F12,view_v1)
        scatter3(hp1(1),hp1(2),hp1(3),'*r')
        scatter3(lp11(1),lp11(2),lp11(3),'og')
        scatter3(lp12(1),lp12(2),lp12(3),'og')
        hold off
        title('Filter Dir 1')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    subplot(358)
        hold on
        ortho_slice_v4(F1,F2,F3,F21 + F22,view_v2)
        scatter3(hp2(1),hp2(2),hp2(3),'*r')
        scatter3(lp21(1),lp21(2),lp21(3),'og')
        scatter3(lp22(1),lp22(2),lp22(3),'og')
        hold off
        title('Filter Dir 2')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    subplot(3,5,13)
        hold on
        ortho_slice_v4(F1,F2,F3,F31 + F32,view_v3)
        scatter3(hp3(1),hp3(2),hp3(3),'*r')
        scatter3(lp31(1),lp31(2),lp31(3),'og')
        scatter3(lp32(1),lp32(2),lp32(3),'og')
        hold off
        title('Filter Dir 3')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    
    % *** harmonic phase images
    subplot(354)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,HP1,view_v1)
        hold off
        title('Phase Dir 1')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(359)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,HP2,view_v2)
        hold off
        title('Phase Dir 2')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(3,5,14)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,HP3,view_v3)
        hold off
        title('Phase Dir 3')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    
    % *** harmonic magnitude images
    subplot(355)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,HM1,view_v1)
        hold off
        title('Magnitude Dir 1')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(3,5,10)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,HM2,view_v2) 
        hold off
        title('Magnitude Dir 2')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(3,5,15)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,HM3,view_v3) 
        hold off
        title('Magnitude Dir 3')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')  
               
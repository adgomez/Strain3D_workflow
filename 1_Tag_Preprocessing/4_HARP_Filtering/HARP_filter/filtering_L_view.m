%% filtered image plots

    % view angles (spherical coor)
    view_v1 = [90 90];
    view_v2 = [90 90]; % [0 90];
    view_v3 = [90 0];
    
    scrsz = get(0,'ScreenSize');
    xp_offset = 150;
    figure('Position',[1 scrsz(4) scrsz(3) scrsz(4)].*[1 0.2 0.7 0.95] + xp_offset*[1 -1 1 -1])

    % *** tagged images (magnitude)
    subplot(341)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,QM1,view_v1)
        hold off
        title('Tagged Dir 1')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(345)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,QM2,view_v2)
        hold off
        title('Tagged Dir 2')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(3,4,9)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,QM3,view_v3)
        hold off
        title('Tagged Dir 3')    
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')

    % *** tagged images (freq.)
    subplot(342)
        hold on
        ortho_slice_v4(F1,F2,F3,log(abs(fftshift(fftn(QM1)))),view_v1)
        scatter3(hp1(1),hp1(2),hp1(3),'*r')
        scatter3(lp11(1),lp11(2),lp11(3),'og')
        scatter3(lp12(1),lp12(2),lp12(3),'og')
        hold off
        title('Freq Dir 1')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    subplot(346)
        hold on
        ortho_slice_v4(F1,F2,F3,log(abs(fftshift(fftn(QM2)))),view_v2)
        scatter3(hp2(1),hp2(2),hp2(3),'*r')
        scatter3(lp21(1),lp21(2),lp21(3),'og')
        scatter3(lp22(1),lp22(2),lp22(3),'og')
        hold off
        title('Freq Dir 2')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    subplot(3,4,10)
        hold on
        ortho_slice_v4(F1,F2,F3,log(abs(fftshift(fftn(QM3)))),view_v3)
        scatter3(hp3(1),hp3(2),hp3(3),'*r')
        scatter3(lp31(1),lp31(2),lp31(3),'og')
        scatter3(lp32(1),lp32(2),lp32(3),'og')
        hold off
        title('Freq Dir 3')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')

    % *** filters
    subplot(343)
        hold on
        ortho_slice_v4(F1,F2,F3,F11 + F12,view_v1)
        scatter3(hp1(1),hp1(2),hp1(3),'*r')
        scatter3(lp11(1),lp11(2),lp11(3),'og')
        scatter3(lp12(1),lp12(2),lp12(3),'og')
        hold off
        title('Filter Dir 1')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    subplot(347)
        hold on
        ortho_slice_v4(F1,F2,F3,F21 + F22,view_v2)
        scatter3(hp2(1),hp2(2),hp2(3),'*r')
        scatter3(lp21(1),lp21(2),lp21(3),'og')
        scatter3(lp22(1),lp22(2),lp22(3),'og')
        hold off
        title('Filter Dir 2')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    subplot(3,4,11)
        hold on
        ortho_slice_v4(F1,F2,F3,F31 + F32,view_v3)
        scatter3(hp3(1),hp3(2),hp3(3),'*r')
        scatter3(lp31(1),lp31(2),lp31(3),'og')
        scatter3(lp32(1),lp32(2),lp32(3),'og')
        hold off
        title('Filter Dir 3')
        xlabel('x (cy/len)'), ylabel('y (cy/len)'), zlabel('z (cy/len)')
    
    % *** magnitude images after tag removal
    subplot(344)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,QF1,view_v1)
        hold off
        title('Magnitude Dir 1')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(3,4,8)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,QF2,view_v2) 
        hold off
        title('Magnitude Dir 2')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')
    subplot(3,4,12)
        hold on
        ortho_slice_v4(Q1,Q2,Q3,QF3,view_v3) 
        hold off
        title('Magnitude Dir 3')
        xlabel('x (len)'), ylabel('y (len)'), zlabel('z (len)')  
               

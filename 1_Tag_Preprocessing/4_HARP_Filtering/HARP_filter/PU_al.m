function [UP,ref_p] = PU_al(WP,dir,ref_id)
% [UP,ref_p] = PU_al(WP,dir,ref_id) slice-wise phase unwrapping along a 
% given direction  
% 
%   INPUT
%   WP = wrapped volume
%   dir = slice direction
%   ref_id = index of reference phase
% 
%   OUTPUT
%   UP = unwrapped phase
%   ref_p = reference phase
% 
%   NOTE: This funciton will not correct phase!

    % save global reference phase
    ref_p = WP(ref_id);
    
    % get local indices for reference
    [ref_g(1),ref_g(2),ref_g(3)] = ind2sub(size(WP),ref_id);
    
    % use single block and default settings
    options.maxblocksize = Inf;
    options.Verbose = false;
    
    % initialize 2D loop
    UP = zeros(size(WP));
    
    % apply slice-wise unwrapping
    if (dir == 1)  
        for cnt_s = 1:size(WP,dir)   
            tmpW = permute(WP(cnt_s,:,:),[2 3 1]);
            tmpU = cunwrap(tmpW,options);           
            UP(cnt_s,:,:) = ipermute(tmpU - tmpU(ref_g(2),ref_g(3)) +  tmpW(ref_g(2),ref_g(3)),[2 3 1]);  
            clc, disp(['unwrapping ' num2str(100*cnt_s/size(WP,dir)) '% done'])
        end
    end       
    if (dir == 2)
        for cnt_s = 1:size(WP,dir)   
            tmpW = permute(WP(:,cnt_s,:),[1 3 2]);
            tmpU = cunwrap(tmpW,options);           
            UP(:,cnt_s,:) = ipermute(tmpU - tmpU(ref_g(1),ref_g(3)) +  tmpW(ref_g(3),ref_g(3)),[1 3 2]);  
            clc, disp(['unwrapping ' num2str(100*cnt_s/size(WP,dir)) '% done'])
        end
    end
    if (dir == 3)
        for cnt_s = 1:size(WP,dir) 
            tmp = cunwrap(WP(:,:,cnt_s), options);
            UP(:,:,cnt_s) = tmp - tmp(ref_g(1),ref_g(2)) +  WP(ref_g(1),ref_g(2),cnt_s);  
            clc, disp(['unwrapping ' num2str(100*cnt_s/size(WP,dir)) '% done'])
        end
    end
    
    % apply reference. See note above
    % UP = UP - UP(ref_id) + ref_p; 
end


function [hp_c,lp_c_1,lp_c_2] = get_HARP_peak_v1(S1,S2,S3,mg,emp_flag)
% [hp_c,lp_c_1,lp_c_2] = get_HARP_peak_v1(S1,S2,S3,mg,emp_flag)
% can find harmonic peaks automatically, calculate them based on 
% empirically determined tag spacing, or inherit them from a file (in 
% future versions) 
% 
% if emp_fag is [0,~], where ~ is any number, the peaks will be found 
% automatically. Alternatively,
% emp_flag carries the tag direction (tg_dir) and separation in pixels
% (tg_s), so emp_flag = [tg_dir tg_s] 

%% find location of main spectral peak

% calculate fft
ft = fftshift(fftn(mg));

% locate FOV
S_box = [min([S1(:) S2(:) S3(:)])
         max([S1(:) S2(:) S3(:)])];

% find main peak
[~,pc] = max(ft(:));
     
% hard hi-pass (it could use some edge softening)
hp_r = mean(diff(S_box,[],1))/12; 
hp_c = [mean(S1(pc)) mean(S2(pc)) mean(S3(pc))];
hp_F = ((S1 - hp_c(1)).^2 + (S2 - hp_c(2)).^2 + (S3 - hp_c(3)).^2) > hp_r.^2;

%% find secondary spectral peaks 

manual = (0<emp_flag(1));

if manual
    % *** empirically measured

    % check for increasing order
    Gn = [S1(2,1,1) - S1(1,1,1) S2(2,1,1) - S2(1,1,1) S3(2,1,1) - S3(1,1,1)
          S1(1,2,1) - S1(1,1,1) S2(1,2,1) - S2(1,1,1) S3(1,2,1) - S3(1,1,1)
          S1(1,1,2) - S1(1,1,1) S2(1,1,2) - S2(1,1,1) S3(1,1,2) - S3(1,1,1)];

    % calculate spatial resolution vector  
    S_res = max(abs(Gn),[],1);
    
    % calculate peak location
    dim = size(mg);
    dir = emp_flag(1);
    lp_c_1  = [0 0 0];
    lp_c_2  = [0 0 0];
    
    lp_c_1(dir) = S_res(dir)/emp_flag(2)*dim(dir);
    lp_c_2(dir) = -S_res(dir)/emp_flag(2)*dim(dir);
    
    lp_c_1 = lp_c_1 + hp_c;
    lp_c_2 = lp_c_2 + hp_c;
else

    % *** via maximum detection

    % general hi-pass filter in all images
    mg = log(abs(ft.*hp_F));

    % direction 1 low-pass filter
    [~,p1] = max(mg(:));
    lp_c_1 = [S1(p1) S2(p1) S3(p1)];
    lp1_v = lp_c_1 - hp_c;
    lp_c_2 = hp_c - lp1_v;

end
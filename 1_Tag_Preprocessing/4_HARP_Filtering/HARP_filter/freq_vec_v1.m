function f = freq_vec_v1(fs,N)
% f = freq_vec_v1(fs,N);
 
% clc
% close all
% clear all
% fs = 1;
% N = 10;

% generates a vector with N elements including 0
if mod(N,2)
    temp1 = (0:fs:fs*floor(0.5*N))';
    temp2 = (-fs*ceil(0.5*N-1):fs:-fs)';
else
    temp1 = (0:fs:fs*ceil(0.5*N - 1))';
    temp2 = (-fs*floor(0.5*N):fs:-fs)';
end

f = [temp2; temp1];


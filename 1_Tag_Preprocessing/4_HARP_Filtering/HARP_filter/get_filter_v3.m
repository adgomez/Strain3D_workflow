function F = get_filter_v3(F1,F2,F3,options)
% F = get_filter_v2(F1,F2,F3,options)

%% get options

type = options.type;

%% generate filter

switch type

    case 'hi-pass'
        % get paramters
        ctr = options.center;
       
        dir = options.direction;
        
        sc = options.scale; % 0.95
         
        % define filter
        if dir == 1
            if ctr(1)>=0
                F =  sc*ctr(1) < F1;
            else
                F =  sc*ctr(1) > F1;
            end
        elseif dir == 2
            if ctr(2)>=0
                F =  sc*ctr(2) < F2;
            else
                F =  sc*ctr(2) > F2;    
            end
        elseif dir == 3 
            if ctr(3)>=0
                F =  sc*ctr(3) < F3;
            else
                F =  sc*ctr(3) > F3;
            end
        else
            error('invalid cyclindrical direction')
        end
              
    case 'gaussian'
        % get paramters
        ctr = options.center;
        sgm = options.sigma;

        % define filter
        F = exp(-((F1-ctr(1)).^2/(2*sgm(1)^2) + ...
                  (F2-ctr(2)).^2/(2*sgm(2)^2) + ...
                  (F3-ctr(3)).^2/(2*sgm(3)^2)));
              
    case 'removal'
        % get paramters
        ctr = options.center;
        sgm = options.sigma;

        % define filter
        F = 1- exp(-((F1-ctr(1)).^2/(2*sgm(1)^2) + ...
                     (F2-ctr(2)).^2/(2*sgm(2)^2) + ...
                     (F3-ctr(3)).^2/(2*sgm(3)^2)));
                 
    case 'removal_low'
        % get paramters
        ctr = options.center;
        sgm = options.sigma;

        % define filter
        F = 1 - exp(-((F1-ctr(1)).^2/(2*sgm(1)^2) + ...
                      (F2-ctr(2)).^2/(2*sgm(2)^2) + ...
                      (F3-ctr(3)).^2/(2*sgm(3)^2))) ...
              - exp(-((F1-2*ctr(1)).^2/(2.5*sgm(1)^2) + ...
                      (F2-2*ctr(2)).^2/(2.5*sgm(2)^2) + ...
                      (F3-2*ctr(3)).^2/(2.5*sgm(3)^2)));
          
    case 'cylindrical_rot'
        % get paramters
        ctr = options.center;
        r = options.radius;
        dir = options.direction;
        
        % define filter
        if dir == 1
            F = (((F3-ctr(3))/r(3)).^2 + ...
                 ((F2-ctr(2))/r(2)).^2) < 1;
        elseif dir == 2
            F = (((F1-ctr(1))/r(1)).^2 + ...
                 ((F3-ctr(3))/r(3)).^2) < 1;
        elseif dir == 3  
            F = (((F1-ctr(1))/r(1)).^2 + ...
                 ((F2-ctr(2))/r(2)).^2) < 1;
        else
            error('invalid cyclindrical direction')
        end
    case 'cylindrical_lin'
        % get paramters
        ctr = options.center;
        r = options.radius;
        dir = options.direction;
        
        % define filter
        if dir == 1
            F = (((F3-ctr(3))/r(3)).^2 + ...
                 ((F2-ctr(2))/r(2)).^2) < 1;
        elseif dir == 2
            F = (((F1-ctr(1))/r(1)).^2 + ...
                 ((F3-ctr(3))/r(3)).^2) < 1;
        elseif dir == 3  
            F = (((F1-ctr(1))/r(1)).^2 + ...
                 ((F2-ctr(2))/r(2)).^2) < 1;
        else
            error('invalid cyclindrical direction')
        end
        
     case 'slab'
        % get paramters
        ctr = options.center;
        dir = options.direction;
        sc = options.scale; % 0.95
        r = options.radius;
         
        % define filter
        if dir == 1
            if ctr(1)>=0
                F =  (sc*ctr(1) < F1)&(F1 < (sc*ctr(1) + r(1)));
            else
                F =  (sc*ctr(1) > F1)&(F1 > (sc*ctr(1) - r(1)));
            end
        elseif dir == 2
            if ctr(2)>=0
                F =  (sc*ctr(2) < F2)&(F2 < (sc*ctr(2) + r(2)));
            else
                F =  (sc*ctr(2) > F2)&(F2 > (sc*ctr(2) - r(2)));    
            end
        elseif dir == 3 
            if ctr(3)>=0
                F =  (sc*ctr(3) < F3)&(F3 < (sc*ctr(3) + r(3)));
            else
                F =  (sc*ctr(3) > F3)&(F3 > (sc*ctr(3) - r(3)));
            end
        else
            error('invalid direction')
        end   
       
    case 'elliptical'    
        
        % get paramters
        ctr = options.center;
        r = options.radius;

        % define filter
        F = (((F1-ctr(1))/r(1)).^2 + ...
             ((F2-ctr(2))/r(2)).^2 + ...
             ((F3-ctr(3))/r(3)).^2 ) < 1;
        
    otherwise
        
        warning('using elliptical filter by default')
        
        % get paramters
        ctr = options.center;
        r = options.radius;

        % define filter
        F = (((F1-ctr(1))/r(1)).^2 + ...
             ((F2-ctr(2))/r(2)).^2 + ...
             ((F3-ctr(3))/r(3)).^2 ) < 1;
        
end

%% smoothing
%3
F = smooth_v1(F,3,0.35);


%% TODO: profile visualization


end

function [Um] = smooth_v1(U,smooth_size,sigma)
% [Um] = smooth_v1(U,smooth_size,sigma) gaussian smoothing

    % prepare smoothing weights
    xwgts = linspace(-1,1,2*smooth_size-1)';
    wgts = exp(-(xwgts/sigma).^2); wgts = wgts/sum(wgts);
    wgts_x = wgts; wgts_y = wgts'; wgts_z(1,1,:) =  wgts; 

    % smoothing u via comvolution
    IM_in = U;
    IM_in = convn(IM_in,wgts_x,'same');
    IM_in = convn(IM_in,wgts_y,'same');
    IM_in = convn(IM_in,wgts_z,'same');
    Um = IM_in;
    
end

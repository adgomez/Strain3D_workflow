function mg_c = interp_free_v1(X_c,Y_c,Z_c,MG_c,S1,S2,S3,int_method,ext_method)
% mg_int = interp_free_v1(X_c,Y_c,Z_c,MG_c,S1,S2,S3)
% direction-free interpolation. basically a gridded interpolant that
% figures out the permutations and order to automatically fulfill the 
% requirement for having data in NDGRID format. 

% version 1: basic functionality

% TODO: preserve input dimensionality
% TODO: add time loop

% ADG 2015-09-17

%% organize coordinates

% check for increasing order
Gn = [X_c(2,1,1) - X_c(1,1,1) Y_c(2,1,1) - Y_c(1,1,1) Z_c(2,1,1) - Z_c(1,1,1)
      X_c(1,2,1) - X_c(1,1,1) Y_c(1,2,1) - Y_c(1,1,1) Z_c(1,2,1) - Z_c(1,1,1)
      X_c(1,1,2) - X_c(1,1,1) Y_c(1,1,2) - Y_c(1,1,1) Z_c(1,1,2) - Z_c(1,1,1)];
  
if numel(find(Gn==0)) ~= 6
    disp('WARNING: lattice may be rotated')  
end

% detect increasing dimension
[~,p_order] = max(abs(Gn),[],1);

% make increasing
p_sign = sign(sum(Gn,1));

                
%% interpolation time loop

% intialize
dim =  size(MG_c);

if size(dim) == 3
    dim(4) = 1;
end

mg_c = S1*0;

% loop counter
H = waitbar(0,'Interpolating time steps ...');
for cnt_t = 1:dim(4); 

    MG1_int = MG_c(:,:,:,cnt_t);

    % create coordinate arrays
    cush = cat(4,X_c,Y_c,Z_c);
    cash = cat(4,S1,S2,S3);

    if (p_sign(1)<0)
        cush(:,:,:,1) = flipdim(cush(:,:,:,1),p_order(1));
        cash(:,:,:,1) = flipdim(cash(:,:,:,1),1);
    end
    if (p_sign(2)<0)
        cush(:,:,:,2) = flipdim(cush(:,:,:,2),p_order(2));
        cash(:,:,:,2) = flipdim(cash(:,:,:,2),2);
    end
    if (p_sign(3)<0)
        cush(:,:,:,3) = flipdim(cush(:,:,:,3),p_order(3));
        cash(:,:,:,3) = flipdim(cash(:,:,:,3),3);
    end

    % interpolate dynamic 1
    Fi = griddedInterpolant(cush(:,:,:,p_order(1)),cush(:,:,:,p_order(2)),cush(:,:,:,p_order(3)),MG1_int,int_method,ext_method);
    mg_int = Fi(cash(:,:,:,p_order(1)),cash(:,:,:,p_order(2)),cash(:,:,:,p_order(3)));

    % put away
    mg_c(:,:,:,cnt_t) = mg_int; 
    
    % update
    waitbar(cnt_t/dim(4),H)

end

close(H)

end

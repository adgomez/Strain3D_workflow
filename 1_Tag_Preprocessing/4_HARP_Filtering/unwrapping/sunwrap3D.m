function UP = sunwrap3D(WP,ref_i)

% this is an implementation of "Fast phase unwrapping algorithm for
% interferometric applications" by Schofield and Zhu
% OPTICS LETTERS / Vol. 28, No. 14 / July 15, 2003
    
% initialize volume
dimi = round(0.5*size(WP));
theta_j = padarray(WP,dimi,'symmetric');
% set up frequency coordinates
[P,Q,R] = ndgrid(freq_vec_v1(1,size(theta_j,1)), ...
                 freq_vec_v1(1,size(theta_j,2)), ...
                 freq_vec_v1(1,size(theta_j,3))); 
P = ifftshift(P);
Q = ifftshift(Q);
R = ifftshift(R);
% calcualte radius
S = (P.^2 + Q.^2 + R.^2);
% approximate unwrapped phase
FA = ifftn(  fftn( cos(theta_j).*ifftn(S.*fftn(sin(theta_j))) )./(S+1e-5)  );
FB = ifftn(  fftn( sin(theta_j).*ifftn(S.*fftn(cos(theta_j))) )./(S+1e-5)  );
theta_prime = (FA - FB);
%  iterate
iterate = true;
iter = 0;
while iterate   
    % calculate increment
    n = round((theta_prime - theta_j)/(2*pi));
    % unwrap
    theta_j = theta_j + 2*pi*n;
    % update iteration count
    iter = iter + 1;
    % check for convergence
    if (max(n(:)) < 1)
        iterate = false;
    end
    % check for max iterations
    if (100 < iter)
        iterate = false;
        warning('unwrapping reached max iterations without convergence')
    end
end
% extract
theta_j = theta_prime(dimi(1) + 1:dimi(1) + size(WP,1), ...
                      dimi(2) + 1:dimi(2) + size(WP,2), ...
                      dimi(3) + 1:dimi(3) + size(WP,3));
% apply reference
theta_j = theta_j - theta_j(ref_i) + WP(ref_i);
% output
UP = theta_j;

function f = freq_vec_v1(fs,N)
% f = freq_vec_v1(fs,N);
 
% clc
% close all
% clear all
% fs = 1;
% N = 10;

% generates a vector with N elements including 0
if mod(N,2)
    temp1 = (0:fs:fs*floor(0.5*N))';
    temp2 = (-fs*ceil(0.5*N-1):fs:-fs)';
else
    temp1 = (0:fs:fs*ceil(0.5*N - 1))';
    temp2 = (-fs*floor(0.5*N):fs:-fs)';
end

f = [temp2; temp1];

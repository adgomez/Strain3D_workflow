function [MG,mg,s,L] = reco_twix(image_data,t)


% get parameters
Col_n = numel(image_data(:,1,1,1,1));
Cha_n = numel(image_data(1,:,1,1,1));
Lin_n = numel(image_data(1,1,:,1,1));
Phs_n = numel(image_data(1,1,1,:,1));
Seg_n = numel(image_data(1,1,1,1,:));
% contiguous lines per phase
Cos_n = Lin_n/Seg_n;
% total lines per segment
Laq_n = Cos_n*Phs_n;

% fix time
s = permute(reshape(t',[4 Laq_n Seg_n]),[2 1 3]);
s = cat(1,s(1:Cos_n,:,:),s,s(end-Cos_n+1:end,:,:));
for cnt_s = 1:Seg_n
    s(:,1,cnt_s) = 1:numel(s(:,1,1));
end

%%

% initialize
int_data = zeros(Col_n,Lin_n,Laq_n);

    cnt_ch = 1;
    
    for cnt_l = 1:Lin_n
    
        % find instances
        Io = s(:,2,:) == cnt_l;
        Ii = find(sum(0<Io,3));
        Ik = find(sum(0<Io,1));

        % positions we have data for
        s_q = s(Ii,:,Ik);

        % positions we need data for
        s_i = (Cos_n+1:Cos_n+Laq_n)';

        % prepare frequency data
        V_q = permute(image_data(:,cnt_ch,cnt_l,s_q(:,3),Ik),[4 1 2 3]);

        % interpolation 
        int_data(:,cnt_l,:) = permute(interp1(s_q(:,1),V_q,s_i),[2 1]);

    end
    
    MG = permute(int_data,[3 1 2])*0;
    for cnt_t = 1:Laq_n
        MG(cnt_t,:,:) = fftshift(ifft2(ifftshift(int_data(:,:,cnt_t))));
    end
    
    
    mg = 0;
    
return

% view_share_twix(image_data,t,view_share)
% view sharing reconstruction for TWIX data
% 
% % get parameters
% Col_n = numel(image_data(:,1,1,1,1));
% Cha_n = numel(image_data(1,:,1,1,1));
% Lin_n = numel(image_data(1,1,:,1,1));
% Phs_n = numel(image_data(1,1,1,:,1));
% Seg_n = numel(image_data(1,1,1,1,:));
% % contiguous lines per phase
% Cos_n = Lin_n/Seg_n;
% % total lines per segment
% Laq_n = Cos_n*Phs_n;
% 
% %% reorganize data 
% % L contains all aquired lines arranged as:
% % 'time', 'data', 'repeat'
% 
% % intialize temporally arranged container
% L = zeros(Laq_n,Col_n,Seg_n,Cha_n);
% for cnt_c = 1:Cha_n
%     % initialize image data (output: 'Lin' 'Col' 'Phs')
%     T = sum(image_data(:,cnt_c,:,:,:),5);
%     T = permute(T,[3 1 4 2 5]);
%     % this loop increases in time
%     for cnt_l = 1:numel(t(:,1))
%         % current k-space line
%         cnt_k = t(cnt_l,2);
%         % current phase
%         cnt_p = t(cnt_l,3);
%         % current segment
%         cnt_s = t(cnt_l,4);
%         % store index
%         s_l = mod(cnt_l - 1,Laq_n) + 1; % assumes consecutive segments
%         % store
%         L(s_l,:,cnt_s,cnt_c) = T(cnt_k,:,cnt_p);
%     end
% end
% 
% %% acquisition time
% % s provides the positioning of the data above:
% % 'time', 'position','repeat'
% % position information consists of 
% % [timestamp k-line segment repeat]
% % hence an image can be formed every Cos_n rows of L
% 
% % timing option (midnight can be used to match acceleration)
% t(:,1) = t(:,1)*2.5; % [ms] relative to midnight
% % t(:,1) = (t(:,1) - t(1,1))*2.5; % [ms] relative to first line
% % positioning array
% s = permute(reshape(t',[4 Laq_n Seg_n]),[2 1 3]);
% 
% %% view sharing
% % MG is a time stack 
% 
% % initialize output variables
% MG = zeros([Laq_n - Cos_n Col_n Lin_n]);
% 
% % view share
% if view_share
%     It = 1:(Laq_n - Cos_n)+1;
% else
%     It = 1:Cos_n:Laq_n;
% end
% 
% % loop over adjacent lines
% for cnt_i = It
%     % adjacent lines
%     ifst = cnt_i;
%     ilst = cnt_i + Cos_n - 1;
%     % loop over all channels
%     IM = zeros([Lin_n Col_n]);
%     im = zeros([Lin_n Col_n]);
%     % combine channels
%     for cnt_c = 1:Cha_n
%         % image information
%         temp = L(ifst:ilst,:,:,cnt_c);
%         temp = reshape(permute(temp,[1 3 2]),[Lin_n Col_n]);
%         % indeces
%         ind = s(ifst:ilst,2,:);
%         % store k-space data
%         IM(ind(:),:) = temp;
%         % sum of squares
%         im = im + ( ifftshift(abs(ifftn(fftshift(IM)))) ).^2;
%     end
%     % store
%     MG(cnt_i,:,:) = (im').^0.5;
% end
% % time data
% MG = MG(It,:,:);
% % frequency data
% mg = ipermute(fft2(permute(MG,[2 3 1])),[2 3 1]);
% end
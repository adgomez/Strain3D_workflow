% Generates a structure array called SliceList, which contains imaging and
% positioning information for each tagged slice. This script is specially
% built for DICOM data. 
% 
% Use the sufix "_Failed" to skip any corrupted files. Alternatively, files
% can be isolated later in the processing, but this should be specificed in
% the subject's paramters file. 

%% housekeeping

clc
close all
clear all

% version 2: (formerly: twix_open) 
% Restructuring to harmonize with dicom reading. In particular,
% this routine just focuses on assembing a slicelist. Everyting is set on
% dynamic 1, combining will occur later

% ADG 2017-01-20

%% load analysis folders;

enable_folders

%% add twix reconstruction library to path

mfilesfolder = 'twix_reader';
path(path,mfilesfolder);

%% load twix data
% TODO: some flip may be needed to match DICOM reconstruction

% this is where we find the data
parent_folder = PARAM.tag.parent_folder

% find all .dat files
Files = dir([parent_folder '/*.dat'] );

% inialize variables
cnt_s = 1; % slice counter

%  loop throught the .dat files looking for spamm data
for cnt_f = 1 % :size(Files,1)
    twix_file = [Files(cnt_f).name];
    
    % echo the name of the file
    twix_file

    % *** parse dat file 
    % set flags to avoid mistakes
    twix_cotent_flag = false;
    twix_reco_flag = false;
    
    % try to open the file
    try
        twix_obj = mapVBVD([parent_folder '/' twix_file]);
        twix_content_flag = true;
    catch 
        warning(['unable to read  ' [parent_folder '/' twix_file]])
    end

    % if the file contains twix data, check for CSPAMM protocol, discard REF
    if twix_content_flag
        if  (~isempty(strfind(twix_obj.hdr.Dicom.tProtocolName,'_CSPAMM_'))) ...
            &(isempty(strfind(twix_obj.hdr.Dicom.tProtocolName,'_REF'))) 
        
            % if the file is a plain CSPAMM slice, signal reco
            twix_reco_flag = true;
        end
    end
        
    % *** reconstruct
    if twix_reco_flag
        
        % oversampling flag
        twix_obj.image.flagRemoveOS = 1;       

        % *** slice position (mean)
        Spos = mean(double(twix_obj.image.slicePos),2)';
        q = Spos(4:7);
        Q = quat2rot_v1(q);
        
        % create a tile
        FOV = [twix_obj.hdr.Meas.ReadFoV twix_obj.hdr.Meas.PhaseFoV];
        X = [0 FOV(1) FOV(1) 0]'; X = X - mean(X);
        Y = [0 0  FOV(2) FOV(2)]'; Y = Y - mean(Y);
        Z = [0 0 0 0]'; Z = Z - mean(Z);

        % extract transformation to laboratory coordinates
        c = Spos(:,1:3)';

        % trasnform tile
        x = Q(1,1)*X + Q(1,2)*Y + Q(1,3)*Z + c(1);
        y = Q(2,1)*X + Q(2,2)*Y + Q(2,3)*Z + c(2);
        z = Q(3,1)*X + Q(3,2)*Y + Q(3,3)*Z + c(3);
        
        % *** define plane3D properties    
        % e1: [3×1 double]
        slice_info3D.e1 = -Q(:,2);
        % e2: [3×1 double]        
        slice_info3D.e2 = -Q(:,1);
        % pixelSpacing: [2×1 double]
        slice_info3D.pixelSpacing = [ twix_obj.hdr.Meas.ReadFoV/twix_obj.image.sqzSize(1)
                                      twix_obj.hdr.Meas.PhaseFoV/twix_obj.image.sqzSize(3)];
        % rows: 1x1 double
        slice_info3D.rows = twix_obj.image.sqzSize(1);
        % cols: 1x1 double
        slice_info3D.cols = twix_obj.image.sqzSize(3);       
        % origin: [3×1 double]
        slice_info3D.origin = [x(4) y(4) z(4)]';
        % pnt: [3×1 double]
        slice_info3D.pnt = [x(4) y(4) z(4)]';
        % norm: [3×1 double]
        slice_info3D.norm = Q(:,3);
        % center: [3×1 double]
        slice_info3D.center = mean([x(:) y(:) z(:)])';
        % lt: [3×1 double]
        slice_info3D.lt = [x(4) y(4) z(4)]';
        % lb: [3×1 double]
        slice_info3D.lb = [x(3) y(3) z(3)]';
        % rt: [3×1 double]
        slice_info3D.rt = [x(1) y(1) z(1)]';
        % rb: [3×1 double]
        slice_info3D.rb = [x(2) y(2) z(2)]';
        % width: 240
        slice_info3D.width = twix_obj.hdr.Meas.ReadFoV;
        % height: 240
        slice_info3D.height = twix_obj.hdr.Meas.ReadFoV;
        % para: [0 0 -1 -60]
        slice_info3D.para = [slice_info3D.norm; (Q*slice_info3D.norm)'*c];
        
        % *** set slice properties    
        % copy 3d info
        SliceList{1,cnt_s}.Planes3D = slice_info3D;
        % pixel spacing (NOTE: multiplied by step)
        SliceList{1,cnt_s}.PxlSpacing = slice_info3D.pixelSpacing;
        % tag spacing (NOTE: can't be found on twix_obj)
        SliceList{1,cnt_s}.TAG_Spacing = 6; % in pixels
        % thickness (NOTE: field is empty)
        SliceList{1,cnt_s}.Thickness = 8; % twix_obj.hdr.Meas.thickness;
        
        % *** special fields for TBI project
        % tagging direction
        SliceList{cnt_s}.dynamic{1}.tagDir = slice_info3D.e1; % (freq)
        % reco type
        SliceList{cnt_s}.dynamic{1}.reco = 'TWIX_v8';
        
        % *** get temporal data     
        % initialize (NOTE: this is a nice spot for catching a exception)
        kspace_data = twix_obj.image{''};
        storage_order = [3 1 2];    
        IM1 = [];
        % collapse segments
        kspace_data = sum(kspace_data,5);
        % reco permutation order
        reco_perm = [1 3 4 2 5];
        % sqzDims: {'Col'  'Cha'  'Lin'  'Phs'  'Seg'}      
        % combine channels across all time frames
        for cnt_ph = 1:size(kspace_data,4)
            for cnt_ch= 1:size(kspace_data,2)
                % get one image
                TEMP = permute(kspace_data(:,cnt_ch,:,cnt_ph,1),reco_perm);
                % sum of squares reconstruction
                if (cnt_ch==1)
                    % initialize when needed
                    im = ifftshift(abs(ifftn(fftshift(TEMP)))).^2;
                else
                    % apply sum of squares
                    im = im + ( ifftshift(abs(ifftn(fftshift(TEMP)))) ).^2;
                end
            end
            % concat temporal data
            IM1 = cat(1,IM1,permute(im,storage_order));
            % transform combined image
            IM = fftshift(fftn(fftshift(im.^0.5)));
            % put back (preserving the format is needed for segment separation)
            kspace_data(:,1,:,cnt_ph,1) =  ipermute(IM,reco_perm);
        end
        % repeat segments (preserving the format is needed for segment separation)
        kspace_data = repmat(kspace_data(:,1,:,:,1),[1 1 1 1 size(twix_obj.image{''},5)]);

        % image data
        SliceList{1,cnt_s}.dynamic{1}.Data =  IM1;
               
        % phase time (approx.)
        TT1 = repmat(twix_obj.hdr.Config.TR/1e3,[1 size(twix_obj.image{''},4)]);
        SliceList{1,cnt_s}.dynamic{1}.ttime = TT1;
        
        % *** raw data and time stamps
        timestamp = [double(twix_obj.image.timestamp)', ...
                     double(twix_obj.image.Lin)', ...
                     double(twix_obj.image.Phs)', ...
                     double(twix_obj.image.Seg)' ];
                 
        SliceList{1,cnt_s}.dynamic{1}.rawData =  kspace_data;
        SliceList{1,cnt_s}.dynamic{1}.timestamp =  timestamp;
                 
        % get data structure
        data_struc(cnt_s,1).folder = parent_folder;
        data_struc(cnt_s,1).name = twix_file;
        data_struc(cnt_s,1).Nt = numel(TT1);  
        
        % *** update slice count
        cnt_s = cnt_s + 1;
    end

end 

%% check

if false  
    figure
    for cnt_s = 1:size(SliceList,2)
        SH = permute(SliceList{cnt_s}.dynamic{1}.Data,[2 3 1]);
        % figure
        slice_view_v4(SH)
        title(['slice ' num2str(cnt_s)]), axis square
        drawnow
        pause(0.5) 
    end   
end

%% save

save([analysis_folder '/' 'TAG_TWX'],'SliceList')
save([analysis_folder '/' 'TAG_TWX_slice_order'],'data_struc')

%% remove twix reader library

rmpath(mfilesfolder)




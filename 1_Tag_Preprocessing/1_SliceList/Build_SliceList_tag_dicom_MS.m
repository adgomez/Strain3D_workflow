% Generates a structure array called SliceList, which contains imaging and
% positioning information for each tagged slice. This script is specially
% built for DICOM data. 
% 
% Use the sufix "_Failed" to skip any corrupted files. Alternatively, files
% can be isolated later in the processing, but this should be specificed in
% the subject's paramters file. 

%% housekeeping

clc
close all
clear all

% latest: adding support for multislice acquisitions (MS)
% ADG 2017-05-08

%% load analysis folders;

enable_folders

%% add dicom reader library

mfilesfolder = 'dicom_reader';
path(path,mfilesfolder);

%% extract parameters

parent_folder = PARAM.tag.parent_folder;
tag_spacing_l = PARAM.tag.spacing_l;
mag_only = PARAM.tag.mag_only;
mag_first = PARAM.tag.mag_first;

%% parse dicom data

% get directory listing
folder_struc = dir(parent_folder);

% initialize
data_N = 0; % slice data total

% *** find subfolders with dicom data
for cnt_f = 1:size(folder_struc,1)
    
    % check the subfolder name, look for CSPAMM, discard REF
    if (  (folder_struc(cnt_f).isdir) ...
         &(~isempty(strfind(folder_struc(cnt_f).name,'_CSPAMM_'))) ...
         &(isempty(strfind(folder_struc(cnt_f).name,'_REF')))   ) 
        
         % check typical size
         if strfind(folder_struc(cnt_f).name,'Failed')
             warning(['the file: ' folder_struc(cnt_f).name ' was skipped (marked as failed).'])
         elseif strfind(folder_struc(cnt_f).name,'_SEG')
             warning(['the file: ' folder_struc(cnt_f).name ' was skipped (marked as SEG scan).'])
         else             
             % find all dicom files
             Files = dir([parent_folder '/' folder_struc(cnt_f).name '/*.dcm'] );

             % advance slice data total
             data_N = data_N + 1;

             % get data structure
             data_struc(data_N,1).folder = folder_struc(cnt_f).name;
             data_struc(data_N,1).name = Files(1).name;
             data_struc(data_N,1).Nt = size(Files,1);        
         end
    end
end

%% throw error if no images were found

if ~exist('data_struc')
    error('no CSPAMM files were found')
end

%% require input for number of timesteps

Steps_n = 19;

for cnt_i = 1:size(data_struc,1)
    numf(cnt_i) = data_struc(cnt_i,1).Nt;  
end 

home
disp(['(more above)'])
disp(['********************************************************* '])
disp(['Found ' num2str(size(data_struc,1)) ' multislice folders.'])
disp(['Each folder has between ' num2str(min(numf)) ' and ' num2str(max(numf)) ' files.'])

% TODO: add timer, so that default will be used after n seconds
continueloop = false;
while continueloop
    disp(['* Reader set to ' num2str(Steps_n) ' time steps. '])
    disp(['  Reco will result in ' num2str(min(numf/Steps_n)) ' to ' num2str(max(numf/Steps_n)) ' slices.'])
    str = input('> Is this correct? (y/n): ','s');

    if strcmp(str,'y')
        disp(['********************************************************* '])
        continueloop = false;
    else
        Steps_n = input('> Please enter new number of steps: ');
    end
end


%% extract -- Magnitude and Phase
% this will look for even number of subfolders
% it images look weird, try inverting mag_first

if ~mag_only

% slice list prototype 
nums = numf/Steps_n; % number of slices per folder
cnt_v = (1:2:size(data_struc,1)); % jump every other (for phase)
numsp = nums(cnt_v); % number of slices per folder (every other)
SliceList = cell(1,sum(numsp(1:2:end))); % total expected number of slices

cnt_s = 0; % counts slices
for cnt_F = 1:numel(cnt_v) % loops through folders
    
    % set folder name for fist frame
    SM_folder{1} = [parent_folder '/' data_struc(cnt_v(cnt_F)).folder '/'];
    
    for cnt_f = 1:numsp(cnt_F) % loops through files

        filein{1,1} = [data_struc(cnt_v(cnt_F)).name(1:end-10) '_' sprintf('%0.5i',cnt_f)]; 

        % read basic information in slicelist format from 1st time frame
        % NOTE: assuming time slices are identical, except for time.
        [slice_data,slice_info] = siemens_Dicom_Load_Data_v3(SM_folder,filein,'SPAMM-LINES');

        % plane3D properties
        slice_info3D = Slice3d_info_dicom_v2(slice_info{1});

        % update slice
        cnt_s = cnt_s + 1;
        
        % copy 3d info
        SliceList{cnt_s}.Planes3D = slice_info3D;

        % pixel spacing (NOTE: multiplied by step)
        SliceList{cnt_s}.PxlSpacing = slice_data.slice{1}.PxlSpacing;

        % tag spacing (NOTE: this appears to be hardcoded in HARP load module)
        SliceList{cnt_s}.TAG_Spacing = tag_spacing_l; % in pixels

        % thickness
        SliceList{cnt_s}.Thickness = slice_data.slice{1}.Thickness;

        % tagging direction
        phase_d = slice_info{1}.InPlanePhaseEncodingDirection;
        if strcmp(phase_d,'ROW')
            SliceList{cnt_s}.dynamic{1}.tagDir = slice_info3D.e1;
        else
            SliceList{cnt_s}.dynamic{1}.tagDir = slice_info3D.e2;
        end

        % reco type
        SliceList{cnt_s}.dynamic{1}.reco = 'DICOM';

        % initialize time loop
        storage_order = [3 1 2];
        TT1 = []; TT2 = [];
        IM1 = []; IM2 = [];
        PH1 = []; PH2 = [];

        % get temporal data
        TM = (cnt_f:numsp(cnt_F):data_struc(cnt_v(cnt_F)).Nt)';
        
        for cnt_t = 1:numel(TM)

            mag_first = 1;
            cnt_m = cnt_v(cnt_F) + (mag_first == 0);
            cnt_p = cnt_v(cnt_F) + (mag_first == 1);

            % echo file name
            % [parent_folder '/' data_struc(cnt_m).folder '/']
            [data_struc(cnt_m).name(1:end-10) '_' sprintf('%0.5i',TM(cnt_t))]

            % dynamic 1 (magnitude)
            SL_folder{1} = [parent_folder '/' data_struc(cnt_m).folder '/'];
            filein{1,1} = [data_struc(cnt_m).name(1:end-10) '_' sprintf('%0.5i',TM(cnt_t))]; 
            [slice_data,slice_info] = siemens_Dicom_Load_Data_v3(SL_folder,filein,'SPAMM-LINES');
            TT1 = cat(1,TT1,slice_data.slice{1}.dynamic{1}.ttime);
            IM1 = cat(1,IM1,permute(slice_data.slice{1}.dynamic{1}.phase{1}.magnitude,storage_order));
            clear slice_data slice_info

            % dynamic 1 (phase)
            SL_folder{1} = [parent_folder '/' data_struc(cnt_p).folder '/'];
            filein{1,1} = [data_struc(cnt_p).name(1:end-10) '_' sprintf('%0.5i',TM(cnt_t))]; 
            [slice_data,slice_info] = siemens_Dicom_Load_Data_v3(SL_folder,filein,'SPAMM-LINES');
            PH1 = cat(1,PH1,permute(slice_data.slice{1}.dynamic{1}.phase{1}.magnitude,storage_order));
            clear slice_data slice_info 

        end

        % time (triggers assignment exception if not the same length) 
        SliceList{cnt_s}.dynamic{1}.ttime = TT1;

        % image data
        SliceList{cnt_s}.dynamic{1}.Data = IM1.*exp(sqrt(-1)*(PH1*pi/2048 - pi));

    end
end

end



%% extract -- Magnitude only
% this will extract all files as magnitude
% if images look weird, try mag and phase

if mag_only
    error('magnitude-only option not yet supported (you can remove phase in later steps)')
end

%% check

if true  
    figure
    for cnt_s = 1:size(SliceList,2)
        SH = permute(SliceList{cnt_s}.dynamic{1}.Data,[2 3 1]);
        % figure
        clf
        slice_view_v4(SH)
        title(['slice ' num2str(cnt_s)])
        drawnow
        pause(0.5) 
    end   
end

%% save

save([analysis_folder '/' 'TAG_DCM'],'SliceList')
save([analysis_folder '/' 'TAG_DCM_slice_order'],'data_struc')

%% remove dicom reader library

rmpath(mfilesfolder)

function QX1d = opt_int_v1(S1,S2,S3,SM,tag_pl,tag_px,Stg,flag)
% QX1d = opt_int_v1(S1,S2,S3,SM,flag) estimates optimal interpolation
% locations along frequency direction


if (nargin == 7)
    flag = 0;
end


%% estimate optimal interpolation resolution

% test quantities
SX1d = Stg(1)*S1(:,1,1) + Stg(2)*S2(:,1,1) + Stg(3)*S3(:,1,1);
SM1d = mean(mean((SM(:,:,:,1)),3),2);
SF1d = fft(SM1d);
% number of tags given target spacing 
tag_n = floor((max(SX1d) - min(SX1d))/tag_pl);
% number of px in target resolution
QX1d_N = tag_px*tag_n;
% target resolution
QX1d_res = tag_pl/tag_px;
% target coordinates
QX1d = min(SX1d):QX1d_res:max(SX1d);
% reduce to target size
QX1d = QX1d(1:round(QX1d_N))';
% match coordinate closest to zero
[~,Y1] = min(abs(QX1d));
[~,Y2] = min(abs(SX1d));
QX1d = QX1d - QX1d(Y1) + SX1d(Y2);
% get reference wave
ref_cos = true;
if ref_cos
    QM1d = max(abs(SM1d))*cos((QX1d - min(QX1d))*2*pi/tag_pl );   
else
    QM1d = interp1(SX1d,SM1d,QX1d);
end
% fft
QF1d = fft(QM1d);

%% feedback

if flag

    figure
    subplot(211)
    hold on
    plot(SX1d,real(SM1d),'b')
    plot(SX1d,imag(SM1d),'--b')
    plot(QX1d,real(QM1d),'r')
    plot(QX1d,imag(QM1d),'--r')
    hold off
    grid on
    title('magnitude in tagged direction')

    subplot(212)
    hold on
    plot(abs(SF1d)/numel(SM1d),'b')
    plot(abs(QF1d)/numel(QM1d),'r')
    hold off
    grid on
    title('freq in tagged direction')

end

end

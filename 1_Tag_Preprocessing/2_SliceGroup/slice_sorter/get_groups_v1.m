function groups = get_groups_v1(sel_array,type,group_min)

% find unique groups
[Urows,~,Ir] = unique(sel_array(:,2:end),'rows');
rep = histcounts(Ir,size(Urows,1))';

groups = [];

% store defining info in each group
Uid = find(rep >= group_min);
for cnt_g = 1:numel(Uid)
    groups(cnt_g).type = type;
    groups(cnt_g).tagDir = Urows(Uid(cnt_g),1:3);
    groups(cnt_g).members = sel_array(Ir == Uid(cnt_g),1);
end

end
function [X_c,Y_c,Z_c,MG_c,MG_dim,MG_res] = slice2matrix_v3(X1,X2,X3,MG)
% [X_c,Y_c,Z_c,MG_c,dim] = slice2matrix_v3(X1,X2,X3,MG)
% 
% Converts slices of the same dimension with physical coordinates X1,X2,X3
% and intensity values MG into a matrix stack. If the slices are parallel, 
% they can form a lattice. A warning will be produced if the scack is not
% parallel.
% 
% If multiple intensity timepoints exist, they will be stored in the 4th
% dimension.

% version 3: implicit storage order [F,P,S,t] 
% 
% TODO: support n intensity value groups
% 
% ADG 2015-09-17

%% generate stack

% dimension variables
SD = size(MG{1});
SL = size(MG);
MG_dim = [SD(1:2) SL(2) SD(3)];

% initalize
X_c = zeros(MG_dim(1:3));
Y_c = zeros(MG_dim(1:3));
Z_c = zeros(MG_dim(1:3));
MG_c = zeros(MG_dim);

for cnt_s = 1:MG_dim(3)

    % coordinates
    X_c(:,:,cnt_s) = X1{cnt_s};
    Y_c(:,:,cnt_s) = X2{cnt_s};
    Z_c(:,:,cnt_s) = X3{cnt_s};
    
    % tagged images
    tempMG1 = reshape(MG{cnt_s},[SD(1:2) 1 SD(3)]);
     
    for cnt_t = 1:MG_dim(4)
        MG_c(:,:,cnt_s,cnt_t) = tempMG1(:,:,cnt_t);
    end
end

%% organize coordinates

% check for increasing order
Gn = [X_c(2,1,1) - X_c(1,1,1) Y_c(2,1,1) - Y_c(1,1,1) Z_c(2,1,1) - Z_c(1,1,1)
      X_c(1,2,1) - X_c(1,1,1) Y_c(1,2,1) - Y_c(1,1,1) Z_c(1,2,1) - Z_c(1,1,1)
      X_c(1,1,2) - X_c(1,1,1) Y_c(1,1,2) - Y_c(1,1,1) Z_c(1,1,2) - Z_c(1,1,1)];
  
% calculate spatial resolution vector  
MG_res = max(abs(Gn),[],1);

if numel(find(abs(Gn)<1e-3)) ~= 6
    warning(['stack may be rotated or composed of non-parallel slices']) 
    warning('Resolution vector was set to zero')
    
    MG_res = max(abs(Gn),[],1)*0;
end



end

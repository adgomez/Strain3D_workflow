function G_box = draw_box_v1(b_box,dflag)

G_box = [b_box(1,1) b_box(1,2) b_box(1,3)
         b_box(1,1) b_box(2,2) b_box(1,3)
         b_box(2,1) b_box(2,2) b_box(1,3)
         b_box(2,1) b_box(1,2) b_box(1,3)
         b_box(1,1) b_box(1,2) b_box(1,3)

         b_box(1,1) b_box(1,2) b_box(2,3)
         b_box(1,1) b_box(2,2) b_box(2,3)
         b_box(2,1) b_box(2,2) b_box(2,3)
         b_box(2,1) b_box(1,2) b_box(2,3)
         b_box(1,1) b_box(1,2) b_box(2,3)

         b_box(1,1) b_box(2,2) b_box(2,3)
         b_box(1,1) b_box(2,2) b_box(1,3)
         b_box(2,1) b_box(2,2) b_box(1,3)
         b_box(2,1) b_box(2,2) b_box(2,3)
         b_box(2,1) b_box(1,2) b_box(2,3)
         b_box(2,1) b_box(1,2) b_box(1,3)];

if dflag
    plot3(G_box(:,1),G_box(:,2),G_box(:,3),'b')
end
    
end
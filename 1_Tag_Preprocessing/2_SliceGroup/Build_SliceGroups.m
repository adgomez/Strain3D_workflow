% Inspects SliceList information and identifies members of the SliceList
% that make up a stack that can be tracked in 3D. This stack is called
% SliceGroup. At this point

%% housekeeping

clc
close all
clear all

% latest: including access to PARAM, removing from search path at the end
% 
% NOTE: Ideally, this script takes backwards-compatible slicelists and
% generates slicegroups what can be interpolated onto a volume. The
% slicegropus have phisical coordinates that match the image data.
% 
% NOTE: although this should produce a backwards-compatible slice list, it
% cannot function properly with older slicelists, because those do not
% include tag direction information
%
% NOTE: the tag spacing cannot be obtained from neither twix or dicom
% readers
% 
% NOTE: the code can include additional checks to make sure slicegroups
% have the same FOV and resolution. these checks can be done in the "find 
% radial groups" section

%% analysis folder path

enable_folders

%% add sorter library to path

mfilesfolder = 'slice_sorter';
path(path,mfilesfolder);

%% extract parameters

dcm = PARAM.tag.dcm;
tag_spacing_l = PARAM.tag.spacing_l;
tag_spacing_px = PARAM.tag.spacing_px;


%% load unsorted slice list

SL_folder = analysis_folder;  

if dcm
    SL_name = 'TAG_DCM';
else
    SL_name = 'TAG_TWX';
end

load([SL_folder '/' SL_name])

%% build loose slice variables

% slice counter  
for cnt_s = 1:size(SliceList,2)

    % *** extract parameters of interest
    % tag direction
    TG(cnt_s,:) = SliceList{cnt_s}.dynamic{1}.tagDir;

    % orienation vectors
    E1(cnt_s,:) = SliceList{cnt_s}.Planes3D.e1; 
    E2(cnt_s,:) = SliceList{cnt_s}.Planes3D.e2; 
    E3(cnt_s,:) = SliceList{cnt_s}.Planes3D.norm; 

    % slice centers
    ctr(cnt_s,:) = SliceList{cnt_s}.Planes3D.center;

    % matrix size
    mtr(cnt_s,:) = [SliceList{cnt_s}.Planes3D.rows ...
                    SliceList{cnt_s}.Planes3D.cols];

    % pixel spacing
    pxl(cnt_s,:) = SliceList{cnt_s}.PxlSpacing;

    % time (if this throws an error data is not the correct number of phases)
    aqtime(cnt_s,:) = SliceList{cnt_s}.dynamic{1}.ttime(1);
           
    % *** get images
    storage_order = [3 1 2];
    MG{cnt_s} = ipermute(SliceList{cnt_s}.dynamic{1}.Data,storage_order);  
    
     % *** permute to always place tagged direction along rows
    if strcmp(SliceList{cnt_s}.dynamic{1}.reco,'DICOM')
        if (sum(TG(cnt_s,:) == E1(cnt_s,:)) ~= 3)     
            MG{cnt_s} = permute(MG{cnt_s},[2 1 3]);
            E1(cnt_s,:) = SliceList{cnt_s}.Planes3D.e2; 
            E2(cnt_s,:) = SliceList{cnt_s}.Planes3D.e1; 
            mtr(cnt_s,:) = [SliceList{cnt_s}.Planes3D.cols ...
                            SliceList{cnt_s}.Planes3D.rows];
            pxl(cnt_s,:) = flip(SliceList{cnt_s}.PxlSpacing,2);
        end
    end
    
    % *** define physical coordinates
    % 1D parameters 
    s1 = (1:mtr(cnt_s,1))';
    s2 = (1:mtr(cnt_s,2))';

    % 2D paramters
    [S1,S2] = ndgrid(s1,s2); 

    % physical space definition
    X1_tmp = pxl(cnt_s,1)*S1.*E1(cnt_s,1)/norm(E1(cnt_s,:)) + pxl(cnt_s,2)*S2.*E2(cnt_s,1)/norm(E2(cnt_s,:));
    X2_tmp = pxl(cnt_s,1)*S1.*E1(cnt_s,2)/norm(E1(cnt_s,:)) + pxl(cnt_s,2)*S2.*E2(cnt_s,2)/norm(E2(cnt_s,:));
    X3_tmp = pxl(cnt_s,1)*S1.*E1(cnt_s,3)/norm(E1(cnt_s,:)) + pxl(cnt_s,2)*S2.*E2(cnt_s,3)/norm(E2(cnt_s,:));

    % center
    X1{cnt_s} = X1_tmp - mean(X1_tmp(:)) + ctr(cnt_s,1);
    X2{cnt_s} = X2_tmp - mean(X2_tmp(:)) + ctr(cnt_s,2);
    X3{cnt_s} = X3_tmp - mean(X3_tmp(:)) + ctr(cnt_s,3);
    
end

%% check for time consistency

dtime = diff(aqtime,1,1);
if any(dtime(:))
    warning('time frames appear to have to be acquired at different intervals');
end
aqint = dtime(1);

%% find radial groups
% NOTE: 
group_dim_tol = 1e-2;
group_dir_tol = 1e-3;
group_min = 4;

% radial groups share tag dir and center
rad_array = [(1:size(SliceList,2))' ...
            round(abs(TG)/group_dir_tol)*group_dir_tol ...
            round(ctr/group_dim_tol)*group_dim_tol];
        
% parallel groups share tag dir, norm, and 2 ctr coordinates
prx_array = [(1:size(SliceList,2))' ...
            round(abs(TG)/group_dir_tol)*group_dir_tol ...
            round(ctr(:,[2 3])/group_dim_tol)*group_dim_tol ...
            round(abs(E3)/group_dir_tol)*group_dir_tol];
        
pry_array = [(1:size(SliceList,2))' ...
            round(abs(TG)/group_dir_tol)*group_dir_tol ...
            round(ctr(:,[1 3])/group_dim_tol)*group_dim_tol ...
            round(abs(E3)/group_dir_tol)*group_dir_tol];
        
prz_array = [(1:size(SliceList,2))' ...
            round(abs(TG)/group_dir_tol)*group_dir_tol ...
            round(ctr(:,[1 2])/group_dim_tol)*group_dim_tol ...
            round(abs(E3)/group_dir_tol)*group_dir_tol];
        
% find unique groups
SliceGroup = [get_groups_v1(prx_array,'par',group_min) ...
              get_groups_v1(pry_array,'par',group_min) ...
              get_groups_v1(prz_array,'par',group_min) ...
              get_groups_v1(rad_array,'rad',group_min)];
        
%% identify repeats
% NOTE: repeats share center, normal, and tag direction

% pick a slice group
for cnt_s = 1:size(SliceGroup,2)

    % find type
    if strcmp(SliceGroup(cnt_s).type,'par')
        % find direction along normal
        Ct = ctr(SliceGroup(cnt_s).members,:);
        Nt = abs(E3(SliceGroup(cnt_s).members,:));
        
        % find dot product
        DT = sum(Ct.*Nt,2);
        DT = round(DT/group_dim_tol)*group_dim_tol; 
        
        % get uniques and assignment in unique array
        [Uloc,Usug,Uord] = unique(DT);
        
        % find repeats
        Rep = histcounts(Uord,size(Uloc,1))';
        Rep = find(Rep>1);
        
        % find holes
        if any( (diff(Uloc) - mean(diff(Uloc))) > 1e-2 )
            warning('some slices appear to be missing')
            % normal = Nt(1,:)
            sparation = Uloc
            gap = diff(Uloc)
        end
        
        % update groups
        SliceGroup(cnt_s).rot = Uloc;
        SliceGroup(cnt_s).memberOrder = Uord;
        SliceGroup(cnt_s).recoOrder = Usug;
        SliceGroup(cnt_s).repeats = Rep; 
    else
        % TODO: implement multi-directional arrangement   
        % get rotation about tagdir
        Ct = TG(SliceGroup(cnt_s).members,:);
        Nt = cross(E3(SliceGroup(cnt_s).members,:),Ct,2);
        
        % this works, but needs to be more robust
        Rot = atand(Nt(:,2)./Nt(:,1));
        Rot = round(Rot/group_dir_tol)*group_dir_tol; 
        
        % get uniques and assignment in unique array
        [Uloc,Usug,Uord] = unique(Rot);
        
        % find repeats
        Rep = histcounts(Uord,size(Uloc,1))';
        Rep = find(Rep>1);
        
        % find holes
        if any(diff(Uloc) ~= mean(diff(Uloc)))
            warning('some slices appear to be missing')
            % normal = Nt(1,:)
            sparation = Uloc
            gap = diff(Uloc)
        end
        
        % update groups
        SliceGroup(cnt_s).rot = Uloc;
        SliceGroup(cnt_s).memberOrder = Uord;
        SliceGroup(cnt_s).recoOrder = Usug;
        SliceGroup(cnt_s).repeats = Rep; 
    end
end

%% alternative reconstruction order and viewshare
% TODO: define alternative reconstruction order from accel data 

reco_type = 'last';

switch reco_type
    case 'first'
        % use the last instance of all repeats
        for  cnt_g = 1:size(SliceGroup,2)
            % get all repeats
            Ir = SliceGroup(cnt_g).repeats;
            for cnt_r = 1:numel(Ir)
               % get all the members that are id as the repeat 
               Iri = find(SliceGroup(cnt_g).memberOrder == Ir(cnt_r));
               % place last instance of repeat in recoRrder
               SliceGroup(cnt_g).recoOrder(Ir(cnt_r)) = Iri(1);
            end
        end
    case 'last'
        % use the last instance of all repeats
        for  cnt_g = 1:size(SliceGroup,2)
            % get all repeats
            Ir = SliceGroup(cnt_g).repeats;
            for cnt_r = 1:numel(Ir)
               % get all the members that are id as the repeat 
               Iri = find(SliceGroup(cnt_g).memberOrder == Ir(cnt_r));
               % place last instance of repeat in recoRrder
               SliceGroup(cnt_g).recoOrder(Ir(cnt_r)) = Iri(end);
            end
        end        
    otherwise
        error('invalid volumetric reconstruction type')
end

%% finalize variables

% pick a slice group
for cnt_g = 1:size(SliceGroup,2)

    % sensitivity matrix construction (define based on selection)
    N(cnt_g,:) = SliceGroup(cnt_g).tagDir; 
    
    % reco order
    Is = SliceGroup(cnt_g).members(SliceGroup(cnt_g).recoOrder);

    % *** get coarse volume
    [S1,S2,S3,SM,~,~] = slice2matrix_v3(X1(Is),X2(Is),X3(Is),MG(Is));

    % save coarse variables
    SliceGroup(cnt_g).S1 = S1;
    SliceGroup(cnt_g).S2 = S2;
    SliceGroup(cnt_g).S3 = S3;
    SliceGroup(cnt_g).SM = SM;
    
    % *** interpolation parameters
    % nominal tag period in length units
    tag_pl = tag_spacing_l;  
    % target tag period in px (ideally integer)
    tag_px = tag_spacing_px; 

    % get interpolation locations along tagged dir
    Stg = SliceGroup(cnt_g).tagDir;
    q = opt_int_v1(S1,S2,S3,SM,tag_pl,tag_px,Stg);
    
    % save interpolation locations
    SliceGroup(cnt_g).q = q;
    SliceGroup(cnt_g).tag_pl = tag_pl;
    SliceGroup(cnt_g).tag_px = tag_px;
    
    % additional information for radial groups
    if strcmp(SliceGroup(cnt_g).type,'rad')    
       % the first sag slice tends to be flipped altogether 
        fslc = 1;
        fdim = 2;
        SliceGroup(cnt_g).S1(:,:,fslc) = flip(S1(:,:,fslc),fdim);
        SliceGroup(cnt_g).S2(:,:,fslc) = flip(S2(:,:,fslc),fdim);
        SliceGroup(cnt_g).SM(:,:,fslc,:) = flip(SM(:,:,fslc,:),fdim);
        t = SliceGroup(cnt_g).rot; 
    end
    
end

%% preview

if false
    
    % pick a slicegroup
    cnt_g = 1;
    
    figure
    hold on
    % pick a frame
    for cnt_t = 1% :13
        clf
        
        % change use first number to see odd or even
        for cnt_j = 1:1:size(SliceGroup(cnt_g).SM(:,:,:,:),3)

            % get slice-based variables
            sm = SliceGroup(cnt_g).SM(:,:,cnt_j,cnt_t);
            s1 = SliceGroup(cnt_g).S1(:,:,cnt_j);
            s2 = SliceGroup(cnt_g).S2(:,:,cnt_j);
            s3 = SliceGroup(cnt_g).S3(:,:,cnt_j);

            slice_view_v4(s1,s2,s3,abs(sm))
            view(-180,90)
            title(['slc = ' num2str(cnt_j) '  t = ' num2str(cnt_t)])
            drawnow

        end
    end
    disp('Stopped to preview data.')
    return
end

%% preview slice location and quick reco

% some visualization vars
p_h = 1; ceil(size(SliceGroup,2)^0.5);
p_v = size(SliceGroup,2);% floor(size(SliceGroup,2)^0.5);

% tile view
show_image = false;

% pick a slice group
for cnt_g = 1:size(SliceGroup,2)

    % bounding box
    subplot(p_h,p_v,cnt_g)
    hold on
    
    % get members
    Is = SliceGroup(cnt_g).members(SliceGroup(cnt_g).recoOrder);
    
    % downsample visualization (optional)
    % Is = Is(1:3:end);

    % get member 
    for cnt_m = 1:numel(Is)
        
        % get slice
        cnt_s = Is(cnt_m);

        % render image/tile
        if show_image
            % get and display first time frame
            IM = MG{cnt_s};       
            slice_view_v4(X1{cnt_s},X2{cnt_s},X3{cnt_s},abs(IM(:,:,1)))
            alpha(0.4)
        else
            % this is just a tile generation
            dimo = size(X1{cnt_s});
            It = sub2ind(dimo,[1 1 dimo(1) dimo(1)],[1 dimo(2) dimo(2) 1]);
            patch(X1{cnt_s}(It),X2{cnt_s}(It),X3{cnt_s}(It),abs(E3(cnt_s,:)),'FaceAlpha',0.05)
        end
        
        % render tag dir
        sc = 0.5*max([max(X1{cnt_s}(:)) - min(X1{cnt_s}(:))
                      max(X2{cnt_s}(:)) - min(X2{cnt_s}(:))
                      max(X3{cnt_s}(:)) - min(X3{cnt_s}(:))]);
        co = mean([X1{cnt_s}(:) X2{cnt_s}(:) X3{cnt_s}(:)]);
        TG = SliceGroup(cnt_g).tagDir;
        quiver3(co(1),co(2),co(3), ...
                sc*TG(1),sc*TG(2),sc*TG(3),0,'Color',abs(TG))
            
        % render slice number in slice list
        lc = 1;
        text(X1{cnt_s}(lc),X2{cnt_s}(lc),X3{cnt_s}(lc),num2str(cnt_s))
    end
    
    view(0,90) %  top
    % view(0,-90) % bottom
    % view(0,0)
    % view(90,0)
    view(-45,20)
    hold off
    grid on 
    axis equal
    
    title(['SliceGroup ' num2str(cnt_g) ' (' SliceGroup(cnt_g).type ')'])
    xlabel('x [mm]')
    ylabel('y [mm]')
    zlabel('z [mm]')

end

%% sequence

show_seq = true;

if show_seq
    % pick a slice group
    cnt_g = 1;
    
     % get members
    Is = SliceGroup(cnt_g).members(SliceGroup(cnt_g).recoOrder);
  
    figure
    hold on
    % change use first number to see odd or even
    for cnt_m = 1:2:numel(Is)
        
        IM = MG{Is(cnt_m)};  
        
        % subplot(1,numel(Is),cnt_m)
        % imshow(abs(IM(:,:,1)),[],'InitialMagnification','fit')
        
        slice_view_v4(X1{Is(cnt_m)},X2{Is(cnt_m)},X3{Is(cnt_m)},abs(IM(:,:,1)))
   
    end
    
end

%% save slicevars and slicegroups    
% NOTE: slicevars may not be used any longer

save([analysis_folder '/' 'SliceVars'],'X1','X2','X3','MG','E3','aqint','ctr')
save([analysis_folder '/' 'SliceGroup'],'SliceGroup')

%% remove dicom reader library

rmpath(mfilesfolder)














% Performs tag removal. This is a sketch.

%% housekeeping

clc
close all
clear all

% latest:
% NOTE: using implicit directions 

%% analysis folder path

enable_folders

%% add required routines to path

% preprocessing
mfilesfolder = [engine_folder '/1_Tag_Preprocessing/4_HARP_Filtering/HARP_filter'];
path(path,mfilesfolder)
        
%% extract parameters

% tag spacing in px in each direction
tag_sp = PARAM.tag.filt_sp;
% total time steps
steps_n = PARAM.tag.time_steps;

%% load data and define directions

% load image variables
im_folder = interp_folder;

% display and storage controls
show_data = false;
save_data_m = true;

%% filter

H = waitbar(0,'Filtering ...');
% pick a time point
for cnt_t = 1:steps_n
    
    % load all directions
    load([im_folder '/' 'TAG1_L_' num2str(cnt_t)]);
    load([im_folder '/' 'TAG2_L_' num2str(cnt_t)]);
    load([im_folder '/' 'TAG3_L_' num2str(cnt_t)]);

    % initialization
    if (cnt_t == 1)
        % *** calculate resolution and size
        Q_res = [Q1(2,1,1) Q2(1,2,1) Q3(1,1,2)] - [Q1(1,1,1) Q2(1,1,1) Q3(1,1,1)];
        Q_dim = size(QM1);
        Q_box = [min([Q1(:) Q2(:) Q3(:)])
                 max([Q1(:) Q2(:) Q3(:)])];

        % *** generate frequency domain
        % approximate frequency bin size
        F_s = 0.5./Q_res;
        F_f = F_s./size(Q1);

        % get freq vectors
        f1 = 2*freq_vec_v1(F_f(1),Q_dim(1));
        f2 = 2*freq_vec_v1(F_f(2),Q_dim(2));
        f3 = 2*freq_vec_v1(F_f(3),Q_dim(3));

        % define frew volumes
        [F1,F2,F3] = ndgrid(f1,f2,f3);

        % ***  build filters
        % find location of spectral peaks 
        [hp1,lp11,lp12] = get_HARP_peak_v2(F1,F2,F3,QM1,[1 tag_sp(1)]);
        [hp2,lp21,lp22] = get_HARP_peak_v2(F1,F2,F3,QM2,[2 tag_sp(2)]);
        [hp3,lp31,lp32] = get_HARP_peak_v2(F1,F2,F3,QM3,[3 tag_sp(3)]);

        % generate filters
        options.type = 'removal_low'; 
        options.sigma = 0.3*(1./tag_sp);
        options.center = lp11; F11 = get_filter_v3(F1,F2,F3,options);
        options.center = lp12; F12 = get_filter_v3(F1,F2,F3,options);
        options.center = lp21; F21 = get_filter_v3(F1,F2,F3,options);
        options.center = lp22; F22 = get_filter_v3(F1,F2,F3,options);
        options.center = lp31; F31 = get_filter_v3(F1,F2,F3,options);
        options.center = lp32; F32 = get_filter_v3(F1,F2,F3,options);
    end

    % *** filter data
    QF1 = ifftn( fftn(QM1).*ifftshift(F11.*F12) );
    QF2 = ifftn( fftn(QM2).*ifftshift(F21.*F22) );
    QF3 = ifftn( fftn(QM3).*ifftshift(F31.*F32) );
    
    % *** combine
    QFC = reshape(max(abs([QF1(:) QF2(:) QF3(:)]),[],2),size(QF1));
    
    % *** output result images
    if show_data
        filtering_L_view
        close(H)
        disp('****** STOPPED FOR PREVIEW ******')
        return
    end

    % *** save filtered data
    % mat format
    if save_data_m

        m_folder = filt_folder;
        save([m_folder '/' 'int_L_vars_' num2str(cnt_t)],...
                   'Q1','Q2','Q3','Q_res', ...
                   'QM1','QM2','QM3', ...     
                   'QF1','QF2','QF3','QFC'); 

        disp(['Saved as: ' 'int_L_vars_' num2str(cnt_t)])
    end
    
    % update
    waitbar(cnt_t/steps_n,H)
end
close(H)
    
%% remove filtering library from path

rmpath(mfilesfolder)

















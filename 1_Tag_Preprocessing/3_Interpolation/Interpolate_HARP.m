% Interpolates images in space using pixelwize complex values. This scripts
% operates on SliceGroups. This script was developed for HARP.

%% housekeeping

clc
close all force
clear all

% latest: replaced scatter-based radial interpolation with a form of weighted back projection
% TODO: zeropad kspace when slice matrices are not square
% TODO: accel-based reco

% NOTE: <I/O>
% reads SliceVars (mat) / writes TAG_t(mat)

%% analysis folder path

enable_folders

%% extract parameters

interp_groups = PARAM.tag.interp_groups;
reco_type = PARAM.tag.reco_type;
tag_spacing_l = PARAM.tag.spacing_l;
tag_spacing_px = PARAM.tag.spacing_px;

% specific
use_abs = PARAM.tag.use_abs_H;
int_method = PARAM.tag.int_method_H;
ext_method = PARAM.tag.ext_method_H;

Q_trans = PARAM.tag.trans;

%% load SliceGroups

sv_folder = analysis_folder;
sv_file = 'SliceGroup_RR';
if exist([sv_folder filesep sv_file '.mat'],'file') ~= 2
    sv_file = 'SliceGroup'; % add to _SYNTH or _FIXED if needed
end
load([sv_folder '/' sv_file])

%% preliminary check

for cnt_g = 1:size(SliceGroup,2)
    % sensitivity matrix construction
    N(cnt_g,:) = SliceGroup(cnt_g).tagDir;   
end

% warn for incomplete groups
if (rank(N) < 3)
    warning('Slicegroups are insufficient for 3D reconstruction') 
end

%% target coordinate system

% encoding directions
enc_dir = [(1:size(N,1))' N*[1;2;3]];

% arrange reconstruction order in x,y,z
[~,Ia] = sort(enc_dir(interp_groups,2));
Ip = interp_groups(Ia);

% define Q coordinates
[Q1,Q2,Q3] = ndgrid(SliceGroup(Ip(1)).q + Q_trans(1),SliceGroup(Ip(2)).q + Q_trans(2),SliceGroup(Ip(3)).q + Q_trans(3));
  
% bounding box
q_box = [min([Q1(:) Q2(:) Q3(:)]) 
         max([Q1(:) Q2(:) Q3(:)])];
     
q_res = [Q1(2,1,1) Q2(1,2,1) Q3(1,1,2)] - [Q1(1,1,1) Q2(1,1,1) Q3(1,1,1)];

%% interpolate

% some parameters
interp_slices = true;
check_result = true;

% variable for progressbar
cnt_i_N = size(Ip,2)*size(SliceGroup(Ip(1)).SM,4);
cnt_i = 0;

if interp_slices
    
    int_method = 'linear'; % 'spline';
    ext_method = 'nearest';

    Q_tag_pl = [];
    Q_tag_px = [];
    
    h = waitbar(cnt_i,'Please wait...');
    for cnt_g = 1:size(Ip,2)
        % disp('here be size(Ip,2)')
        % check for radial 
        if strcmp(SliceGroup(Ip(cnt_g)).type,'rad')
            warning('using weighted back-projection for radial interpolation') 
            for cnt_t = 1:size(SliceGroup(Ip(cnt_g)).SM,4)        
                % get image stack
                Si = SliceGroup(Ip(cnt_g)).SM(:,:,:,cnt_t);
                % apply absolute value
                if use_abs 
                    Si = abs(Si);
                end
                % interpolate
                TMP = Q1*0;
                for cnt_j = 1:size(Si,3)
                    % get slice-based variables
                    sm = Si(:,:,cnt_j);
                    s1 = SliceGroup(Ip(cnt_g)).S1(:,:,cnt_j);
                    s2 = SliceGroup(Ip(cnt_g)).S2(:,:,cnt_j);
                    s3 = SliceGroup(Ip(cnt_g)).S3(:,:,cnt_j);
                    % calculate normal
                    p1 = [s1(1,1) s2(1,1) s3(1,1)];
                    p2 = [s1(end,1) s2(end,1) s3(end,1)];
                    p3 = [s1(1,end) s2(1,end) s3(1,end)];
                    n = cross(p2-p1,p3-p2);
                    n = n/norm(n);
                    % define separation
                    k = 0.5*norm(max([s1(:),s2(:),s3(:)]) - min([s1(:),s2(:),s3(:)]));
                    % get forward and backward slice position
                    b1 = s1 - k*n(1);
                    b2 = s2 - k*n(2);
                    b3 = s3 - k*n(3);
                    f1 = s1 + k*n(1);
                    f2 = s2 + k*n(2);
                    f3 = s3 + k*n(3);
                    % get grid coordinates
                    [~, ~, ~, F_fwd,F_inv] = cart2im(cat(3,b1,s1,f1),cat(3,b2,s2,f2),cat(3,b3,s3,f3));
                    % get indices
                    I_q = (F_fwd*([Q1(:) Q2(:) Q3(:) 0*Q1(:) + 1]'))'; 
                    % interpolate
                    F = griddedInterpolant(cat(3,0*sm,sm,0*sm),int_method,ext_method);
                    TMP = TMP + reshape(F(I_q(:,1),I_q(:,2),I_q(:,3)),size(Q1));
                    TMP(isnan(TMP)) = 0;    
                end
                % save
                str_varn = ['QM' num2str(cnt_g)];
                str_cm = [str_varn ' = TMP;'];
                eval(str_cm)
                % save
                disp(['saving: ' 'TAG' num2str(cnt_g) '_H_' num2str(cnt_t)])
                save([interp_folder '/' 'TAG' num2str(cnt_g) '_H_' num2str(cnt_t)], ...
                      'Q1','Q2','Q3',str_varn)
                % waitbar 
                cnt_i = cnt_i + 1;
                waitbar(cnt_i/cnt_i_N,h)
            end
        else
            % get grid coordinates
            [~, ~, ~, F_fwd,F_inv] = cart2im(SliceGroup(Ip(cnt_g)).S1, ...
                                             SliceGroup(Ip(cnt_g)).S2, ...
                                             SliceGroup(Ip(cnt_g)).S3);
            % unpack image
            IM = SliceGroup(Ip(cnt_g)).SM;   
             % apply absolute value
            if use_abs 
                IM = abs(IM);
            end
            % get indices
            I_q = (F_fwd*([Q1(:) Q2(:) Q3(:) 0*Q1(:) + 1]'))';
            % get tag spec
            Q_tag_pl = [Q_tag_pl SliceGroup(cnt_g).tag_pl];
            Q_tag_px = [Q_tag_px SliceGroup(cnt_g).tag_px];
            % extrapolation weight
            W = (Q1.^2 + Q2.^2 + Q3.^2).^0.5;
            W = W/max(W(:));
            % time loop
            for cnt_t = 1:size(IM,4)       
                % interpolate
                F = griddedInterpolant(IM(:,:,:,cnt_t),int_method,ext_method);
                TMP = reshape(F(I_q(:,1),I_q(:,2),I_q(:,3)),size(Q1));
                % guide for extrapolation
                F = griddedInterpolant(IM(:,:,:,cnt_t),int_method,'none');
                GDE = reshape(F(I_q(:,1),I_q(:,2),I_q(:,3)),size(Q1));
                TMP(isnan(GDE)) = TMP(isnan(GDE)).*W(isnan(GDE));
                str_varn = ['QM' num2str(cnt_g)];
                str_cm = [str_varn ' = TMP;'];
                eval(str_cm)
                % save
                disp(['saving: ' 'TAG' num2str(cnt_g) '_H_' num2str(cnt_t)])
                save([interp_folder '/' 'TAG' num2str(cnt_g) '_H_' num2str(cnt_t)], ...
                      'Q1','Q2','Q3',str_varn)
                % waitbar 
                cnt_i = cnt_i + 1;
                waitbar(cnt_i/cnt_i_N,h)
            end
        end
    end
    close(h)
    
    % save tag spec
    save([interp_folder '/' 'TAG_spec'],'Q_tag_pl','Q_tag_px');
    
    % check resul
    if check_result
        
        view_v = [45 20];
        
        for cnt_g = 1:size(Ip,2)

            subplot(2,size(Ip,2),cnt_g)
            ortho_slice_v4(SliceGroup(Ip(cnt_g)).S1,SliceGroup(Ip(cnt_g)).S2,SliceGroup(Ip(cnt_g)).S3,SliceGroup(Ip(cnt_g)).SM(:,:,:,end),view_v)
            title(['slice group ' num2str(Ip(cnt_g))])
            
            subplot(2,size(Ip,2),size(Ip,2) + cnt_g)
            eval(['ortho_slice_v4(Q1,Q2,Q3,QM' num2str(cnt_g) ',view_v)'])
            title(['QM' num2str(cnt_g)])
        end
    end
    
end























% Register frames 2 though N to frame 1 for each slice. Works on SliceGroup

%% housekeeping

clc
close all
clear all

% latest: works at slicegroup level 
% AKK 2018-02-23

%% load analysis folders;

enable_folders

%% load 

% load SliceGroups
useTI = true;
if useTI == 0
    sliceGroupFile = 'SliceGroup';
elseif useTI == 1
    sliceGroupFile = 'SliceGroup_TI';
end

load([analysis_folder '/' sliceGroupFile])

%% run rigid registration

if strcmp(analysis_folder(end),filesep) == 0
    analysis_folder = [analysis_folder filesep];
end
RRfolder = [analysis_folder 'RR' filesep];

if exist(RRfolder,'dir') ~= 7
    mkdir(RRfolder)
end

SliceGroup = RegisterToBaseline_v4(RRfolder,SliceGroup,engine_folder);

%% save data

save([analysis_folder 'SliceGroup_RR'],'SliceGroup')

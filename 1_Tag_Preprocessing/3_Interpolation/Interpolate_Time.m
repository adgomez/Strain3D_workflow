% Interpolates images in time using segmented aquisition timing. This
% script operates in SliceGroups.

%% housekeeping

clc
close all
clear all

% TODO: add timing table for linear data (19 instead of 13 phases)
% latest: adding downsampling
% ADG 2018-06-21

%% load analysis folders;

enable_folders

%% parameters

% downsampling step (per current protocol, k-space interpolation adds 5 in between)
nn = 3; % 3 returns twice the number of steps

%% add t interpolation library to path

mfilesfolder = 'time_interpolator';
path(path,mfilesfolder);

%% load 

% load SliceGroups
sv_folder = analysis_folder;
sv_file = 'SliceGroup_FIXED';
load([sv_folder '/' sv_file])

% load twix timing
warning('using standard timing table for 13 phases')
load('twix_timing_tag')

%% interpolation loop

% useful temporal relationships
dimt = max(t); % time, lines, phases, segments

% initialize
SliceGroup_t = SliceGroup;

warning('reducing temporal resolution')
for cnt_g = 1:size(SliceGroup,2)
    SliceGroup_t(cnt_g).SM = SliceGroup_t(cnt_g).SM(:,:,:,1:13);
end

%loop over SliceGroups
for cnt_g = 1:size(SliceGroup,2)
    disp(['interpolating group ' num2str(cnt_g) ' of ' num2str(size(SliceGroup,2))])
    % get stack
    STK = SliceGroup(cnt_g).SM;
    % intialize
    STK_V = zeros(size(STK,1),size(STK,2),size(STK,3),size(t,1)/max(t(:,4)));
    % loop over slices
    for cnt_s = 1:size(STK,3)
        disp(['slice ' num2str(cnt_s) ' of ' num2str(size(STK,3))])
        % get slice
        IM = permute(STK(:,:,cnt_s,:),[4 1 2 3]);
        % *** deconstruct into freqency data in twix format 'Col'    'Cha'    'Lin'    'Phs'    'Seg'
        frequency_data = zeros([size(IM,3) 1 max(t(:,2:end))]);
        for cnt_i = 1:size(t,1)
            % grab slice
            TMP = abs(squeeze(IM(t(cnt_i,3),:,:)));
            % undo zero padding
            TMP = fftshift(fftn(fftshift(TMP)));
            strt = (size(frequency_data,1)-size(frequency_data,3))/2;
            TMP = TMP(:,strt + 1:strt + size(frequency_data,3));
            % store
            frequency_data(:,1,t(cnt_i,2),t(cnt_i,3),t(cnt_i,4)) = TMP(:,t(cnt_i,2));
        end
        % *** interpolate
        % apply interpolation
        [MG,~,~] = frequency_interpolator(frequency_data,t);
        % *** finalize
        % zero padding
        IM_V = zeros([size(MG,1) size(MG,2) size(MG,2)]);
        for cnt_i = 1:size(MG,1)
            IM_V(cnt_i,:,:) = ifftn( ifftshift(padarray(fftshift(fftn(squeeze(MG(cnt_i,:,:)))),[0 (size(MG,2)-size(MG,3))/2])) ) ;
        end
        % permute to stack format
        STK_V(:,:,cnt_s,:) = ipermute(IM_V,[4 1 2 3]);
    end
    % put back
    SliceGroup_t(cnt_g).SM = STK_V(:,:,:,2:nn:(end-nn));
end

%%

% verify
if false
    % echo
    disp(['verifying'])
    % slice
    cnt_g = 2; cnt_s = 1;
    % reference rotations
    IM_T = permute(SliceGroup(cnt_g).SM(:,:,cnt_s,:),[1 2 4 3]);
    [~,Reg_T] = stack_reg(IM_T);      
    T_theta = squeeze(asind(Reg_T(2,1,:)));
    T_tvec = squeeze(Reg_T(3,1:2,:))';
    T_ind = linspace(1,numel(T_theta),numel(T_theta))';
    % interpolated rotations
    IM_Q = permute(SliceGroup_t(cnt_g).SM(:,:,cnt_s,:),[1 2 4 3]);
    [~,Reg_Q] = stack_reg(IM_Q); 
    Q_theta = squeeze(asind(Reg_Q(2,1,:)));
    Q_tvec = squeeze(Reg_Q(3,1:2,:))';
    Q_ind = linspace(1,numel(T_theta),numel(Q_theta))';
    % plot
    figure
    hold on
    plot(Q_ind,Q_theta,'-b')
    plot(Q_ind,squeeze(asind(Reg_Q(2,1,:))),'-or')
    plot(T_ind,T_theta,'*b')
    title('rotation')
    xlabel('frame')
    ylabel('rotation (deg)')
    figure
    hold on
    plot(Q_ind,Q_tvec,'-b')
    plot(Q_ind,squeeze((Reg_Q(3,1:2,:))),'-or')    
    plot(T_ind,T_tvec,'*b')
    title('offset')
    xlabel('frame')
    ylabel('position [mm]')
    % 
    error('stopped at verification')
end

%% check

if true
    figure
    cnt_g = 3;
    for cnt_s = 8%:numel(Ip)
        % create sequence
        
        s1 = SliceGroup(cnt_g).S1(:,:,cnt_s);
        s2 = SliceGroup(cnt_g).S2(:,:,cnt_s);
        s3 = SliceGroup(cnt_g).S3(:,:,cnt_s);

        % IM_V = permute(SliceGroup(cnt_g).SM(:,:,cnt_s,:),[1 2 4 3]);
        IM_V = permute(SliceGroup_t(cnt_g).SM(:,:,cnt_s,:),[1 2 4 3]);
        for cnt_t = 1:size(IM_V,3)
            IM_s = abs( IM_V(:,:,cnt_t) );
            clf
            slice_view_v4(s1,s2,s3,IM_s);
            title(num2str(cnt_t))
            caxis([1 150])
            view(0,90)
            
            drawnow
            pause(10e-3)
        end
    end
    error('stopped at check, don''t forget to save')
end

%% save

disp(['interpolated groups saved in: ' analysis_folder '/' 'SliceGroup_TI'])
SliceGroup = SliceGroup_t;
save([analysis_folder '/' 'SliceGroup_TI'],'SliceGroup')

% remove t interpolation library
rmpath(mfilesfolder)

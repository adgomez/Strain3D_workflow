% Interpolates images in space using pixelwize complex values. This scripts
% operates on SliceGroups. This script was developed for tag removal.

%% housekeeping

clc
close all force
clear all

% latest: references to PARAM struct. 
% TODO: zeropad kspace when slice matrices are not square
% TODO: accel-based reco

% NOTE: <I/O>
% reads SliceVars (mat) / writes TAG_t(mat)

%% analysis folder path

enable_folders

%% extract parameters

interp_groups = PARAM.tag.interp_groups;
reco_type = PARAM.tag.reco_type;
tag_spacing_l = PARAM.tag.spacing_l;
tag_spacing_px = PARAM.tag.spacing_px;

% specific
use_abs = PARAM.tag.use_abs_L;
int_method = PARAM.tag.int_method_L;
ext_method = PARAM.tag.ext_method_L;
    
Q_trans = PARAM.tag.trans;

%% load SliceGroups

sv_folder = analysis_folder;
sv_file = 'SliceGroup_TI';
if exist([sv_folder filesep sv_file '.mat'],'file') ~= 2
    sv_file = 'SliceGroup'; % add to _SYNTH or _FIXED if needed
end
disp(['time interpolation using ' sv_file])
load([sv_folder '/' sv_file])

%% preliminary check

for cnt_g = 1:size(SliceGroup,2)
    % sensitivity matrix construction
    N(cnt_g,:) = SliceGroup(cnt_g).tagDir;   
end

% warn for incomplete groups
if (rank(N) < 3)
    warning('Slicegroups are insufficient for 3D reconstruction') 
end

%% target coordinate system

% encoding directions
enc_dir = [(1:size(N,1))' N*[1;2;3]];

% arrange reconstruction order in x,y,z
[~,Ia] = sort(enc_dir(interp_groups,2));
Ip = interp_groups(Ia);

% define Q coordinates
[Q1,Q2,Q3] = ndgrid(SliceGroup(Ip(1)).q + Q_trans(1),SliceGroup(Ip(2)).q + Q_trans(2),SliceGroup(Ip(3)).q + Q_trans(3));
  
% bounding box
q_box = [min([Q1(:) Q2(:) Q3(:)]) 
         max([Q1(:) Q2(:) Q3(:)])];
     
q_res = [Q1(2,1,1) Q2(1,2,1) Q3(1,1,2)] - [Q1(1,1,1) Q2(1,1,1) Q3(1,1,1)];

%% interpolate
% some parameters
interp_slices = true;
check_result = true;

% variable for progressbar
cnt_i_N = size(Ip,2)*size(SliceGroup(Ip(1)).SM,4);
cnt_i = 0;

if interp_slices
   
    Q_tag_pl = [];
    Q_tag_px = [];
    
    h = waitbar(cnt_i,'Please wait...');
    for cnt_g = 1:size(Ip,2)
        
        % check for radial 
        if strcmp(SliceGroup(Ip(cnt_g)).type,'rad')
             warning('radial interpolation via scatter interpolation') 
            
            for cnt_t = 1:size(SliceGroup(Ip(cnt_g)).SM,4)       
                % get image stack
                Si = SliceGroup(Ip(cnt_g)).SM(:,:,:,cnt_t);
                % apply absolute value
                if use_abs 
                    Si = abs(Si);
                end
                % interpolate
                F = scatteredInterpolant(SliceGroup(Ip(cnt_g)).S1(:), ...
                                         SliceGroup(Ip(cnt_g)).S2(:), ...
                                         SliceGroup(Ip(cnt_g)).S3(:), ...
                                         Si(:),int_method,ext_method);
                TMP = F(Q1,Q2,Q3);
                TMP(isnan(TMP)) = 0;
                str_varn = ['QM' num2str(cnt_g)];
                str_cm = [str_varn ' = TMP;'];
                eval(str_cm)
                % save
                disp(['saving: ' 'TAG' num2str(cnt_g) '_L_' num2str(cnt_t)])
                save([interp_folder '/' 'TAG' num2str(cnt_g) '_L_' num2str(cnt_t)], ...
                      'Q1','Q2','Q3',str_varn)
                % waitbar 
                cnt_i = cnt_i + 1;
                waitbar(cnt_i/cnt_i_N,h)
            end
        else
            % get grid coordinates
            [~, ~, ~, F_fwd,F_inv] = cart2im(SliceGroup(Ip(cnt_g)).S1, ...
                                             SliceGroup(Ip(cnt_g)).S2, ...
                                             SliceGroup(Ip(cnt_g)).S3);
            % unpack image
            IM = SliceGroup(Ip(cnt_g)).SM;       
            % apply absolute value
            if use_abs 
                IM = abs(IM);
            end
            % get indices
            I_q = (F_fwd*([Q1(:) Q2(:) Q3(:) 0*Q1(:) + 1]'))';
            % get tag spec
            Q_tag_pl = [Q_tag_pl SliceGroup(cnt_g).tag_pl];
            Q_tag_px = [Q_tag_px SliceGroup(cnt_g).tag_px];
            % time loop
            for cnt_t = 1:size(IM,4)       
                % interpolate
                % NOTE: removing weighted extrapolation, which is good for tags, 
                % but not for label transfer 
                F = griddedInterpolant(IM(:,:,:,cnt_t),int_method,ext_method);
                TMP = reshape(F(I_q(:,1),I_q(:,2),I_q(:,3)),size(Q1));
                TMP(isnan(TMP)) = 0;
                % save
                str_varn = ['QM' num2str(cnt_g)];
                str_cm = [str_varn ' = TMP;'];
                eval(str_cm)
                % save
                disp(['saving: ' 'TAG' num2str(cnt_g) '_L_' num2str(cnt_t)])
                save([interp_folder '/' 'TAG' num2str(cnt_g) '_L_' num2str(cnt_t)], ...
                      'Q1','Q2','Q3',str_varn)
                % waitbar 
                cnt_i = cnt_i + 1;
                waitbar(cnt_i/cnt_i_N,h)
            end
        end
    end
    close(h)
    
    % check result
    if check_result
        
        view_v = [45 20];
        
        for cnt_g = 1:size(Ip,2)

            subplot(2,size(Ip,2),cnt_g)
            ortho_slice_v4(SliceGroup(Ip(cnt_g)).S1,SliceGroup(Ip(cnt_g)).S2,SliceGroup(Ip(cnt_g)).S3,SliceGroup(Ip(cnt_g)).SM(:,:,:,end),view_v)
            title(['slice group ' num2str(Ip(cnt_g))])
            
            subplot(2,size(Ip,2),size(Ip,2) + cnt_g)
            eval(['ortho_slice_v4(Q1,Q2,Q3,QM' num2str(cnt_g) ',view_v)'])
            title(['QM' num2str(cnt_g)])
        end
    end
    
end

















function [im_V,Reg] = stack_reg(IM_V)
% function [im_V,Reg] = stack_reg(IM_V)

% Configure rgistration
[optimizer, metric] = imregconfig('monomodal');
% initalize
im_V = zeros(size(IM_V));
im_V(:,:,1) = IM_V(:,:,1);  
[Ii,Ij] = ndgrid(1:size(IM_V,1),1:size(IM_V,2));
% initialize
Reg = zeros(3,3,size(IM_V,3));
Reg(:,:,1) = eye(3);
% registration loop
for cnt_t = 2:size(IM_V,3);
    cnt_t
    % get images from time sequence
    IM_s = IM_V(:,:,1);  
    IM_t = IM_V(:,:,cnt_t);
    % set input
    fixed  = abs(IM_s);
    moving = abs(IM_t);
    % register
    tform = imregtform(moving, fixed, 'rigid', optimizer, metric);
    % put transform in grid format used here 
    F_T = tform.T;
    F_T = [F_T(:,2) F_T(:,1) F_T(:,3:end)];
    F_T = [F_T(2,:); F_T(1,:); F_T(3:end,:)];
    % move image (this strategy allows preserving complex data)
    TEMP = [Ii(:) Ij(:) Ii(:)*0+1]*inv(F_T);
    im_V(:,:,cnt_t) = interpn(Ii,Ij,IM_t,reshape(TEMP(:,1),size(Ii)),reshape(TEMP(:,2),size(Ij)),'linear');
    % save transformation
    Reg(:,:,cnt_t) = tform.T;
end
% im_V(isnan(im_V)) = 0;
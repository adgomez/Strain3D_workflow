function F_T = tform2affine(tformT)
% F_T = tform2affine(tformT)

F_T = tformT;
F_T = [F_T(:,2) F_T(:,1) F_T(:,3:end)];
F_T = [F_T(2,:); F_T(1,:); F_T(3:end,:)];
function SG = RegisterToBaseline_v4(SD,SliceGroup,engine_folder)

rigidRegistration1 = [engine_folder filesep 'Utils' filesep 'RigidRegistration2D_v4.sh'];
rigidRegistration2 = [engine_folder filesep 'Utils' filesep 'RigidRegistration2D_test.sh'];

% initialize output
SG = SliceGroup;

% load masks
cd(SD)

% parameters 
res = [1.5 1.5 10]; % mm 

% Note - need to update ANTs path within the shell script

% for keeping track of progress
total = 0;
for ix = 1:length(SliceGroup)
    total = total + size(SliceGroup(ix).SM,3);
end

ii = 0;

if strcmp(SD(end),filesep) == 0
    SD = [SD filesep];
end

fclose('all');
H = waitbar(0,'Registration to baseline...');
for slgrp = 1:length(SliceGroup)
    SM = SliceGroup(slgrp).SM;
    numSlices = size(SM,3);
    
    for sl = 1:numSlices
        ii = ii + 1;
        
        % k-space
        im = abs(squeeze(SM(:,:,sl,:)));        
        numFrames = size(im,3);
        
        %%% write NIFTIs for registration %%%
        if sl < 10
            SL = ['Slice0' mat2str(sl)];
        else
            SL = ['Slice' mat2str(sl)];
        end
        
        tagGroup = ['tag' mat2str(slgrp)];        
        D1 = [SD tagGroup '_' SL];
        if exist(D1,'dir') ~= 7
            mkdir(D1)
        end
        
        cd(D1)
        for ix = 1:numFrames            
            if ix < 10
                IN = ['IN0' mat2str(ix)];
            else
                IN = ['IN' mat2str(ix)];
            end
            
            nmTG = [tagGroup '_' SL '_' IN '.nii'];            
            WriteNIFTI(nmTG,res,im(:,:,ix));
        end
        
        % the following line was changed to avoid pesky time stamp
        % preservation--watch out windows users!
        eval(['! cp ' [tagGroup '_' SL '_IN01.nii'] ' ' ['t' tagGroup '_' SL '_IN01.nii']])

        %%% register frames 2 through N to frame 1
        waitbar(1/numFrames,H,['Rigid Registration of image ' mat2str(ii) ' of ' mat2str(total)]);
        for ix = 2:numFrames
            fixedTag = [tagGroup '_' SL '_IN01.nii'];
            
            if ix < 10
                IN = ['IN0' mat2str(ix)];
            else
                IN = ['IN' mat2str(ix)];
            end
            movingTag = [tagGroup '_' SL '_' IN '.nii'];
            
            if ix == 2
                txt = ['!' rigidRegistration1 ' ' fixedTag ' ' movingTag ' ' D1];               
                tform = [movingTag(1:(end-4)) '_to_' fixedTag(1:(end-4)) '0GenericAffine.mat'];
            else                
                txt = ['!' rigidRegistration2 ' ' fixedTag ' ' movingTag ' ' D1 ' ' tform];                
                tform = [movingTag(1:(end-4)) '_to_' fixedTag(1:(end-4)) '0GenericAffine.mat'];
            end
            eval(txt)
            
            waitbar(ix/numFrames,H,['Rigid Registration of image ' mat2str(ii) ' of ' mat2str(total)]);
        end
        
        % organize registered data
        lst = dir('ttag*nii');
        
        tag = zeros(size(im));        
        for ix = 1:length(lst)
            nm = lst(ix).name;
            N = nifti(nm);
            tag(:,:,ix) = N.dat(:,:);
            clear N
        end
        
        SG(slgrp).SM(:,:,sl,:) = tag;
        
        cd(SD)
    end
end
close(H)


end
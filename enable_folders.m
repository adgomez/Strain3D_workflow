% version 2: implementing analysis folder outside strain engine

% TODO: make folders into a structure

%% working folder locations

% data folder: data for all subjects  
% data_folder = '/iacl/pg17/brain_biomech/Processing';
% data_folder = '/home/dgomez/davidmeows/pg17/dgomez/Brain_Biomech_X3/HARP';
data_folder = '/home/knutsenak/Processing';

% engine folder: the strain3D routines
engine_folder = '/home/dgomez/davidmeows/Brain_Mechanics/Strain3D_workflow';

% put engine folder on path 
path(path,engine_folder);

%% subject specific information

% subject ID: subject in question
subject_ID = 'S043_3_drop';
% subject_ID = 'PV01_lin';

% analysis folder: subject data 
analysis_folder = [data_folder '/' subject_ID];

% ckeck existence of subject file
if ~exist(analysis_folder,'dir')
    
    % create folder
    mkdir(analysis_folder) 
    
    % create parameter definition script with default parameters
    d_param_file = 'define_param.m';
    fileID = fopen([analysis_folder '/' d_param_file],'w+');
    write_default_param
    fclose(fileID);
        
    % copy default paramters
    d_param_file_txt = 'default_param.txt';
    system(['cat '  analysis_folder '/' d_param_file ' ' ...
                    engine_folder '/' d_param_file_txt ' > ' ...
                    analysis_folder '/' 'temp.txt']);
    system(['mv ' analysis_folder '/' 'temp.txt' ' ' analysis_folder '/' d_param_file]);
    
    % open parameter definition script
    edit([analysis_folder '/' d_param_file])
    
else
    % execute paramter definition script
    this_folder = pwd;
    cd(analysis_folder)
    define_param
    cd(this_folder);
end

% add utilities 
cd(engine_folder)
niifolder = [engine_folder '/' 'Utils'];
addpath(genpath(niifolder));
cd(this_folder)

%% internal processing structure

% create folder for interpolated images
interp_folder = [analysis_folder '/' 'interp' ];
if ~exist(interp_folder,'dir')
    mkdir(interp_folder)
end

% create folder for time interpolated images
t_interp_folder = [analysis_folder '/' 't_interp' ];
if ~exist(interp_folder,'dir')
    mkdir(interp_folder)
end

% create folder for filtered images
filt_folder = [analysis_folder '/' 'filt' ];
if ~exist(filt_folder,'dir')
    mkdir(filt_folder)
end

% define folder for performing HARP-FE 
HFE_folder = [engine_folder '/'  '3_Tag_Tracking/HARP_FE'];
if ~exist(HFE_folder,'dir')
    mkdir(HFE_folder)
end

% define folder for performing 3DHARP
H3D_folder = [analysis_folder '/'  '3DHARP'];
if ~exist(H3D_folder,'dir')
    mkdir(H3D_folder)
end

% % define folder for performing HARP-FE 
% HFE_folder = [engine_folder '/'  '3_Tag_Tracking/HARP_FE'];
% if ~exist(HFE_folder,'dir')
%     mkdir(HFE_folder)
% end

% create folder for vtk output
VTK_folder = [analysis_folder '/' 'vtk'];
if ~exist(VTK_folder,'dir')
    mkdir(VTK_folder)
end

% create folder for DTI info
DTI_folder = [analysis_folder '/' 'DTI'];
if ~exist(DTI_folder,'dir')
    mkdir(DTI_folder)
end

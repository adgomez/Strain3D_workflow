function WriteNIFTI(name,res,im)

if size(res,1) == 4 && size(res,2) == 4
    mat = res;
else
    mat = [-res(1) 0 0 0; 0 -res(2) 0 0; 0 0 res(3) 0; 0 0 0 1];
end

if length(size(im)) == 3
      dat = file_array;
      
      dat.fname = name;
      dat.dim = size(im);
      dat.dtype = 'FLOAT32-LE';
      dat.offset = 352;
      dat.scl_slope = 1;
      dat.scl_inter = 0;
      
      N = nifti;
      
      N.dat = dat;
      N.mat0 = mat;
      N.mat0_intent = 'Scanner';
      N.mat0 = N.mat;
      N.mat0_intent = 'Aligned';
      
      create(N);
      
      N.dat(:,:,:) = im;
      
      clear dat N
      
elseif length(size(im)) == 4
      dat = file_array;
      
      dat.fname = name;
      dat.dim = size(im);
      dat.dtype = 'FLOAT32-LE';
      dat.offset = 352;
      dat.scl_slope = 1;
      dat.scl_inter = 0;
      
      N = nifti;
      
      N.dat = dat;
      N.mat0 = mat;
      N.mat0_intent = 'Scanner';
      N.mat0 = N.mat;
      N.mat0_intent = 'Aligned';
      
      create(N);
      
      N.dat(:,:,:,:) = im;
      
      clear dat N
      
elseif length(size(im)) == 2
    dat = file_array;
      
      dat.fname = name;
      dat.dim = size(im);
      dat.dtype = 'FLOAT32-LE';
      dat.offset = 352;
      dat.scl_slope = 1;
      dat.scl_inter = 0;
      
      N = nifti;
      
      N.dat = dat;
      N.mat0 = mat;
      N.mat0_intent = 'Scanner';
      N.mat0 = N.mat;
      N.mat0_intent = 'Aligned';
      
      create(N);
      
      N.dat(:,:) = im;
      
      clear dat N
end

end
function h = vector_view_v2(X,Y,Z,U,V,W,n,S,DIM,option,LINESPEC)
% h = vector_view_v2(X,Y,Z,U,V,W,n,S,DIM,option,LINESPEC)
% Shows slice S of a vector field in the direction dim. S can be a vector 
% containing the desired slice numbers to be shown.
% option 1 = no borders
% option 2 = invert arrows
% option 3 = 1 and 2
% 
% version 2 includes support for individual slices. set DIM = 0, and the
% routine will draw S normal to the smallest direction

if option == 1
    X = X(2:end-1,2:end-1,2:end-1);
    Y = Y(2:end-1,2:end-1,2:end-1);
    Z = Z(2:end-1,2:end-1,2:end-1);
    U = U(2:end-1,2:end-1,2:end-1);
    V = V(2:end-1,2:end-1,2:end-1);
    W = W(2:end-1,2:end-1,2:end-1);
    S = S-1;
elseif option == 2
    X = X+U;
    Y = Y+V;
    Z = Z+W;
    U = -U;
    V = -V;
    W = -W;
elseif option == 3
    X = X(2:end-1,2:end-1,2:end-1);
    Y = Y(2:end-1,2:end-1,2:end-1);
    Z = Z(2:end-1,2:end-1,2:end-1);
    U = U(2:end-1,2:end-1,2:end-1);
    V = V(2:end-1,2:end-1,2:end-1);
    W = W(2:end-1,2:end-1,2:end-1);
    S = S-1;
    X = X+U;
    Y = Y+V;
    Z = Z+W;
    U = -U;
    V = -V;
    W = -W;
    
end

if DIM == 0
   DIM = 3;
end

if DIM == 1
    Xa = X(S,1:n:end,1:n:end);
    Ya = Y(S,1:n:end,1:n:end);
    Za = Z(S,1:n:end,1:n:end);
    Ua = U(S,1:n:end,1:n:end);
    Va = V(S,1:n:end,1:n:end);
    Wa = W(S,1:n:end,1:n:end);
    view(90,0)
elseif DIM == 2    
    Xa = X(1:n:end,S,1:n:end);
    Ya = Y(1:n:end,S,1:n:end);
    Za = Z(1:n:end,S,1:n:end);
    Ua = U(1:n:end,S,1:n:end);
    Va = V(1:n:end,S,1:n:end);
    Wa = W(1:n:end,S,1:n:end);  
    view(0,0)
elseif DIM == 3
    Xa = X(1:n:end,1:n:end,S);
    Ya = Y(1:n:end,1:n:end,S);
    Za = Z(1:n:end,1:n:end,S);
    Ua = U(1:n:end,1:n:end,S);
    Va = V(1:n:end,1:n:end,S);
    Wa = W(1:n:end,1:n:end,S);   
    view(0,-90)
else
    error('dimension requested is more than 3')
end
    
if nargin == 10
    quiver3(Xa,Ya,Za,Ua,Va,Wa,0)
else
    quiver3(Xa,Ya,Za,Ua,Va,Wa,0,LINESPEC)
end
axis equal     
axis tight








% 
% 
% 
% 
% %
% % slice_view_v3(IM) shows center slice of stack IM in the third direction.
% % coorinates are assigned automatically x = row, y = column, z =  slice 
% %
% % slice_view_v3(IM,S) shows slice S of IM in the third direction. Axis are
% % assigned as in slice_view_v3(IM)
% % 
% % slice_view_v3(IM,S,DIM) shows slice S of IM in the direction DIM (1=x, 
% % 2=y, and 3=z). Axis are assigned as in slice_view_v3(IM)
% %
% % slice_view_v3(X,Y,Z,IM) is equivalent to slice_view_v3(IM) except with
% % specified spatial coordinates.
% %
% % slice_view_v3(X,Y,Z,IM,S) is equivalent to slice_view_v3(IM,S) except 
% % with specified spatial coordinates.
% %
% % slice_view_v3(X,Y,Z,IM,S,DIM) is equivalent to slice_view_v3(IM,S,DIM)
% % except with specified spatial coordinates.
% 
% dim = size(X);
% 
% % compatibility for 2D data (only for nargin==1)
% if length(dim)==2
%     dim(3) = 2;
% end
% 
% % parse inputs to allow multiple uses
% if nargin==1
%     IM = X;
%     [X,Y,Z] = ndgrid(1:dim(1),1:dim(2),1:dim(3));
%     S = round(dim(3)/2);
%     DIM = 3;
% elseif nargin==2
%     IM = X;
%     S = Y;
%     [X,Y,Z] = ndgrid(1:dim(1),1:dim(2),1:dim(3));
%     DIM = 3; 
% elseif nargin==3
%     IM = X;
%     S = Y;
%     DIM = Z;
%     [X,Y,Z] = ndgrid(1:dim(1),1:dim(2),1:dim(3));
% elseif nargin==4
%     S = round(dim(3)/2);
%     DIM = 3;
% elseif nargin==5
%     DIM = 3;
% end
% 
% % select dimension
% % clf reset % clear current figure
% switch DIM
%     case 3
%         hold on
%         for cnt_s = 1:length(S)
%             ax = X(:,:,S(cnt_s));        
%             ay = Y(:,:,S(cnt_s));
%             az = Z(:,:,S(cnt_s));
%             am = IM(:,:,S(cnt_s));
%             h = slice_view_v2(ax,ay,az,am);
%             if (length(S)>1)
%                 view(40,20)          
%                 axis equal
%                 axis vis3d
%             else
%                 axis equal     
%                 axis tight
%                 % axis([min(min(ax)) max(max(ax)) min(min(ay)) max(max(ay))])
%                 view(0,-90)
%             end
%         end
%         % hold off
%      case 2
%         hold on
%         for cnt_s = 1:length(S)
%             ax = squeeze(X(:,S(cnt_s),:));        
%             ay = squeeze(Y(:,S(cnt_s),:));
%             az = squeeze(Z(:,S(cnt_s),:));
%             am = squeeze(IM(:,S(cnt_s),:));
%             h = slice_view_v2(ax,ay,az,am);
%             if (length(S)>1)
%                 view(40,20)
%                 axis equal                
%                 axis vis3d
%             else
%                 axis equal
%                 axis tight
%                 view(0,0)
%             end
%         end
%         % hold off
%       case 1
%         hold on
%         for cnt_s = 1:length(S)
%             ax = squeeze(X(S(cnt_s),:,:));        
%             ay = squeeze(Y(S(cnt_s),:,:));
%             az = squeeze(Z(S(cnt_s),:,:));
%             am = squeeze(IM(S(cnt_s),:,:));
%             h = slice_view_v2(ax,ay,az,am);
%             if (length(S)>1)
%                 view(40,20)
%                 axis equal
%                 axis vis3d
%             else
%                 axis equal
%                 axis tight
%                 view(90,0)
%             end
%         end
%         % hold off
%     otherwise
%         error('Only 3D data is supported')    
% end
% 
% end
% 
% function h = slice_view_v2(ax,ay,az,am)
% % shows an image slice
% % USAGE slice_view_v2(ax,ay,az,am)
% % INPUTS ax, ay and az are the coordinates and am is
% % the image data
% 
% dim = size(am);
% 
% % doublesampled averaged
% bx = ave_mat_v1(ax);
% by = ave_mat_v1(ay);
% bz = ave_mat_v1(az);
% 
% % nearest neighbor (doubled)
% dm = zeros(2*dim(1),2*dim(2));
% dm(1:2:end,1:2:end) = am;
% dm(2:2:end,1:2:end) = am;
% dm(:,2:2:end) = dm(:,1:2:end);
% dm = dm(2:end,2:end); 
% 
% % % smoothing (an add on)
% % bm = ave_mat_v1(am);
% % cm = am;
% % cm(1:dim(1)-1,1:dim(2)-1) = bm(2:2:end,2:2:end);
% 
% h = surf(bx,by,bz,dm);
% % h = surf(bx,by,bz,bm);
% % h = surf(ax,ay,am,cm)'
% 
% colormap gray
% shading flat
% % axis equal
% % axis([min(min(ax)) max(max(ax)) ...
% %       min(min(ay)) max(max(ay)) ])
% %   view(0,-90)
% xlabel('x(i) ')
% ylabel('y(j) ')
% zlabel('z(k) ')
%   
% end
% 
% function Xm = ave_mat_v1(X)
% 
% dim = size(X);
% I = dim(1);
% J = dim(2);
% 
% ax = X;
% bx = zeros(2*I,2*J);
% bx(1:2:end,1:2:end) = ax;
% dxx = diff(ax,1,1);
% bx(2:2:2*I-2,1:2:2*J) = ax(1:I-1,:) + 0.5*dxx;
% dxy = diff(ax,1,2);
% bx(1:2:2*I,2:2:2*J-1) = ax(:,1:J-1) + 0.5*dxy;
% dxx = diff(bx(1:2:2*I,2:2:2*J-1),1,1);
% bx(2:2:2*I-1,2:2:2*J-1) = bx(1:2:2*I-2,2:2:2*J-1) + 0.5*dxx;
% bx = bx(1:end-1,1:end-1);
% 
% ay = X;
% by = zeros(2*I,2*J);
% by(1:2:end,1:2:end) = ay;
% dyy = diff(ay,1,2);
% by(1:2:2*I,2:2:2*J-2) = ay(:,1:J-1)+ 0.5*dyy;
% dyx = diff(ay,1,1);
% by(2:2:2*I-2,1:2:2*J) = ay(1:I-1,:) + 0.5*dyx;
% dyy = diff(by(2:2:2*I-2,1:2:2*J),1,2);
% by(2:2:2*I-1,2:2:2*J-1) = by(2:2:2*I-1,1:2:2*J-2) + 0.5*dyy;
% by = by(1:end-1,1:end-1);
% 
% Xm = 0.5*(bx + by);
% 
% end

#!/bin/bash

##############################################
### Rigid registration of PET to CT images ###
##############################################

# Usage: ANTs_PETtoCT.sh fixedImage movingImage intputDirectory outputDirectory

ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=4  # controls multi-threading
export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS

# path to ANTs binaries
AP=/usr/ANTs-1.9.4/bin

fixed=$1
moving=$2
outputDir=$3
tagged=$4

fBase=`basename ${fixed}`
fName=` echo ${fBase} | cut -d '.' -f 1 `

mBase=`basename ${moving}`
mName=` echo ${mBase} | cut -d '.' -f 1 `

tBase=`basename ${tagged}`
tName=` echo ${tBase} | cut -d '.' -f 1 `

nm=${outputDir}/${mName}_to_${fName}

its=5000x2500x1500

tol=5e-8
interp=BSpline

# Registration
${AP}/antsRegistration -d 2 -r [ $fixed, $moving ,0] -m mattes[  $fixed, $moving , 1 , 32, regular, 0.3 ] -t rigid[ 0.1 ] \
	-c [$its,${tol},20] -s 4x2x0vox -f 4x2x1 -l 1 -u 1 -z 1 -v 0 -o ${nm}

# Warp moving to fixed image
${AP}/antsApplyTransforms -d 2 -n ${interp} -i ${moving} -r ${fixed} -t ${nm}0GenericAffine.mat -o ${outputDir}/t${mName}.nii
${AP}/antsApplyTransforms -d 2 -n ${interp} -i ${tagged} -r ${fixed} -t ${nm}0GenericAffine.mat -o ${outputDir}/t${tName}.nii



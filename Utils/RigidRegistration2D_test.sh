#!/bin/bash

##############################################
### Rigid registration of PET to CT images ###
##############################################

# Usage: ANTs_PETtoCT.sh fixedImage movingImage intputDirectory outputDirectory

ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=6  # controls multi-threading
export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS

AP=/usr/ANTs-1.9.4/bin

tagFixed=$1
tagMoving=$2
outputDir=$3
initTransform=$4

fBase=`basename ${tagFixed}`
fName=` echo ${fBase} | cut -d '.' -f 1 `

mBase=`basename ${tagMoving}`
mName=` echo ${mBase} | cut -d '.' -f 1 `

nm=${outputDir}/${mName}_to_${fName}

its=2000x1000x500x250
smooth=3x2x1x0vox
factors=8x4x2x1

tol=1e-6
interp=BSpline

# Registration
${AP}/antsRegistration -d 2 -r $initTransform -m mattes[ $tagFixed, $tagMoving , 1 , 32, regular, 0.3 ] -t rigid[ 0.1 ] -c [$its,${tol},20] -s ${smooth} -f ${factors} -l 1 -u 1 -z 1 -v 0 -o ${nm}

# Warp moving to fixed image
${AP}/antsApplyTransforms -d 2 -n ${interp} -i ${tagMoving} -r ${tagFixed} -t ${nm}0GenericAffine.mat -o ${outputDir}/t${mName}.nii





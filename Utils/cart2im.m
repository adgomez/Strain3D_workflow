function [I1,I2,I3,F_fwd,F_inv] = cart2im(S1,S2,S3)
%  [I1,I2,I3,F_fwd,F_inv] = cart2im(S1,S2,S3) transformation from cartesian 
% coordinates to image indexes in ndgrid. Handy for interpolation
% 
% S1, S2, S3 =  cartesian coordinates
% 
% I1, I2, I3, image coordinates
% F_fwd = transforms S to I
% F_inv = transforms I to S

% ADG 2016-12-07

%% get transforms
s_dim = size(S1);

L = [S1(1,1,1)   S2(1,1,1)   S3(1,1,1)  
     S1(end,1,1) S2(end,1,1) S3(end,1,1)
     S1(1,end,1) S2(1,end,1) S3(1,end,1)
     S1(1,1,end) S2(1,1,end) S3(1,1,end)];

l = [1        1        1       
     s_dim(1) 1        1       
     1        s_dim(2) 1       
     1        1        s_dim(3)];

M = [ L   L*0 L*0 ones(4,1)  zeros(4,1) zeros(4,1) 
      L*0 L   L*0 zeros(4,1) ones(4,1)  zeros(4,1)  
      L*0 L*0 L   zeros(4,1) zeros(4,1) ones(4,1)];
  
f = inv(M)*l(:);     

F_fwd = [reshape(f(1:9),3,3)' f(10:12); 0 0 0 1];
F_inv = inv(F_fwd);

% check
% F_fwd*[L' ; ones(1,4)]
% l'
% F_inv*[l' ; ones(1,4)]
% L'

%% image coordinates

[I1,I2,I3] = ndgrid(1:s_dim(1),1:s_dim(2),1:s_dim(3));


end
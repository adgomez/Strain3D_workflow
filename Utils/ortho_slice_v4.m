function ortho_slice_v4(X1,X2,X3,IM,view_v,per)
% ortho_slice_v4(X1,X2,X3,IM,view_v,per)

    if nargin==1
        IM = X1;
        dim = size(IM);
        [X1,X2,X3] = ndgrid(1:dim(1),1:dim(2),1:dim(3));
        view_v = [45,45];
        per = [0.5, 0.5, 0.5];
    end

    if nargin<5
        view_v = [45,45];
        per = [0.5, 0.5, 0.5];
    end
    
    if nargin<6
        per = [0.5, 0.5, 0.5];
    end
    
    if isempty(view_v)
        view_v = [45,45];
    end
    
    dim = size(IM);

    hold on
    slice_view_v4(X1,X2,X3,IM,round(dim(1)*per(1)),1);
    slice_view_v4(X1,X2,X3,IM,round(dim(2)*per(2)),2);
    slice_view_v4(X1,X2,X3,IM,round(dim(3)*per(3)),3);
    
    view(view_v)
    
    xlabel('x(i) ')
    ylabel('y(j) ')
    zlabel('z(k) ')
    
end

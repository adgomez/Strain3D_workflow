clc
% close all
clear all

% latest: testing with unwrapped images
% TODO: enable tracking initialization 

%% analysis folder path

enable_folders

%% NOTES

% 3DHARP Solution
run_n = 21; 

% This is a string to identify the results
PARAM.comments.H3D = [ 'tracking real data, video phantom mesh' ...
                       'slab filter'];

%% add 3DHARP library to search path

HARPfolder = 'M_files';
path(path,HARPfolder);

%% tracking loop

% load mesh
FE_folder = analysis_folder;
FE_file = 'mesh_tag_1';
load([FE_folder '/' FE_file])
ref_nodes = nodes;

t_steps = 13;
n_nodes = numel(ref_nodes(:,1));
nod_u = zeros(n_nodes,3,t_steps + 1);


sequential_track = false;

%% track seed over all timeponts
% TODO: save residual

for cnt_t = 1:t_steps
    cnt_t
    % *** initialize mesh
    if cnt_t == 1
        % refrence nodes
        nodes = ref_nodes;   
    else
        % refrence nodes for template evaluation
        Ip = 1:numel(nodes(:,1));
        
        % update
        % nodes(Ip,2:4) = ref_nodes(Ip,2:4); % absolute (no update) 
        % nodes(Ip,2:4) = ref_nodes(Ip,2:4) + nod_u(Ip,:,cnt_t); % sequential updates   
        
        % incremental update
        nod_u_inc = nod_u(Ip,:,cnt_t) - nod_u(Ip,:,cnt_t-1);
        % nodes(Ip,2:4) = nodes(Ip,2:4) + nod_u_inc;
        
        % import update (this can just go outside the if else)
        load([analysis_folder '/' 'trans_TAG_N_TAG_1'])
        N_nod = [ref_nodes(Ip,2:4) ref_nodes(Ip,1)*0+1]*inv(F(:,:,cnt_t+1));
        nodes(Ip,2:4) = N_nod(Ip,1:3) + 0.5*nod_u_inc*(cnt_t==4) + 0.5*nod_u_inc*(cnt_t==5);
    end
    
    % *** tracking setup
    s = cnt_t + 1; % target
    if sequential_track
        t = cnt_t % sequential template
    else
        t = 1; % absolute template
    end

    % *** load phase images
    load([filt_folder '/' 'int_H_vars_' num2str(t)])
    PHI1 = HP1;
    PHI2 = HP2;
    PHI3 = HP3;
    load([filt_folder '/' 'int_H_vars_' num2str(s)])
    phi1 = HP1;
    phi2 = HP2;
    phi3 = HP3;

    % *** perform HARP tracking 
    % construct 
    H3D = HARP3D_MPT_v4(Q1,Q2,Q3,PHI1,PHI2,PHI3,phi1,phi2,phi3);
    % conventional tracking
    itr = 10;
    [xn(:,1),xn(:,2),xn(:,3)] = H3D.tracki(ref_nodes(:,2),ref_nodes(:,3),ref_nodes(:,4), ...
                                          nodes(:,2),nodes(:,3),nodes(:,4),itr);
    % save Lagrangian displacements
    nod_u(:,:,cnt_t + 1) = xn - ref_nodes(:,2:4); 
        
    % *** check point (for reference)
    % TODO: this can be used to calculate residual in the future
    In = [7500]; %35000 104212
    target = [H3D.FP1.winterp(ref_nodes(In,2),ref_nodes(In,3),ref_nodes(In,4)), ...
              H3D.FP2.winterp(ref_nodes(In,2),ref_nodes(In,3),ref_nodes(In,4)), ...
              H3D.FP3.winterp(ref_nodes(In,2),ref_nodes(In,3),ref_nodes(In,4)),]
     % check point
    def_template = [H3D.Fp1.winterp(xn(In,1),xn(In,2),xn(In,3)) 
                    H3D.Fp2.winterp(xn(In,1),xn(In,2),xn(In,3)) 
                    H3D.Fp3.winterp(xn(In,1),xn(In,2),xn(In,3))]'      
end


%% check

if false

    nn = 10;

    In = (1:nn:size(nodes,1))';
    
    figure

    for cnt_t = 1:t_steps
        clf
        % ortho_slice_v4(Q1,Q2,Q3,HM1 + HM2 + HM3)
        % quiver3(ref_nodes(In,2),ref_nodes(In,3),ref_nodes(In,4), ...
        %         nod_u(In,1,cnt_t),nod_u(In,2,cnt_t),nod_u(In,3,cnt_t),0,'r')
        scatter3(ref_nodes(In,2) + nod_u(In,1,cnt_t), ...
                 ref_nodes(In,3) + nod_u(In,2,cnt_t), ...
                 ref_nodes(In,4) + nod_u(In,3,cnt_t),'.b')
        grid on
        axis equal
        view(-180,90)
        axis([min(ref_nodes(:,2)) max(ref_nodes(:,2)), ...
              min(ref_nodes(:,3)) max(ref_nodes(:,3)), ...
              min(ref_nodes(:,4)) max(ref_nodes(:,4))])
        drawnow
        pause(0.5)
    end
    return
end

%% save parameters structure

save([H3D_folder '/' '3DHARP_parameters'],'PARAM')

%% save

% preview
save([H3D_folder '/' 'nodal_disp_3DH_v' num2str(run_n)],'nod_u')

% run where all states are prescribed displacements
dim_u = size(nod_u);
out_u = nodes(:,1);
for cnt_t = 1:dim_u(3)
    out_u = cat(2,out_u,nod_u(:,:,cnt_t));
    % out_u = cat(2,out_u,nodes(:,2:4));
end
% dlmwrite([H3D_folder '/' '3DHARP_v' num2str(run_n) '.txt'],out_u,',');
dlmwrite([H3D_folder '/' '3DHARP_v' num2str(run_n) '.txt'],out_u,'delimiter',',','precision','%.6g');  
    

% echo save files
disp(['saved '])
disp(['nodal_disp_3DH_v' num2str(run_n)])
disp(['3DHARP_v' num2str(run_n) '.txt'])




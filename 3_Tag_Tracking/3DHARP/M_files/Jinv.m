
%% inverse
function [JN11,JN21,JN31,JN12,JN22,JN32,JN13,JN23,JN33] = Jinv(G11,G21,G31,G12,G22,G32,G13,G23,G33) 

    Jbar = Jdet(G11,G21,G31,G12,G22,G32,G13,G23,G33);
    
    JN11 = (G22.*G33 - G23.*G32)./Jbar; JN12 = (G13.*G32 - G12.*G33)./Jbar; JN13 = (G12.*G23 - G13.*G22)./Jbar; 
    JN21 = (G23.*G31 - G21.*G33)./Jbar; JN22 = (G11.*G33 - G13.*G31)./Jbar; JN23 = (G13.*G21 - G11.*G23)./Jbar; 
    JN31 = (G21.*G32 - G22.*G31)./Jbar; JN32 = (G12.*G31 - G11.*G32)./Jbar; JN33 = (G11.*G22 - G12.*G21)./Jbar; 
            
end

%% determinat
function Jbar = Jdet(G11,G21,G31,G12,G22,G32,G13,G23,G33) 

   Jbar = G11.*G22.*G33 + G12.*G23.*G31 + G13.*G21.*G32 ...
        - G13.*G22.*G31 - G12.*G21.*G33 - G11.*G23.*G32;
end

function G = gradientStar_v2(im)
% G = gradientStar_v2(im)

%%  wrapped gradient calculation
    
% initialize output
G = [];
    
if size(im,3)>1  
    
    % calculate gradients
    [FX,FY,FZ] = gradient(im); 
    [FXp,FYp,FZp] = gradient(wrap(im+pi));
    
    % locate wrapped locations
    ind = abs(FX)>abs(FXp); FX(ind) = FXp(ind);
    ind = abs(FY)>abs(FYp); FY(ind) = FYp(ind);
    ind = abs(FZ)>abs(FZp); FZ(ind) = FZp(ind);
    
    % store
    G(:,:,:,1) = FY; G(:,:,:,2) = FX; G(:,:,:,3) = FZ;    
else    
    
    % calculate gradients
    [FX,FY] = gradient(im); 
    [FXp,FYp] = gradient(wrap(im+pi));
    
    % locate wrapped locations
    ind = abs(FX)>abs(FXp); FX(ind) = FXp(ind);
    ind = abs(FY)>abs(FYp); FY(ind) = FYp(ind);
   
    % store
    G(:,:,1) = FY; G(:,:,2) = FX;
end

end
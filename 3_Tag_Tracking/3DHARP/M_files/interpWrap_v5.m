classdef interpWrap_v5
% F = interpWrap_v5(X,Y,Z,IM) (construct)
% im = F.winterp(x,y,z) (interpolate)
% NOTE: use data in ndgrid format

% version 5: trying to implement a faster point-wise version

%% class definition
   properties
        % phase image
        IM
   end
    
   properties (SetAccess = protected)
        % finite differences
        IMi
        IMj
        IMk
        IMij
        IMjk
        IMik
        IMijk
        % image properties
        res
        org
   end
    
    methods
        %% constructor
        function obj = interpWrap_v5(X,Y,Z,IM)
            % finite differences
            J = IM; J(1:end-1,:,:) = J(2:end,:,:); IMi = wrap(J - IM);
            J = IM; J(:,1:end-1,:) = J(:,2:end,:); IMj = wrap(J - IM);
            J = IM; J(:,:,1:end-1) = J(:,:,2:end); IMk = wrap(J - IM);
            J = IMi; J(:,1:end-1,:) = J(:,2:end,:); IMij = wrap(J - IMi);
            J = IMj; J(:,:,1:end-1) = J(:,:,2:end); IMjk = wrap(J - IMj);
            J = IMi; J(:,:,1:end-1) = J(:,:,2:end); IMik = wrap(J - IMi);
            J = IMij; J(:,:,1:end-1) = J(:,:,2:end); IMijk = wrap(J - IMij);
            
            %  resolution
            obj.res = [X(2,1,1) - X(1,1,1)
                   Y(1,2,1) - Y(1,1,1)
                   Z(1,1,2) - Z(1,1,1)];

            % origin
            obj.org = [min(X(:)) min(Y(:)) min(Z(:))]; 

            % set values
            obj.IM = IM;
            obj.IMi = IMi;
            obj.IMj = IMj;
            obj.IMk = IMk;
            obj.IMij = IMij;
            obj.IMjk = IMjk;
            obj.IMik = IMik;
            obj.IMijk = IMijk;
        end

        %% trilinear interpolation
        function im = winterp(obj,x,y,z)
            % find indeces
            i = 1 + (x - obj.org(1))/obj.res(1);
            j = 1 + (y - obj.org(2))/obj.res(2);
            k = 1 + (z - obj.org(3))/obj.res(3);

            % measure matrix extent
            dim = size(obj.IM);

            % select indexes in range
            Ir = (((1<=i)&(i<dim(1)))) & ...
                 (((1<=j)&(j<dim(2)))) & ...
                 (((1<=k)&(k<dim(3))));

            % get image locations 
            Ip = sub2ind(size(obj.IM),floor(i(Ir)),floor(j(Ir)),floor(k(Ir)));

            % define index resuduals 
            Ri = i - floor(i);
            Rj = j - floor(j);
            Rk = k - floor(k);

            % preallocate output
            im = i*nan;

            % interpolate
            im(Ir) = obj.IM(Ip) + ...
                     obj.IMi(Ip).*Ri(Ir) + ...
                     obj.IMj(Ip).*Rj(Ir) + ...
                     obj.IMk(Ip).*Rk(Ir) + ...
                     obj.IMij(Ip).*Ri(Ir).*Rj(Ir) + ...
                     obj.IMjk(Ip).*Rj(Ir).*Rk(Ir) + ...
                     obj.IMik(Ip).*Ri(Ir).*Rk(Ir) + ...
                     obj.IMijk(Ip).*Ri(Ir).*Rj(Ir).*Rk(Ir) ;

            % wrap
            im = wrap(im);
        end
    end
end


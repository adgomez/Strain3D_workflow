%% determinat
function Jbar = Jdet(G11,G21,G31,G12,G22,G32,G13,G23,G33) 

   Jbar = G11.*G22.*G33 + G12.*G23.*G31 + G13.*G21.*G32 ...
        - G13.*G22.*G31 - G12.*G21.*G33 - G11.*G23.*G32;
end
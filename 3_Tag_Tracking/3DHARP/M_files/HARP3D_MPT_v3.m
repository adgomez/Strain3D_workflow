function [U,x,ulist,xlist] = HARP3D_MPT_v3(S1,S2,S3,X,PHI1,PHI2,PHI3,phi1,phi2,phi3,total_iter)
% [U,x,ulist,xlist] = HARP3D_MPT_v3(S1,S2,S3,X,PHI1,PHI2,PHI3,phi1,phi2,phi3,total_iter)

%% HARP 3D
% version 3: solving for individual points using quasi-newton iterations. 
% Coordinates are in length units.
% NOTE: LG stands for Lagrangian

%% preliminaries

% relaxation coefficient
alpha = 0.9;

% zero jacobian tolerance
Jtol = 1e-5;

% initialize output
U = X*0;  
x = X*0;  
ulist = zeros([length(X) 3 total_iter+1]);
xlist = zeros([length(X) 3 total_iter+1]);
xlist(:,:,1) = X;

%% start fixed point algorithm

% *** set up geometrical domain
Y1 = X(:,1);  
Y2 = X(:,2);  
Y3 = X(:,3);

% *** initialization
% deformed coordinates 
y1 = Y1;  
y2 = Y2;  
y3 = Y3;

% phase values
P1 = interpWrap_v4(S1,S2,S3,PHI1,Y1,Y2,Y3);
P2 = interpWrap_v4(S1,S2,S3,PHI2,Y1,Y2,Y3);
P3 = interpWrap_v4(S1,S2,S3,PHI3,Y1,Y2,Y3);

%% gradients for quasi-newton iterations

g1 = gradientStar_v2(phi1); 
g2 = gradientStar_v2(phi2); 
g3 = gradientStar_v2(phi3);

Fg11 = griddedInterpolant(S1,S2,S3,g1(:,:,:,1)); Fg12 = griddedInterpolant(S1,S2,S3,g1(:,:,:,2)); Fg13 = griddedInterpolant(S1,S2,S3,g1(:,:,:,3));
Fg21 = griddedInterpolant(S1,S2,S3,g2(:,:,:,1)); Fg22 = griddedInterpolant(S1,S2,S3,g2(:,:,:,2)); Fg23 = griddedInterpolant(S1,S2,S3,g2(:,:,:,3));
Fg31 = griddedInterpolant(S1,S2,S3,g3(:,:,:,1)); Fg32 = griddedInterpolant(S1,S2,S3,g3(:,:,:,2)); Fg33 = griddedInterpolant(S1,S2,S3,g3(:,:,:,3));

G11 = Fg11(y1,y2,y3); G12 = Fg12(y1,y2,y3); G13 = Fg13(y1,y2,y3);
G21 = Fg21(y1,y2,y3); G22 = Fg22(y1,y2,y3); G23 = Fg23(y1,y2,y3);
G31 = Fg31(y1,y2,y3); G32 = Fg32(y1,y2,y3); G33 = Fg33(y1,y2,y3);

%% HARP3D loop


% *** phase tracking
H = waitbar(0,'fixed point iteration....');
for iter = 1:total_iter  
    
    % phase field interpolation
    p1 = interpWrap_v4(S1,S2,S3,phi1,y1,y2,y3);
    p2 = interpWrap_v4(S1,S2,S3,phi2,y1,y2,y3);
    p3 = interpWrap_v4(S1,S2,S3,phi3,y1,y2,y3);
    
    % differences
    dP1 = wrap(p1 - P1);
    dP2 = wrap(p2 - P2);
    dP3 = wrap(p3 - P3);

    % phase jacobian inverse
    [JN11,JN21,JN31,JN12,JN22,JN32,JN13,JN23,JN33] = Jinv(G11,G21,G31,G12,G22,G32,G13,G23,G33); 
    
    % calculation of displacements
    dx = JN11.*dP1 + JN12.*dP2 + JN13.*dP3;
    dy = JN21.*dP1 + JN22.*dP2 + JN23.*dP3;
    dz = JN31.*dP1 + JN32.*dP2 + JN33.*dP3;
    
    % determinant check 
    Jbar = Jdet(G11,G21,G31,G12,G22,G32,G13,G23,G33);
    dx(abs(Jbar) < Jtol) = 0; 
    dy(abs(Jbar) < Jtol) = 0; 
    dz(abs(Jbar) < Jtol) = 0;
    
    % nan check
    dx(isnan(dx)) = 0;
    dy(isnan(dy)) = 0;
    dz(isnan(dz)) = 0;
    
    % update estimate
    y1 = y1 - alpha*dx; 
    y2 = y2 - alpha*dy; 
    y3 = y3 - alpha*dz;
    
    % store
    ulist(:,:,iter+1) = [(y1 - Y1) (y2 - Y2) (y3 - Y3)];
    xlist(:,:,iter+1) = xlist(:,:,1) + ulist(:,:,iter+1);

    % TODO: termination
    
    % update 
    waitbar(iter/total_iter,H);
end
close(H)

U = ulist(:,:,end);
x = xlist(:,:,end);

end

function [ui,uj,uk] = HARP3D_LG_v1(PHI1,PHI2,PHI3,phi1,phi2,phi3,total_iter)
% [ui,uj,uk] = HARP3D_LG_v1(PHI1,PHI2,PHI3,phi1,phi2,phi3,total_iter)

%% HARP 3D
% version 1: solving for the whole grid using quasi-newton iterations. All
% coordinates are in pixels.
% NOTE: LG stands for Lagrangian

%% gradients for quasi-newton iterations

% zero jacobian tolerance
Jtol = 1e-5;

g1 = gradientStar_v2(phi1); 
g2 = gradientStar_v2(phi2); 
g3 = gradientStar_v2(phi3);

G11 = g1(:,:,:,1); G12 = g1(:,:,:,2); G13 = g1(:,:,:,3);
G21 = g2(:,:,:,1); G22 = g2(:,:,:,2); G23 = g2(:,:,:,3);
G31 = g3(:,:,:,1); G32 = g3(:,:,:,2); G33 = g3(:,:,:,3);

%% start fixed point algorithm

% *** set up geometrical domain
[s1,s2,s3] = size(phi1);
[Y1,Y2,Y3] = ndgrid(1:s1,1:s2,1:s3);

% *** initialization
% deformed coordinates 
y1 = Y1;  
y2 = Y2;  
y3 = Y3;
% intensity
I1 = phi1;
I2 = phi2;
I3 = phi3;
  
% *** phase tracking
H = waitbar(0,'fixed point iteration....');
for iter = 1:total_iter  
    
    if (iter>1)
        % phase field interpolation
        I1 = interpWrap_v3(phi1,y1,y2,y3);
        I2 = interpWrap_v3(phi2,y1,y2,y3);
        I3 = interpWrap_v3(phi3,y1,y2,y3);
    end
    
    % differences
    dP1 = wrap(I1 - PHI1);
    dP2 = wrap(I2 - PHI2);
    dP3 = wrap(I3 - PHI3);
    
    % phase jacobian inverse
    [JN11,JN21,JN31,JN12,JN22,JN32,JN13,JN23,JN33] = Jinv(G11,G21,G31,G12,G22,G32,G13,G23,G33); 
    
    % calculation of displacements
    dx = JN11.*dP1 + JN12.*dP2 + JN13.*dP3;
    dy = JN21.*dP1 + JN22.*dP2 + JN23.*dP3;
    dz = JN31.*dP1 + JN32.*dP2 + JN33.*dP3;
    
    % determinant check 
    Jbar = Jdet(G11,G21,G31,G12,G22,G32,G13,G23,G33);
    dx(Jbar < Jtol) = 0; 
    dy(Jbar < Jtol) = 0; 
    dz(Jbar < Jtol) = 0;
    
    % nan check
    dx(isnan(dx)) = 0;
    dy(isnan(dy)) = 0;
    dz(isnan(dz)) = 0;
    
    % update estimate
    y1 = y1 - dx; 
    y2 = y2 - dy; 
    y3 = y3 - dz;
    
    % termination (TBA)
    % N = (dy1.^2 + dy2.^2 + dy3.^2).^0.5; 
    % S = sum(N(:)); 
    % N = (dP1.^2 + dP2.^2 + dP3.^2).^0.5; 
    % S = sum(N(~isnan(N)))
    
    % update 
    waitbar(iter/total_iter,H);
end
close(H)

% save deformation field
ui = (y1 - Y1); 
uj = (y2 - Y2); 
uk = (y3 - Y3);
    
end

%% determinat
function Jbar = Jdet(G11,G21,G31,G12,G22,G32,G13,G23,G33) 

   Jbar = G11.*G22.*G33 + G12.*G23.*G31 + G13.*G21.*G32 ...
        - G13.*G22.*G31 - G12.*G21.*G33 - G11.*G23.*G32;
end

%% inverse
function [JN11,JN21,JN31,JN12,JN22,JN32,JN13,JN23,JN33] = Jinv(G11,G21,G31,G12,G22,G32,G13,G23,G33) 

    Jbar = Jdet(G11,G21,G31,G12,G22,G32,G13,G23,G33);
    
    JN11 = (G22.*G33 - G23.*G32)./Jbar; JN12 = (G13.*G32 - G12.*G33)./Jbar; JN13 = (G12.*G23 - G13.*G22)./Jbar; 
    JN21 = (G23.*G31 - G21.*G33)./Jbar; JN22 = (G11.*G33 - G13.*G31)./Jbar; JN23 = (G13.*G21 - G11.*G23)./Jbar; 
    JN31 = (G21.*G32 - G22.*G31)./Jbar; JN32 = (G12.*G31 - G11.*G32)./Jbar; JN33 = (G11.*G22 - G12.*G21)./Jbar; 
            
end










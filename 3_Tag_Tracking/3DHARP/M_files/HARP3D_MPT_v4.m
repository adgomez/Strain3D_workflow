classdef HARP3D_MPT_v4
% F =  HARP3D_MPT_v4(S1,S2,S3,PHI1,PHI2,PHI3,phi1,phi2,phi3) (construct)
% [y1,y2,y3] = F.track(obj,Y1,Y2,Y3,total_iter) (track)

   %% class definition
   properties (SetAccess = protected)
        % phase gradient interpolants
        Fg11,Fg12,Fg13
        Fg21,Fg22,Fg23
        Fg31,Fg32,Fg33
        % image property
        res
        % phase image interpolants
        FP1,FP2,FP3
        Fp1,Fp2,Fp3 
   end
    
   methods
        %% constructor
        function obj = HARP3D_MPT_v4(S1,S2,S3,PHI1,PHI2,PHI3,phi1,phi2,phi3)
            
            % *** gradients for quasi-newton iterations
            res = abs([S1(2,1,1) - S1(1,1,1)
                       S2(1,2,1) - S2(1,1,1)
                       S3(1,1,2) - S3(1,1,1)]);     

            g1 = gradientStar_v2(phi1)./res(1); 
            g2 = gradientStar_v2(phi2)./res(2); 
            g3 = gradientStar_v2(phi3)./res(3);

            obj.Fg11 = griddedInterpolant(S1,S2,S3,g1(:,:,:,1)); obj.Fg12 = griddedInterpolant(S1,S2,S3,g1(:,:,:,2)); obj.Fg13 = griddedInterpolant(S1,S2,S3,g1(:,:,:,3));
            obj.Fg21 = griddedInterpolant(S1,S2,S3,g2(:,:,:,1)); obj.Fg22 = griddedInterpolant(S1,S2,S3,g2(:,:,:,2)); obj.Fg23 = griddedInterpolant(S1,S2,S3,g2(:,:,:,3));
            obj.Fg31 = griddedInterpolant(S1,S2,S3,g3(:,:,:,1)); obj.Fg32 = griddedInterpolant(S1,S2,S3,g3(:,:,:,2)); obj.Fg33 = griddedInterpolant(S1,S2,S3,g3(:,:,:,3));
       
            % *** set interpolants
            % static (PHI) values
            obj.FP1 = interpWrap_v5(S1,S2,S3,PHI1);
            obj.FP2 = interpWrap_v5(S1,S2,S3,PHI2);
            obj.FP3 = interpWrap_v5(S1,S2,S3,PHI3);

            % set interpolants for moving (phi) values
            obj.Fp1 = interpWrap_v5(S1,S2,S3,phi1);
            obj.Fp2 = interpWrap_v5(S1,S2,S3,phi2);
            obj.Fp3 = interpWrap_v5(S1,S2,S3,phi3);
            
            % *** set properties
            obj.res = res;
    
        end
        
        %% standard tracking
         function [y1,y2,y3] = track(obj,Y1,Y2,Y3,total_iter)
             
             % point is initialized at its reference location
             U1 = Y1;
             U2 = Y2;
             U3 = Y3;
             
             % track
             [y1,y2,y3] = tracki(obj,Y1,Y2,Y3,U1,U2,U3,total_iter);

         end

        %% tracking with initialization
        function [y1,y2,y3] = tracki(obj,Y1,Y2,Y3,U1,U2,U3,total_iter)
            
             % relaxation coefficient
            alpha = 0.9;

            % zero jacobian tolerance
            Jtol = 1e-5;

            % number of resolutions for nulling displacement update
            resN = 10; % 4
    
            % initialize deformed coordinates 
            y1 = U1;  
            y2 = U2;  
            y3 = U3;
            
            % phase field interpolation
            P1 = obj.FP1.winterp(Y1,Y2,Y3);
            P2 = obj.FP2.winterp(Y1,Y2,Y3);
            P3 = obj.FP3.winterp(Y1,Y2,Y3);        
            
            % gradient evaluation
            G11 = obj.Fg11(y1,y2,y3); G12 = obj.Fg12(y1,y2,y3); G13 = obj.Fg13(y1,y2,y3);
            G21 = obj.Fg21(y1,y2,y3); G22 = obj.Fg22(y1,y2,y3); G23 = obj.Fg23(y1,y2,y3);
            G31 = obj.Fg31(y1,y2,y3); G32 = obj.Fg32(y1,y2,y3); G33 = obj.Fg33(y1,y2,y3);

            % phase jacobian inverse
            [JN11,JN21,JN31,JN12,JN22,JN32,JN13,JN23,JN33] = Jinv(G11,G21,G31,G12,G22,G32,G13,G23,G33); 
            Jbar = Jdet(G11,G21,G31,G12,G22,G32,G13,G23,G33);

            for iter = 1:total_iter  

                % phase field interpolation
                p1 = obj.Fp1.winterp(y1,y2,y3);
                p2 = obj.Fp2.winterp(y1,y2,y3);
                p3 = obj.Fp3.winterp(y1,y2,y3);
                
                % differences
                dP1 = wrap(p1 - P1);
                dP2 = wrap(p2 - P2);
                dP3 = wrap(p3 - P3);
                % [dP1 dP2 dP3]
                % calculation of displacements
                dx = JN11.*dP1 + JN12.*dP2 + JN13.*dP3;
                dy = JN21.*dP1 + JN22.*dP2 + JN23.*dP3;
                dz = JN31.*dP1 + JN32.*dP2 + JN33.*dP3;

                % determinant check 
                dx(abs(Jbar) < Jtol) = 0; 
                dy(abs(Jbar) < Jtol) = 0; 
                dz(abs(Jbar) < Jtol) = 0;

                % nan check
                dx(isnan(dx)) = 0;
                dy(isnan(dy)) = 0;
                dz(isnan(dz)) = 0;

                % estimate limit
                dx(resN*obj.res(1) <= abs(dx)) = mean(dx);
                dy(resN*obj.res(2) <= abs(dy)) = mean(dy);
                dz(resN*obj.res(3) <= abs(dz)) = mean(dz);
                
                % update estimate
                y1 = y1 - alpha*dx; 
                y2 = y2 - alpha*dy; 
                y3 = y3 - alpha*dz;
            end
        end
   end
end

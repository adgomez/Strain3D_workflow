function im = interpWrap_v4(X,Y,Z,IM,x,y,z)
% im = interpWrap_v4(X,Y,Z,IM,x,y,z)
% use data in ndgrid format

%% indexes

%  resolution
res = [X(2,1,1) - X(1,1,1)
       Y(1,2,1) - Y(1,1,1)
       Z(1,1,2) - Z(1,1,1)];

% origin
org = [min(X(:)) min(Y(:)) min(Z(:))]; 

% find indeces
i = 1 + (x - org(1))/res(1);
j = 1 + (y - org(2))/res(2);
k = 1 + (z - org(3))/res(3);

%% finite differences
J = IM; J(1:end-1,:,:) = J(2:end,:,:); IMi = wrap(J - IM);
J = IM; J(:,1:end-1,:) = J(:,2:end,:); IMj = wrap(J - IM);
J = IM; J(:,:,1:end-1) = J(:,:,2:end); IMk = wrap(J - IM);
J = IMi; J(:,1:end-1,:) = J(:,2:end,:); IMij = wrap(J - IMi);
J = IMj; J(:,:,1:end-1) = J(:,:,2:end); IMjk = wrap(J - IMj);
J = IMi; J(:,:,1:end-1) = J(:,:,2:end); IMik = wrap(J - IMi);
J = IMij; J(:,:,1:end-1) = J(:,:,2:end); IMijk = wrap(J - IMij);

%% trilinear interpolation

% measure matrix extent
dim = size(IM);

% select indexes in range
Ir = (((1<=i)&(i<dim(1)))) & ...
     (((1<=j)&(j<dim(2)))) & ...
     (((1<=k)&(k<dim(3))));

% get image locations 
Ip = sub2ind(size(IM),floor(i(Ir)),floor(j(Ir)),floor(k(Ir)));

% define index resuduals 
Ri = i - floor(i);
Rj = j - floor(j);
Rk = k - floor(k);
 
% preallocate output
im = i*nan;

% interpolate
im(Ir) = IM(Ip) + ...
         IMi(Ip).*Ri(Ir) + ...
         IMj(Ip).*Rj(Ir) + ...
         IMk(Ip).*Rk(Ir) + ...
         IMij(Ip).*Ri(Ir).*Rj(Ir) + ...
         IMjk(Ip).*Rj(Ir).*Rk(Ir) + ...
         IMik(Ip).*Ri(Ir).*Rk(Ir) + ...
         IMijk(Ip).*Ri(Ir).*Rj(Ir).*Rk(Ir) ;

% wrap
im = wrap(im);

end


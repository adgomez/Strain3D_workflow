%% wire spring boundary condition

formatSpec = '\t<Boundary>\n';
fprintf(fileID,formatSpec);

    if (~isempty(bc_rigid))&rigid_mat 
        formatSpec = '\t\t<contact type="rigid">\n';
        fprintf(fileID,formatSpec);
        formatSpec = '\t\t\t <node id="%i" rb="1"></node>\n';
        fprintf(fileID,formatSpec,bc_rigid);
        formatSpec = '\t\t</contact>\n';
        fprintf(fileID,formatSpec);
    end

    if ~isempty(bc_fixed) 
        % for cnt_sp = 1:size(bc_fixed,1)
            formatSpec = '\t\t <fix>\n';
            fprintf(fileID,formatSpec);
                formatSpec = '\t\t\t <node id="%i" bc="xyz"/> \n';
                fprintf(fileID,formatSpec,bc_fixed);
            formatSpec = '\t\t</fix>\n';
            fprintf(fileID,formatSpec);
        % end
    end
    
    if ~isempty(bc_spring) 
       for cnt_sp = 1:size(bc_spring,1)
            formatSpec = '\t\t <spring>\n';
            fprintf(fileID,formatSpec);
                formatSpec = '\t\t\t <node>%i,%i</node>\n';
                fprintf(fileID,formatSpec,bc_spring(cnt_sp,:));
                formatSpec = '\t\t\t <E>%f</E>\n';
                fprintf(fileID,formatSpec,Esp);
            formatSpec = '\t\t </spring>\n';
            fprintf(fileID,formatSpec);
       end
    end
    
formatSpec = '\t </Boundary>\n';
fprintf(fileID,formatSpec);


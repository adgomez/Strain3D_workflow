        % save initial condition for absolute tracking 
        nod_out = [ref_nodes(:,1) ref_nodes(:,1) ref_nodes(:,2:4)];
        fileID = fopen(file_e,'w');
        formatSpec = '\t<Initial>\n';
            fprintf(fileID,formatSpec);       
            formatSpec = '\t\t<velocity>\n';
            fprintf(fileID,formatSpec);
            formatSpec = '\t\t\t<node id="%i">%15.7e,%15.7e,%15.7e</node> \n';
            fprintf(fileID,formatSpec,[nod_out(:,1) nod_out(:,3:5)]');
            formatSpec = '\t\t</velocity>\n';
            fprintf(fileID,formatSpec);
        formatSpec = '\t</Initial>\n';
        fprintf(fileID,formatSpec);     
        fclose(fileID);
   %% write material portion

    formatSpec = '<?xml version="1.0" encoding="ISO-8859-1"?>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '<febio_spec version="1.2">\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t<Module type="solid"/>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t<Globals>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t\t<Constants>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t\t\t<T>1</T>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t\t\t<R>1</R>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t\t\t<Fc>0</Fc>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t\t</Constants>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t</Globals>\n';
    fprintf(fileID,formatSpec);
    
%     formatSpec = '\t<Control>\n';
%     fprintf(fileID,formatSpec);
%         formatSpec = '\t\t<integration>\n';
%         fprintf(fileID,formatSpec);
%             formatSpec = '\t\t\t<rule elem="tet4">GAUSS4</rule>\n';
%             fprintf(fileID,formatSpec);
% %             formatSpec = '\t\t\t<rule elem="tet4" type="UT4">\n';
% %             fprintf(fileID,formatSpec);
% %                 formatSpec = '\t\t\t\t<alpha>0.5</alpha>\n';
% %                 fprintf(fileID,formatSpec);
% %                 formatSpec = '\t\t\t\t<iso_stab>0</iso_stab>\n';
% %                 fprintf(fileID,formatSpec);
% %             formatSpec = '\t\t\t</rule>\n';
% %            fprintf(fileID,formatSpec);
%         formatSpec = '\t\t</integration>\n';
%         fprintf(fileID,formatSpec);
%     formatSpec = '\t</Control>\n';
%     fprintf(fileID,formatSpec);

    formatSpec = '\t<Material>\n';
    fprintf(fileID,formatSpec);

    for cnt_m = 1:numel(Im)
        if (cnt_m == 1)&&rigid_mat
            % rigid material
            % disp(['Material ' num2str(cnt_m) ' set to rigid'])
            formatSpec = '\t\t<material id="%i" name="Material%i" type="rigid body">\n';
            fprintf(fileID,formatSpec,[Im(cnt_m) Im(cnt_m)]');
                formatSpec = '\t\t\t<density>1</density>\n';
                fprintf(fileID,formatSpec);
                formatSpec = '\t\t\t<center_of_mass>%f,%f,%f</center_of_mass>\n';
                fprintf(fileID,formatSpec,Cg);
        else
       
            % non-rigid materials (quasi-incompressible)       
            formatSpec = '\t\t<material id="%i" name="Material%i" type="uncoupled solid mixture">\n';
            fprintf(fileID,formatSpec,[Im(cnt_m) Im(cnt_m)]');
            % formatSpec = '\t\t\t<mat_axis type="vector"><a>1,0,0</a><d>0,0,1</d></mat_axis>\n';
            % fprintf(fileID,formatSpec);
            formatSpec = '\t\t\t<density>1</density>\n';
            fprintf(fileID,formatSpec);
            formatSpec = '\t\t\t<solid type="Mooney-Rivlin">\n';
            fprintf(fileID,formatSpec);
            formatSpec = '\t\t\t\t<c1>1</c1>\n';
            fprintf(fileID,formatSpec);
            formatSpec = '\t\t\t\t<c2>0</c2>\n';
            fprintf(fileID,formatSpec);
            formatSpec = '\t\t\t\t<k>100</k>\n';
            fprintf(fileID,formatSpec);
            formatSpec = '\t\t\t</solid>\n';
            fprintf(fileID,formatSpec);
     
%             % non-rigid materials (compressible)
%             formatSpec = '\t\t<material id="%i" name="Material%i" type="orthotropic elastic">\n';
%             fprintf(fileID,formatSpec,[Im(cnt_m) Im(cnt_m)]');
%                 formatSpec = '\t\t\t<density>1</density>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<mat_axis type="vector"><a>1,0,0</a><d>0,0,1</d></mat_axis>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<E1>0.33</E1>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<E2>0.33</E2>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<E3>0.33</E3>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<G12>0.5</G12>\n';
%                 fprintf(fileID,formatSpec)
%                 formatSpec = '\t\t\t\t<G23>0.5</G23>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<G31>0.5</G31>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<v12>0.1</v12>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<v23>0.1</v23>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<v31>0.1</v31>\n';
%                 fprintf(fileID,formatSpec);             
                
%             % non-rigid materials (quasi-incompressible)
%             formatSpec = '\t\t<material id="%i" name="Material%i" type="Fung orthotropic">\n';
%             fprintf(fileID,formatSpec,[Im(cnt_m) Im(cnt_m)]');
%                 formatSpec = '\t\t\t<density>1</density>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<mat_axis type="vector"><a>1,0,0</a><d>0,0,1</d></mat_axis>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<E1>3.0</E1>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<E2>3.0</E2>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<E3>3.0</E3>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<G12>18.0</G12>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<G23>18.0</G23>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<G31>1.8</G31>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<v12>0.0</v12>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<v23>0.0</v23>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<v31>0.0</v31>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<c>4.0</c>\n';
%                 fprintf(fileID,formatSpec);               
%                 formatSpec = '\t\t\t\t<k>100</k>\n';
%                 fprintf(fileID,formatSpec);
                
%             % non-rigid materials (quasi-incompressible)
%             formatSpec = '\t\t<material id="%i" name="Material%i" type="Mooney-Rivlin">\n';
%             fprintf(fileID,formatSpec,[Im(cnt_m) Im(cnt_m)]');
%                 formatSpec = '\t\t\t<density>1</density>\n';
%                 fprintf(fileID,formatSpec);
%                 
%                 if (cnt_m == 1)
%                     formatSpec = '\t\t\t\t<c1>1e7</c1>\n';
%                     fprintf(fileID,formatSpec);
%                     formatSpec = '\t\t\t\t<c2>0</c2>\n';
%                     fprintf(fileID,formatSpec);
%                     formatSpec = '\t\t\t\t<k>1e9</k>\n';
%                     fprintf(fileID,formatSpec);
%                 else
%                     formatSpec = '\t\t\t\t<c1>1</c1>\n';
%                     fprintf(fileID,formatSpec);
%                     formatSpec = '\t\t\t\t<c2>0</c2>\n';
%                     fprintf(fileID,formatSpec);
%                     formatSpec = '\t\t\t\t<k>1e2</k>\n';
%                     fprintf(fileID,formatSpec);
%                 end
                

          
            % non-rigid materials (compressible)
%             formatSpec = '\t\t<material id="%i" name="Material%i" type="neo-Hookean">\n';
%             fprintf(fileID,formatSpec,[Im(cnt_m) Im(cnt_m)]');
%                 formatSpec = '\t\t\t<density>1</density>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<E>1</E>\n';
%                 fprintf(fileID,formatSpec);
%                 formatSpec = '\t\t\t\t<v>0</v>\n';
%                 fprintf(fileID,formatSpec);
        end
        formatSpec = '\t\t</material>\n';
        fprintf(fileID,formatSpec);
    end

    formatSpec = '\t</Material>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t<Geometry>\n';
    fprintf(fileID,formatSpec);
  

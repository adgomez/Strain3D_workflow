clc
close all force
clear all

% lastest: adding the ability to start from an arbitrary time point

% NOTE: HARP-FE material tracking can be adjusted in lines 146 and 218 of
% write_HARP_step

% TODO: define orthotropic material properties
% TODO: straighten mesh file sources
% TODO: enable advanced tracking features

%% analysis folder path

enable_folders

%% NOTES

% HARP-FE Solution
run_n = 40; 

% This is a string to identify the results
PARAM.comments.HFE = [ 'tracking using simple_brain mesh'];

%% extract paramters
% NOTE: load curve numbers shold remain as they are for now

% number of tracking frames
t_steps = PARAM.tracking.time_steps; 

% sets material 1 to rigid
rigid_mat = PARAM.tracking.hfe.rigid_mat; 
% [trans x, trans y, trans z, rot x, rot y, rot z]
rigid_const = PARAM.tracking.hfe.rigid_const; 
% boundary condition type: 'springs', 'file', 'none'
use_bcs = PARAM.tracking.hfe.use_bcs;
% enable sequantial multiframe approach
sequential_track = PARAM.tracking.hfe.sequential_track;    
% penalty load curve number
penalty_lc = 1;
% penalty load curve  
penalty = PARAM.tracking.hfe.penalty;
% enable lag. augumentation 
augon = PARAM.tracking.hfe.augon;
% lag. augumentation load curve number   
atol_lc = 2;
% lag. augumentation load curve
atol = PARAM.tracking.hfe.atol;

% start at a number greater than 1 if some steps have already been done,
% and we wish to contiue at a later step
start_at = 1;

%% load reference mesh
% reference mesh (to save or load user-specific referenge geometry)

FE_folder = analysis_folder;
FE_file = 'mesh_tag_1'; % mesh_lin
load([FE_folder '/' FE_file])

ref_nodes = nodes;
ref_elements = elements;

 % load tracking vars
load([analysis_folder '/' 'track_vars']);

 % intialize containers
n_nodes = numel(ref_nodes(:,1));
nod_u = zeros(n_nodes,3,t_steps + 1);
nod_res = zeros(n_nodes,1,t_steps + 1);

 % create folder for saving HARP-FE output
HFE_run_folder = [analysis_folder '/'  'HFE_run' num2str(run_n)];
if ~exist(HFE_run_folder,'dir')
    mkdir(HFE_run_folder)
end

%% time loop

H = waitbar(0,'Tracking ...');
for cnt_t = 1:t_steps
    
    % update screen
    disp(['*********** HARP-FE STEP: ' num2str(cnt_t) ' OF ' num2str(t_steps) ' ***********'])

    % *** initialize
    if cnt_t == 1
        % refrence nodes
        nodes = ref_nodes;
        
        % file names 
        GEO_folder =  pwd;
        GEO_source_file = 'CM_v7.feb';
        file_a = [GEO_source_file(1:end-4) '_a.txt']; % matl
        file_b = [GEO_source_file(1:end-4) '_b.txt']; % nodes
        file_c = [GEO_source_file(1:end-4) '_c.txt']; % elem 
        file_d = [GEO_source_file(1:end-4) '_d.txt']; % boundary
        file_e = [GEO_source_file(1:end-4) '_e.txt']; % initial
        file_f = [GEO_source_file(1:end-4) '_f.txt']; % step
    
        % material file 
        Im = [];
        for cnt_m = 1:size(ref_elements,2)
            Im = [Im; unique(ref_elements{cnt_m}(:,2))];
        end
        Im = sort(Im);
        fileID = fopen(file_a,'w');
        write_harp_mat
        fclose(fileID);
        
        % write elements file
        fileID = fopen(file_c,'w'); 
        write_harp_elem
        fclose(fileID);
        
        % write boundary file
        switch use_bcs
            case 'file'
                warning('this feature has not been tested')
                load([analysis_folder '/' 'bcs_lin']);
                bc_rigid = [];
                bc_fixed = []; 

                fileID = fopen(file_d,'w');
                write_harp_boundary
                fclose(fileID);
                
            case 'springs'
                bc_rigid = [];
                bc_spring = spring;
                bc_fixed = bc_spring(:,2);
                Esp = 1e-3; % 1e-3

                fileID = fopen(file_d,'w');
                write_harp_boundary
                fclose(fileID);
            case 'none'
                file_d = '';
        end
   
        % write initial file
        if sequential_track
            file_e = '';
        else
            write_harp_init
        end

    else
        % refrence nodes for template evaluation
        Ip = 1:numel(nodes(:,1));
        
        % absolute update
        % nodes(Ip,2:4) = ref_nodes(Ip,2:4); % absolute (no update) 
        nodes(Ip,2:4) = ref_nodes(Ip,2:4) + nod_u(Ip,:,cnt_t); % sequential updates   
        
        % incremental update
        % nod_u_inc = nod_u(Ip,:,cnt_t) - nod_u(Ip,:,cnt_t-1);
        % nodes(Ip,2:4) = nodes(Ip,2:4) + 2*nod_u_inc;
        
        % import update (this can just go outside the if else)
        % load([analysis_folder '/' 'trans_TAG_N_TAG_1'])
        % N_nod = [ref_nodes(Ip,2:4) ref_nodes(Ip,1)*0+1]*inv(F(:,:,cnt_t+1));
        % nodes(Ip,2:4) = N_nod(Ip,1:3) + 0.5*nod_u_inc*(cnt_t==4) + 0.5*nod_u_inc*(cnt_t==5);
    end
    
    % save moved nodes 
    fileID = fopen(file_b,'w'); 
    write_harp_nodes
    fclose(fileID);
    
    %% write constraint section on FEBio file

    % location of raw images
    raw_folder = filt_folder;

    % Q image information 
    raw_dim = Q_dim;
    raw_box = Q_box;   

    % advanced tracking features     
    double_tracking = false;
    t = cnt_t; % sequential template
    t_abs = 1; % absolute template
    s = cnt_t + 1; % target

    % harp-fe *.feb file for this time point (changed to cnt_t from t)
    file_FE = ['HFE_' num2str(cnt_t) '_v' num2str(run_n) '.feb'];
    file_nod = [file_FE(1:end-4) '.txt'];
    file_res = [file_FE(1:end-4) '_res.txt'];
    
    %w write step section
    fileID = fopen(file_f,'w'); 
    write_harp_step
    fclose(fileID);
    
    %% form HARP-FE files for this time point
    % (UNIX)
    system(['cat ' file_a ' ' file_b ' ' file_c ' ' file_d ' ' file_e ' ' file_f ' > ' GEO_folder '/' file_FE]);
    % (WIN7) -- check
    % system(['copy /b '  file_a '+' file_b '+' file_c  '+' file_d  ' ' file_FE]);
    
    % *** execute FEBio
    if (cnt_t >= start_at)
        % (UNIX)
        % system(['./febio2.lnx64 -i ' file_FE ]); 
        system(['unset LD_LIBRARY_PATH; ' './febio2.lnx64 -i ' file_FE ])
        % (WIN7)
        % system(['FEBio2 -i ' file_FE ]); 
    else
       warning('Reading from an exisiting HARP-FE file') 
    end
    
    % update screen
    disp(['******* HARP-FE STEP: ' num2str(cnt_t) ' OF ' num2str(t_steps) ' COMPLETE *******'])

    
    %% extract results
    
    file_log = [file_FE(1:end-4) '.log'];
    fid = fopen(file_log);
    temp = textscan(fid,'%s',1,'Delimiter','\n');
    while isempty(strfind(cell2mat(temp{1}),'Number of time steps completed'));
        temp = textscan(fid,'%s',1,'Delimiter','\n');  
    end
    temp = cell2mat(temp{1});   
    n_steps = str2num(temp(54:end));
    fclose(fid);
    
    % load nodal displacements
    Nlist = read_data_log_v3(file_nod,n_steps,n_nodes,4,0,4);

    % save nodal displacements
    nod_u(:,:,cnt_t + 1) = Nlist(:,2:4,end) - ref_nodes(:,2:4);
    
    % load nodal phase residuals
    Rlist = read_data_log_v3(file_res,n_steps,n_nodes,2,0,4);
    
    % save nodal phase residuals
    nod_res(:,:,cnt_t + 1) = Rlist(:,2,end);
    
    % update progress bar
    waitbar(cnt_t/t_steps,H)
end
close(H)

%% save parameters structure

save('HFE_parameters','PARAM')

%% generate output file with all displacements  

% preview
save([HFE_run_folder '/' 'nodal_disp' ],'nod_u')

% save resudual for postview visualization
dim_u = size(nod_res);
out_u = nodes(:,1);
for cnt_t = 1:dim_u(3)
    out_u = cat(2,out_u,nod_res(:,:,cnt_t));
end
dlmwrite([HFE_run_folder '/' 'HFE_PhaseRes' '.txt'],out_u,',');

%% copy HFE files

% material prototype file (for postprocessing)
system(['cp ' file_a ' ' HFE_run_folder]);

Df = dir('*HFE*');
for cnt_f = 1:size(Df,1)
    system(['mv ' Df(cnt_f).name ' ' HFE_run_folder]);
end

% echo save files
disp(['output files saved in'])
disp(HFE_run_folder)


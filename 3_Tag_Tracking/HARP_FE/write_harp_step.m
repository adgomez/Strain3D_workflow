%% HARP-FE step
% including some debuging features

formatSpec = '\t<Step name="HARP-FE">\n';
    fprintf(fileID,formatSpec);
    
        formatSpec = '\t\t<Module type="solid"/>\n';
        fprintf(fileID,formatSpec);
        formatSpec = '\t\t<Control>\n';
        fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<time_steps>10</time_steps>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<step_size>0.1</step_size>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<max_refs>5</max_refs>\n'; % 5
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<max_ups>10</max_ups>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<dtol>0.001</dtol>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<etol>0.01</etol>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<rtol>0</rtol>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<lstol>0.9</lstol>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<time_stepper>\n';
            fprintf(fileID,formatSpec);   
                formatSpec = '\t\t\t\t<dtmin>0.01</dtmin>\n';
                fprintf(fileID,formatSpec);               
                formatSpec = '\t\t\t\t<dtmax>0.1</dtmax>\n';
                fprintf(fileID,formatSpec);  
                formatSpec = '\t\t\t\t<max_retries>2</max_retries>\n'; % 2
                fprintf(fileID,formatSpec);               
                formatSpec = '\t\t\t\t<opt_iter>10</opt_iter>\n';
                fprintf(fileID,formatSpec);  
            formatSpec = '\t\t\t</time_stepper>\n';
            fprintf(fileID,formatSpec);
            formatSpec = '\t\t\t<analysis type="static"/>\n';
            fprintf(fileID,formatSpec); 
        formatSpec = '\t\t</Control>\n';
        fprintf(fileID,formatSpec);  
        
        formatSpec = '\t\t<Constraints>\n';
        fprintf(fileID,formatSpec);   
        
        if rigid_mat&&any(rigid_const)
            formatSpec = '\t\t\t<rigid_body mat="1">\n';
            fprintf(fileID,formatSpec);   
            
            if rigid_const(1)
                formatSpec = '\t\t\t\t<trans_x type="fixed"></trans_x>\n';
                fprintf(fileID,formatSpec);     
            end
            if rigid_const(2)
                formatSpec = '\t\t\t\t<trans_y type="fixed"></trans_y>\n';
                fprintf(fileID,formatSpec);  
            end
            if rigid_const(3)
                formatSpec = '\t\t\t\t<trans_z type="fixed"></trans_z>\n';
                fprintf(fileID,formatSpec);  
            end
            if rigid_const(4)
                formatSpec = '\t\t\t\t<rot_x type="fixed"></rot_x>\n';
                fprintf(fileID,formatSpec); 
            end
            if rigid_const(5)
                formatSpec = '\t\t\t\t<rot_y type="fixed"></rot_y>\n';
                fprintf(fileID,formatSpec);  
            end
            if rigid_const(6)
                formatSpec = '\t\t\t\t<rot_z type="fixed"></rot_z>\n';
                % formatSpec = '\t\t\t\t<rot_z type="prescribed" lc="3">1</rot_z>\n';
                % formatSpec = '\t\t\t\t<rot_z type="force" lc="3">1</rot_z>\n';
                fprintf(fileID,formatSpec);  
            end
            
            formatSpec = '\t\t\t</rigid_body>\n';
            fprintf(fileID,formatSpec);   
        end

if (sequential_track||double_tracking)
    
            % sequential tracs t (cnt_t) to s (cnt_t+1)
    
            formatSpec = '\t\t\t<constraint type="HARP-FE">\n';
            fprintf(fileID,formatSpec); 
                % formatSpec = '\t\t\t\t<dom>%i</dom>';
                % fprintf(fileID,formatSpec,1);
                formatSpec = '\t\t\t\t<TP1 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI1t%i.raw">\n';
                fprintf(fileID,formatSpec,t);  
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</TP1>\n';
                fprintf(fileID,formatSpec);              
                formatSpec = '\t\t\t\t<TP2 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI2t%i.raw">\n';
                fprintf(fileID,formatSpec,t);  
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</TP2>\n';
                fprintf(fileID,formatSpec); 
                formatSpec = '\t\t\t\t<TP3 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI3t%i.raw">\n';
                fprintf(fileID,formatSpec,t);   
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</TP3>\n';
                fprintf(fileID,formatSpec);  
                
                formatSpec = '\t\t\t\t<SP1 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI1t%i.raw">\n';
                fprintf(fileID,formatSpec,s);  
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</SP1>\n';
                fprintf(fileID,formatSpec);   
                formatSpec = '\t\t\t\t<SP2 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI2t%i.raw">\n';
                fprintf(fileID,formatSpec,s);  
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</SP2>\n';
                fprintf(fileID,formatSpec); 
                formatSpec = '\t\t\t\t<SP3 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI3t%i.raw">\n';
                fprintf(fileID,formatSpec,s);    
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</SP3>\n';
                fprintf(fileID,formatSpec);                                        
                formatSpec = '\t\t\t\t<range_min>%f,%f,%f</range_min>\n';
                fprintf(fileID,formatSpec,raw_box(1,:)); 
                formatSpec = '\t\t\t\t<range_max>%f,%f,%f</range_max>\n';
                fprintf(fileID,formatSpec,raw_box(2,:)); 
                formatSpec = '\t\t\t\t<penalty lc="%i">1</penalty>\n';
                fprintf(fileID,formatSpec,penalty_lc);  
                formatSpec = '\t\t\t\t<stop_at>%i</stop_at>\n';
                fprintf(fileID,formatSpec,numel(Im)-2); 
                formatSpec = '\t\t\t\t<laugon>1</laugon>\n';
                fprintf(fileID,formatSpec);                  
                formatSpec = '\t\t\t\t<altol lc="%i">1</altol>\n';
                fprintf(fileID,formatSpec,atol_lc);                  
                formatSpec = '\t\t\t\t<template_time>%f</template_time>\n';
                fprintf(fileID,formatSpec,0.00);                   
            formatSpec = '\t\t\t</constraint>\n';
            fprintf(fileID,formatSpec);  

end
            
if (~sequential_track||double_tracking)
    
            % absolute traks t_abs (1) to s (cnt_t)
            formatSpec = '\t\t\t<constraint type="HARP-FE">\n';
            fprintf(fileID,formatSpec);   
                formatSpec = '\t\t\t\t<TP1 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI1t%i.raw">\n';
                fprintf(fileID,formatSpec,t_abs);
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</TP1>\n';
                fprintf(fileID,formatSpec);                   
                formatSpec = '\t\t\t\t<TP2 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI2t%i.raw">\n';
                fprintf(fileID,formatSpec,t_abs);
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</TP2>\n';
                fprintf(fileID,formatSpec); 
                formatSpec = '\t\t\t\t<TP3 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI3t%i.raw">\n';
                fprintf(fileID,formatSpec,t_abs);  
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</TP3>\n';
                fprintf(fileID,formatSpec);                 
                formatSpec = '\t\t\t\t<SP1 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI1t%i.raw">\n';
                fprintf(fileID,formatSpec,s);  
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</SP1>\n';
                fprintf(fileID,formatSpec);                   
                formatSpec = '\t\t\t\t<SP2 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI2t%i.raw">\n';
                fprintf(fileID,formatSpec,s); 
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</SP2>\n';
                fprintf(fileID,formatSpec); 
                formatSpec = '\t\t\t\t<SP3 file="%s/';
                fprintf(fileID,formatSpec,raw_folder); 
                formatSpec = 'PHI3t%i.raw">\n';
                fprintf(fileID,formatSpec,s);  
                    formatSpec = '\t\t\t\t\t<size>%i,%i,%i</size>\n';
                    fprintf(fileID,formatSpec,[raw_dim]');                   
                formatSpec = '\t\t\t\t</SP3>\n';
                fprintf(fileID,formatSpec);                                        
                formatSpec = '\t\t\t\t<range_min>%f,%f,%f</range_min>\n';
                fprintf(fileID,formatSpec,raw_box(1,:)); 
                formatSpec = '\t\t\t\t<range_max>%f,%f,%f</range_max>\n';
                fprintf(fileID,formatSpec,raw_box(2,:)); 
                formatSpec = '\t\t\t\t<penalty lc="%i">0.5</penalty>\n';
                fprintf(fileID,formatSpec,penalty_lc); 
                formatSpec = '\t\t\t\t<stop_at>%i</stop_at>\n';
                fprintf(fileID,formatSpec,numel(Im)-2); 
                formatSpec = '\t\t\t\t<laugon>%i</laugon>\n';
                fprintf(fileID,formatSpec,augon);                  
                formatSpec = '\t\t\t\t<altol lc="%i">1</altol>\n';
                fprintf(fileID,formatSpec,atol_lc);                  
                formatSpec = '\t\t\t\t<template_time>%f</template_time>\n';
                fprintf(fileID,formatSpec,-0.001);                   
            formatSpec = '\t\t\t</constraint>\n';
            fprintf(fileID,formatSpec); 
end
            
        formatSpec = '\t\t</Constraints>\n';
        fprintf(fileID,formatSpec);                   
    formatSpec = '\t</Step>\n';
    fprintf(fileID,formatSpec);
    
    formatSpec = '\t<LoadData>\n';
    fprintf(fileID,formatSpec);
        formatSpec = '\t\t<loadcurve id="1" name="HARP-FE penaly" type="linear">\n';
        fprintf(fileID,formatSpec);          
           for cnt_l = 1: numel(penalty(:,1))
               formatSpec = '\t\t\t<loadpoint>%f,%f</loadpoint>\n';
               fprintf(fileID,formatSpec,penalty(cnt_l,:)); 
           end
        formatSpec = '\t\t</loadcurve>\n';
        fprintf(fileID,formatSpec);      
        formatSpec = '\t\t<loadcurve id="2" name="HARP-FE aug tol" type="linear">\n';
        fprintf(fileID,formatSpec);   
           for cnt_l = 1: numel(atol(:,1))
               formatSpec = '\t\t\t<loadpoint>%f,%f</loadpoint>\n';
               fprintf(fileID,formatSpec,atol(cnt_l,:)); 
           end
        formatSpec = '\t\t</loadcurve>\n';
        fprintf(fileID,formatSpec);
%         formatSpec = '\t\t<loadcurve id="3" name="DBrot" type="linear">\n';
%         fprintf(fileID,formatSpec);   
%            for cnt_l = 1: numel(DBrot(:,1))
%                formatSpec = '\t\t\t<loadpoint>%f,%f</loadpoint>\n';
%                fprintf(fileID,formatSpec,DBrot(cnt_l,:)); 
%            end
%         formatSpec = '\t\t</loadcurve>\n';
%         fprintf(fileID,formatSpec); 
    formatSpec = '\t</LoadData>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t<Output>\n';
    fprintf(fileID,formatSpec);
        formatSpec = '\t\t<plotfile type="febio">\n';
        fprintf(fileID,formatSpec);  
            formatSpec = '\t\t\t<var type="displacement"/>\n';
            fprintf(fileID,formatSpec);          
            formatSpec = '\t\t\t<var type="relative volume"/>\n';
            fprintf(fileID,formatSpec); 
            formatSpec = '\t\t\t<var type="stress"/>\n';
            fprintf(fileID,formatSpec); 
            formatSpec = '\t\t\t<var type="harpFE-TrackForce"/>\n';
            fprintf(fileID,formatSpec);          
            formatSpec = '\t\t\t<var type="harpFE-PhaseTemplate"/>\n';
            fprintf(fileID,formatSpec); 
            formatSpec = '\t\t\t<var type="harpFE-PhaseTarget"/>\n';
            fprintf(fileID,formatSpec);             
            formatSpec = '\t\t\t<var type="harpFE-PhaseResidual"/>\n';
            fprintf(fileID,formatSpec);                 
        formatSpec = '\t\t</plotfile>\n';
        fprintf(fileID,formatSpec);
        formatSpec = '\t\t<logfile>\n';
        fprintf(fileID,formatSpec);  
           formatSpec = '\t\t\t<node_data data="x;y;z" name="nodal coordinates" file="%s" delim=","></node_data>\n';
           fprintf(fileID,formatSpec,file_nod); 
        formatSpec = '\t\t</logfile>\n';
        fprintf(fileID,formatSpec);          
        formatSpec = '\t\t<logfile>\n';
        fprintf(fileID,formatSpec);  
           formatSpec = '\t\t\t<node_data data="PR" name="PhaseRes" file="%s" delim=","></node_data>\n';
           fprintf(fileID,formatSpec,file_res); 
        formatSpec = '\t\t</logfile>\n';
        fprintf(fileID,formatSpec);            
    formatSpec = '\t</Output>\n';
    fprintf(fileID,formatSpec);  
formatSpec = '</febio_spec>\n';
fprintf(fileID,formatSpec);   


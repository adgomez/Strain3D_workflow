%% standard element definition

formatSpec = '\t\t <Elements>\n';
fprintf(fileID,formatSpec);

for cnt_m = 1:numel(element_type) 
    elem_type_i = element_type{cnt_m};
    switch elem_type_i
    
    case 'hex8'
        formatSpec = '\t\t\t <hex8 id="%i" mat="%i">%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i</hex8> \n';
        fprintf(fileID,formatSpec,elements{cnt_m}'); 
    case 'hex20'
        formatSpec = '\t\t\t <hex20 id="%i" mat="%i">%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i</hex20> \n';
        fprintf(fileID,formatSpec,elements{cnt_m}');  
    case 'tet4'
        formatSpec = '\t\t\t <tet4 id="%i" mat="%i">%6i,%6i,%6i,%6i</tet4> \n';
        fprintf(fileID,formatSpec,elements{cnt_m}');
    case 'penta6'
        formatSpec = '\t\t\t <penta6 id="%i" mat="%i">%6i,%6i,%6i,%6i,%6i,%6i</penta6> \n';
        fprintf(fileID,formatSpec,elements{cnt_m}');
    otherwise
        error('undefined element type')
    end
end

formatSpec = '\t\t </Elements>\n';
fprintf(fileID,formatSpec);
formatSpec = '\t </Geometry>\n';
fprintf(fileID,formatSpec);    
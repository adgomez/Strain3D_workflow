function Q = get_Q_v2(v,theta)
% Q = get_Q_v2(v,theta)
% generates rotation matrix according to Rodriges' rotational formula

    theta = -theta;

    outter_v = [v(1)^2       v(1)*v(2)       v(1)*v(3)
                v(1)*v(2)    v(2)^2          v(2)*v(3)
                v(1)*v(3)    v(2)*v(3)       v(3)^2];
    cross_v = [ 0      -v(3)    v(2)
                v(3)    0      -v(1)
               -v(2)    v(1)    0];
    Q = eye(3)*cos(theta) + cross_v*sin(theta) + outter_v*(1-cos(theta));
    
end
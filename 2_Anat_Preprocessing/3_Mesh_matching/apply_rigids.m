% T1_1mm information to tagged reference frame

%% housekeeping

close all
clear all
clc

% latest: removed manual modifiers on transformation
% NOTE: some paramters could be added.

%% analysis folder path

enable_folders

%% extract paramters

% additional displacement (mm, x y z)
manual_c = PARAM.anat.manual_trans;
% additional rotation (deg, x y z)
manual_r = PARAM.anat.manual_rot;

%% load data in T1_1mm
% NOTE: there are 3 files: 2 surfaces for checking positioning, and the FE mesh for HARP-FE 

% location of files
FE_folder = analysis_folder;

% head surface
H_file = 'head_atl_T1_1mm.surf';
[H_ver,H_tri] = surf2mat_v3([FE_folder '/' H_file],'amira');

% cerebrum surface
C_file = 'csf_atl_T1_1mm.surf';
[C_ver,C_tri] = surf2mat_v3([FE_folder '/' C_file],'amira');

% FE mesh
load([FE_folder '/' 'mesh_T1_1mm'])

%% attach spring locations

% spring lenght
s_l = 20;

% brain group
Mg = 1;

% approximate centroid
% TODO: vectorize for speed
M = 0;
RM = 0;
Ib = find(elements{Mg}(:,2)~=1);
for cnt_e = 1:size(Ib,1)
    % get elements 
    En = elements{Mg}(Ib(cnt_e),3:6);
    % define vertices
    a = nodes(En(1),2:4);
    b = nodes(En(2),2:4);
    c = nodes(En(3),2:4);
    d = nodes(En(4),2:4);
    % define tet volume (or mass, assuming constant density)
    m = (1/6)*abs(dot((a - d),cross((b - d),(c - d))));
    % calcuate tet centroid
    r = mean(nodes(En,2:4),1);
    % integrate
    M = M + m;
    RM = RM + r*m;
end

% save volume
volume = M;
save([analysis_folder '/' 'brain_vol'],'volume')

% per centroid definition
ctr = RM./M;
% closest node
N = sum((nodes(:,2:4) - repmat(ctr,[size(nodes,1) 1])).^2,2).^0.5;
[~,Ictr] = min(N);

% surface points
pt = H_ver(:,2:4);

% approximate radius
res = pt - repmat(ctr,[size(pt,1) 1]);

R = mean(sum(res.^2,2).^0.5,1);

% set up tet about centroid
tet = R*[1 1 1
         1 -1 -1
        -1 1 -1 
        -1 -1 1];
   
% scale tet
tet = (tet - repmat(mean(tet,1),[size(tet,1) 1]) + repmat(ctr,[size(tet,1) 1]));

% get directional vectors from centroid
n = tet - repmat(ctr,[size(tet,1) 1]);
n = n./sum(n.^2,2).^0.5;

% find point closest to each tet vertex 
ap = (repmat(ctr,[size(pt,1) 1]) - pt);
for cnt_p = 1:size(tet,1) 
    % distance to line
    N = repmat(n(cnt_p,:),[size(pt,1) 1]);
    DL = ap - dot(ap,N,2).*N;
    DL = sum(DL.^2,2).^0.5;
    % distance to vertex
    DV = sum((pt - repmat(tet(cnt_p,:),[size(pt,1) 1])).^2,2).^0.5;
    % total 
    D = DL + DV;
    [~,Ic(cnt_p,1)] = min(D); 
    % closest coordinates in mesh nodes
    D = sum((nodes(:,2:4) - repmat(pt(Ic(cnt_p,1),:),[size(nodes,1) 1])).^2,2).^0.5;
    [~,In(cnt_p,1)] = min(D); 
    % extend 
    S_n(cnt_p,:) = pt(Ic(cnt_p,1),:) + n(cnt_p,:)*s_l;
end

if true
    figure
    hold on
    nn = 1;
    scatter3(pt(:,1),pt(:,2),pt(:,3),'.b')
    scatter3(ctr(1),ctr(2),ctr(3),'*g')
    scatter3(tet(:,1),tet(:,2),tet(:,3),'*r')
    scatter3(nodes(In,2),nodes(In,3),nodes(In,4),'or')
    scatter3(S_n(:,1),S_n(:,2),S_n(:,3),'.r')
    hold off
    grid on
    axis equal
end

spring = [In (size(nodes,1)+1 : size(nodes,1)+size(S_n,1))'];

nodes = [nodes; [(size(nodes,1)+1 : size(nodes,1)+size(S_n,1))' S_n]];
ref_nodes = nodes;

%% load deformations

% transformation from hires to tag1
load([analysis_folder '/' 'trans_T11mm_2_TAG.mat'])

%% transform data to tagged frames
% NOTE this can be done in a loop if needed

% output folder
OUT_folder = analysis_folder;

% timeframe
cnt_t = 1;

% head surface
Tmp = [H_ver(:,2:4) H_ver(:,1)*0+1]*F;
h_ver(:,2:4) = Tmp(:,1:3);
% csf surface
Tmp = [C_ver(:,2:4) C_ver(:,1)*0+1]*F;
c_ver(:,2:4) = Tmp(:,1:3);
% FE mesh
Tmp = [ref_nodes(:,2:4) ref_nodes(:,1)*0+1]*F;
nodes(:,2:4) = Tmp(:,1:3);

% save head surface
writeAvizoSurf(H_tri,h_ver(:,2:4),[OUT_folder '/' H_file(1:end-12) '_tag_' num2str(cnt_t) '.surf']);
% save csf surface
writeAvizoSurf(C_tri,c_ver(:,2:4),[OUT_folder '/' C_file(1:end-12) '_tag_' num2str(cnt_t) '.surf']);
% save mesh
save([OUT_folder '/' 'mesh_tag_' num2str(cnt_t)],'nodes','elements','element_type')

%% get imaging data to check 

% load APt data
Q_folder = interp_folder;
load([Q_folder '/' 'TAG1_L_' num2str(cnt_t) ])
load([Q_folder '/' 'TAG2_L_' num2str(cnt_t) ])
load([Q_folder '/' 'TAG3_L_' num2str(cnt_t) ])
 
%% save other quantities of interest

% center of gravity
Cg = [ctr 1]*F;
Cg = Cg(1:3)

% resulution/image size
Q_dim = size(QM1)
Q_box = [min([Q1(:) Q2(:) Q3(:)])
         max([Q1(:) Q2(:) Q3(:)])]

% save 
save([analysis_folder '/' 'track_vars'],'Cg','Q_dim','Q_box','spring')
     
     
%% plots

% check using coordinate transform
if true
    
    figure
    hold on
 
    check_im = 'tag_1';
    
    switch check_im
        case 'T1_1mm'
            % load MPRAGE 1mm images
            load([analysis_folder '/' 'MPRAGE_1mm'])
            % alter coordinates
            Tmp = [X1(:) X2(:) X3(:) X1(:)*0+1]*F;
            x1 = reshape(Tmp(:,1),size(X1));
            x2 = reshape(Tmp(:,2),size(X2));
            x3 = reshape(Tmp(:,3),size(X3));
            % display image
            ortho_slice_v4(x1,x2,x3,XM) 
            
         case 'tag_1'
            % display
            QM = QM1 + QM2 + QM3;
            % ortho_slice_v4(Q1,Q2,Q3,QM) 
            ortho_slice_v4(Q1,Q2,Q3,QM,[0 0],[0.5 0.5 0.5]) 
         
        case 'reg_results'
            % load
            load([analysis_folder '/' 'hires2tag_results'])
            % display
            ortho_slice_v4(Q1,Q2,Q3,QFCb,[0 0],[0.5 0.5 0.5]) 
            % ortho_slice_v4(Q1,Q2,Q3,QXMb,[0 0],[0.5 0.5 0.5]) 
          
    end
            
    % center of gravity
    scatter3(Cg(1),Cg(2),Cg(3),'*r')
    % scatter3(nodes(Ictr,2),nodes(Ictr,3),nodes(Ictr,4),'*g')
    
    % show surface
    % patch('Faces',C_tri,'Vertices',c_ver(:,2:4),'FaceColor','yellow','FaceAlpha',0.05)  
    patch('Faces',H_tri,'Vertices',h_ver(:,2:4),'FaceColor','red','FaceAlpha',0.5)  

    % show tracts
    % tract_view_v3(lengths,tracts,2);

    view(90,0)
end


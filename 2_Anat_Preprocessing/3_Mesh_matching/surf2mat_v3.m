function [vertices,triangles] = surf2mat_v3(filename,format) 
% [vertices,triangles] = surf2mat_v3(filename,format) 

if (nargin == 1)
    format = 'default';
end

% load nodes
fid = fopen(filename);

% read vertices
temp = textscan(fid,'%s',1,'Delimiter','\n');
while ~strcmp(temp{1},'Vertices');
    temp = textscan(fid,'%s %f',1,'Delimiter',' ');
end
n_vert = temp{2};
temp = textscan(fid,'%f%f%f',n_vert,'Delimiter',' ');
vertices = [(1:n_vert)' temp{1} temp{2} temp{3}];
% vertices = [temp{1} temp{2} temp{3}];

% read triangles
while ~strcmp(temp{1},'Triangles');
    temp = textscan(fid,'%s %f',1,'Delimiter',' ');
end
n_tri = temp{2};

switch format
    case 'amira'
        temp = textscan(fid,'%s%s%f%f%f',n_tri,'Delimiter',' ');
        triangles = [temp{3} temp{4} temp{5}];
    case 'preview'
        temp = textscan(fid,'%f%f%f',n_tri,'Delimiter',' ');
        triangles = [temp{1} temp{2} temp{3}];
    otherwise
        disp('using default format')
        temp = textscan(fid,'%f%f%f',n_tri,'Delimiter',' ');
        triangles = [temp{1} temp{2} temp{3}];
end

fclose(fid);

end
% Registers tagged sequence after tag removal
% this is useful for tracking (jumping avoidance)

%% housekeeping

close all
clear all
clc

% latest: basic test

%% analysis folder path

enable_folders

%% extract paramters

steps_n = PARAM.tracking.time_steps;

%% TAG1
% tagged data folder
Q_folder = filt_folder;

%% registration 

F = zeros(4,4,steps_n+1);
F(:,:,1) = eye(4);

% load fixed image
cnt_s = 1;
load([Q_folder '/' 'int_L_vars_' num2str(cnt_s)])
% get combined image
QMs = abs(QFC);
% blur image (Gaussian)
smooth_size = 15; % px 5
sigma = 0.21; % 0.1
QMs = smooth_v1(QMs,smooth_size,sigma);
% basic normalization
QMs = (QMs - min(QMs(:)))./max(QMs(:) - min(QMs(:)));
% view
% ortho_slice_v4(Q1,Q2,Q3,QMs), return

for cnt_f = 1:steps_n

    cnt_f
    % *** load images
    % load moving image
    cnt_t = cnt_f + 1;
    load([Q_folder '/' 'int_L_vars_' num2str(cnt_t)])
    % get combined image
    QMt = abs(QFC);
    % blur image (Gaussian)
    smooth_size = 15; % px
    sigma = 0.21;
    QMt = smooth_v1(QMt,smooth_size,sigma);
    % basic normalization
    QMt = (QMt - min(QMt(:)))./max(QMt(:) - min(QMt(:)));

    % *** register
    % set up initial mutual info registration
    [optimizer, metric] = imregconfig('monomodal');
    % options (manual)
    optimizer.MaximumIterations = 500;
    % optimizer.MinimumStepLength = 5e-4;
    %  moving first, then fixed
    tform = imregtform(QMt,QMs, ...
               'rigid',optimizer, metric, ...
               'PyramidLevels',2);
    % put transform in grid format used here 
    F_T = tform.T;
    F_T = [F_T(:,2) F_T(:,1) F_T(:,3:end)];
    F_T = [F_T(2,:); F_T(1,:); F_T(3:end,:)];
    % define coordinate transform
    [~,~,~,Fq_fwd,Fq_inv] = cart2im(Q1,Q2,Q3); 
    F_i = (Fq_fwd')*F_T*(Fq_inv');
    
    % store
    F(:,:,cnt_f+1) = F_i;

end

%% adapt mesh

% check using transformed image
if true
    qmt = imwarp(abs(QMt),tform,'OutputView',imref3d(size(QMt)));

    Tmp = [Q1(:) Q2(:) Q3(:) Q1(:)*0+1]*F_i;
    q1 = reshape(Tmp(:,1),size(Q1));
    q2 = reshape(Tmp(:,2),size(Q2));
    q3 = reshape(Tmp(:,3),size(Q3));

    figure
    subplot(131)
    ortho_slice_v4(Q1,Q2,Q3,QMt)
    title('moving')
    view(90,0)
    subplot(132)
    ortho_slice_v4(Q1,Q2,Q3,QMs)
    title('fixed')
    view(90,0)
    subplot(133)
    % ortho_slice_v4(q1,q2,q3,QMt)
    ortho_slice_v4(Q1,Q2,Q3,qmt)
    title('moved')
    view(90,0)
end


%% save

% save transformation
disp(['transformation saved in: ' analysis_folder '/' 'trans_TAG_N_TAG_1'])
save([analysis_folder '/' 'trans_TAG_N_TAG_1'],'F')
    



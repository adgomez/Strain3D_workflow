function [Um] = smooth_v1(U,smooth_size,sigma)
% [Um] = smooth_v1(U,smooth_size,sigma) gaussian smoothing

    % prepare smoothing weights
    xwgts = linspace(-1,1,2*smooth_size-1)';
    wgts = exp(-(xwgts/sigma).^2); wgts = wgts/sum(wgts);
    wgts_x = wgts; wgts_y = wgts'; wgts_z(1,1,:) =  wgts; 

    % smoothing u via comvolution
    IM_in = U;
    IM_in = convn(IM_in,wgts_x,'same');
    IM_in = convn(IM_in,wgts_y,'same');
    IM_in = convn(IM_in,wgts_z,'same');
    Um = IM_in;


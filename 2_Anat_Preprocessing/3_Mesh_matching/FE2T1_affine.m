% Takes an atlas FE mesh and matches it to a volunteer's brain.
% A surface of the FE mesh and the volunteer is expected.

%% Housekeeping

close all
clear all
clc

% latest: added reference to PARAM structure, compatibility with
% matlab-based pointcloud

%% analysis folder path

enable_folders

%% extract paramters

% registration target
traget_type = PARMA.anat.match_target;
% convex hull simpification
conv_calc = PARAM.anat.conv_calt;
% subsample factor
nn = PARAM.anat.point_subsample;
% manual expansion of final surface
manual_exp = PARAM.anat.manual_exp;
exp_r = PARAM.anat.matnual_exp_f;
   
%% add mapVBVD to path

mfilesfolder = 'icp_finite';
path(path,mfilesfolder);

mfilesfolder = 'FE_reg';
path(path,mfilesfolder);

%% load data

% location of atlas FE
% FE_folder = [engine_folder '/' '0_Atlases' '/' 'Collin27'];
% FE_folder = [engine_folder '/' '0_Atlases' '/' 'Collin27_Alt'];
FE_folder = [engine_folder '/' '0_Atlases' '/' 'SimpleBrain1'];
% FE_folder = [engine_folder '/' '0_Atlases' '/' 'VideoPhantom'];

% location of brain surface from volunteer
SB_folder = analysis_folder;

% output folder
OUT_folder = analysis_folder;

% template file (deforms)
T_file = 'csf_atl.surf';
[T,T_tri] = surf2mat_v3([FE_folder '/' T_file],'preview');
T_n = numel(T(:,1));
T_id = T(:,1);

% target (stationary)
switch traget_type
    case 'surface'
        S_file = 'BS_1mm.surf';
        [S,S_tri] = surf2mat_v3([SB_folder '/' S_file],'amira');
        S_n = numel(S(:,1));
        S_id = S(:,1);
    case 'pointcloud'
        S_file = 'SurfVars_1mm';
        load([analysis_folder '/' S_file])
        S_n = size(BS_pt,1);
        S_id = (1:S_n)';
        S = [S_id BS_pt];
end


% initial alginment
Q = get_Q_v2([0 0 1],pi);
Mi = eye(4); Mi(1:3,1:3) = Q;
T(:,2:4) = movepoints(Mi,T(:,2:4));
si = mean(S(:,2:4),1) - mean(T(:,2:4),1);
T(:,2:4) = T(:,2:4) + repmat(si,[T_n 1]);

% quick plot
figure
hold on
scatter3(T(:,2),T(:,3),T(:,4),'.b')
scatter3(S(:,2),S(:,3),S(:,4),'.k')
axis equal
title('Registration Point Couds--Before ')
legend(T_file,S_file)


%% subsample points

% master subspace indexes
It = find(T_id>0);
It = It(1:nn:end);
T_r = T(It,2:4);

% target subspace indexes
Is = find(S_id>02);
Is = Is(1:nn:end);
S_r = S(Is,2:4);

%% calculate convex hull

if conv_calc
    S_k = convhull(S_r(:,1),S_r(:,2),S_r(:,3));
    S_r = S_r(S_k,:);

    T_k = convhull(T_r(:,1),T_r(:,2),T_r(:,3));
    T_r = T_r(T_k,:);
end

%% register

% load ICP registration library
ICP_folder = 'icp_finite';
path(path,ICP_folder);

% Register the points
options.Registration = 'Affine';
options.TolX = 1e-6;
options.TolP = 1e-8;
options.Optimizer = 'fminlbfgs';

[t_r,M] = ICP_finite(S_r, T_r, options);

if manual_exp
   warning('manual expansion on')
   exp_M = exp_r*eye(3);
   M(1:3,1:3) = M(1:3,1:3)*exp_M;
end


%% trasform FE mesh coordinates 
% reference mesh (to save or load user-specific referenge geometry)

% read FE mesh 
load([FE_folder '/' 'mesh_atlas']);

% apply transform
nodes(:,2:4) = movepoints(Mi,nodes(:,2:4));
nodes(:,2:4) = nodes(:,2:4) + repmat(si,[numel(nodes(:,1)) 1]);
nodes(:,2:4) = movepoints(M,nodes(:,2:4));

%% save 

% save mesh
save([OUT_folder '/' 'mesh_T1_1mm'],'nodes','elements','element_type')

% save cerebrum surface
t = movepoints(M,T(:,2:4));
writeAvizoSurf(T_tri,t,[OUT_folder '/' T_file(1:end-5) '_T1_1mm.surf']);

% save head surface
P_file = 'head_atl.surf';
[P,P_tri] = surf2mat_v3([FE_folder '/' P_file],'preview');
P(:,2:4) = movepoints(Mi,P(:,2:4));
P(:,2:4) = P(:,2:4) + repmat(si,[numel(P(:,1)) 1]);
p = movepoints(M,P(:,2:4));
writeAvizoSurf(P_tri,p,[OUT_folder '/' P_file(1:end-5) '_T1_1mm.surf']);

%% visualization

% view angles
view_vector = [65 0];
b_box = [min(S_r,[],1); max(S_r,[],1)];

% registration plot
figure
subplot(121)
    hold on
    scatter3(T_r(:,1),T_r(:,2),T_r(:,3),'.b')
    scatter3(S_r(:,1),S_r(:,2),S_r(:,3),'ok')
    axis equal
    axis(b_box(:))
    title('Registration Point Couds--Before ')
    view(view_vector)
    L = legend(T_file,S_file);
    set(L,'Location','NorthEast','Interpreter','none');
    
subplot(122)
    hold on
    scatter3(t_r(:,1),t_r(:,2),t_r(:,3),'.b')
    scatter3(S_r(:,1),S_r(:,2),S_r(:,3),'ok')
    axis equal
    axis(b_box(:))
    title('Registration Point Couds--After ')
    view(view_vector)
    L = legend([T_file ' (deformed)'],S_file);
    set(L,'Location','NorthEast','Interpreter','none');
    
% mesh plot
figure
    nn = 5;
    hold on
    scatter3(t(:,1),t(:,2),t(:,3),'.r')
    scatter3(p(:,1),p(:,2),p(:,3),'.g')
    scatter3(S_r(:,1),S_r(:,2),S_r(:,3),'ok')
    % scatter3(nodes(1:nn:end,2),nodes(1:nn:end,3),nodes(1:nn:end,4),'.g')
    axis equal
    axis(b_box(:))
    title('Mesh Comparison')
    view(view_vector)
    L = legend([T_file ' (deformed)'],'atlas_mesh');
    set(L,'Location','NorthEast','Interpreter','none');

%% remove library

mfilesfolder = 'icp_finite';
rmpath(mfilesfolder)

mfilesfolder = 'FE_reg';
rmpath(mfilesfolder)




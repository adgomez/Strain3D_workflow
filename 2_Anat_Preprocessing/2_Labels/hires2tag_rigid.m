% Registers tagged volume after tag removal to blured T1 anatomical image.
% This script is used to label the tagged volumes, which help with
% interpretation of resuls and placement of mesh for HARP-FE

%% housekeeping

close all
clear all
clc

% latest: single registration to timeframe one, added head mask
% NOTE: something is up with combination of images: QM3 has a lowered
% magnitude

% TODO: since digital resolution is the same, a single filter could be used
% to smooth both images. 
% TODO: time registration ussing tagged images may be useful later

%% analysis folder path

enable_folders

%% extract paramters

% initialize steps
box_size = PARAM.anat.box_size;

% import affine registraion
fetch_affine = PARAM.anat.fetch_affine;
% registration target
fetch_affine_file = PARAM.anat.fetch_affine_file;

% additional displacement (mm, x y z)
manual_c = PARAM.anat.manual_trans;
% additional rotation (deg, x y z)
manual_r = PARAM.anat.manual_rot;

%% MPRAGE

% load MPRAGE image
load([analysis_folder '/' 'MPRAGE_1mm'])
T1 = XM;

% blur image  (Gaussian)
smooth_size = 30; % 10 px
sigma = 0.2;
XB = smooth_v1(XM,smooth_size,sigma);

% basic normalization
XB = (XB - min(XB(:)))./max(XB(:) - min(XB(:)));

% check (optional)
% ortho_slice_v4(X1,X2,X3,XB), return

%% TAG1
% tagged data folder
Q_folder = filt_folder;

% load first time frame
cnt_t = 1;
load([Q_folder '/' 'int_L_vars_' num2str(cnt_t)])

% get combined image
QB = abs(QFC);

% blur image (Gaussian)
smooth_size = 20; % px
sigma = 0.2;
QB = smooth_v1(QB,smooth_size,sigma);

% basic normalization
QB = (QB - min(QB(:)))./max(QB(:) - min(QB(:)));

% check (optional)
% figure, ortho_slice_v4(QB), return

%% arrange data according to hires2ref method

% image order
s = 1; % hi res
t = 2; 

% load MPRAGE images
TMP1{s} = X1; TMP2{s} = X2; TMP3{s} = X3; TMP4{s} = XB;
TMP1{t} = Q1; TMP2{t} = Q2; TMP3{t} = Q3; TMP4{t} = QB;

% merge
X1 = TMP1; X2 = TMP2; X3 = TMP3; XM = TMP4;

%% equalize resolution via interpolation

% approx centroids
for cnt_s = 1:size(XM,2)  
    IM = XM{cnt_s};
    Msk = (IM > mean(IM(:)));
    Ci(cnt_s,:) = mean([X1{cnt_s}(Msk(:)) X2{cnt_s}(Msk(:)) X3{cnt_s}(Msk(:))]); 
end
Ti = diff(Ci,1,1);

% stationary image
TM = XM{t};

% interopolation method
int_method = 'spline';
ext_method = 'none';

% get ndgrid equivalent coordinates
[~,~,~,F_fwd,~] = cart2im(X1{s},X2{s},X3{s}); 
Im = [X1{t}(:)-Ti(1) X2{t}(:)-Ti(2) X3{t}(:)-Ti(3) X1{t}(:)*0+1]*(F_fwd');
I1 = Im(:,1);
I2 = Im(:,2);
I3 = Im(:,3);

Fi = griddedInterpolant(XM{s},int_method,ext_method);
SM = reshape(Fi(I1,I2,I3),size(X1{t}));
SM(isnan(SM)) = 0;

% ortho_slice_v4(X1{t},X2{t},X3{t},SM), return

%% registration
% NOTE: this step occurs in intrinsic image coordinates

if ~fetch_affine
    % echo
    disp('Performing registration ... ')
    
    % set up initial mutual info registration
    [optimizer, metric] = imregconfig('multimodal');

    optimizer.InitialRadius = 0.009;
    optimizer.Epsilon = 1.5e-4;
    optimizer.GrowthFactor = 1.01;
    optimizer.MaximumIterations = 600;

    %  moving first, then fixed
    tform = imregtform(abs(SM),abs(TM), ...
               'rigid',optimizer, metric, ...
               'PyramidLevels',2);
           
    % put transform in grid format used here 
    F_T = tform.T;
    F_T = [F_T(:,2) F_T(:,1) F_T(:,3:end)];
    F_T = [F_T(2,:); F_T(1,:); F_T(3:end,:)];

    % define coordinate transform
    [~,~,~,F_fwd,F_inv] = cart2im(X1{t},X2{t},X3{t}); 
    Fr = (F_fwd')*F_T*(F_inv');

    % add initial displacement
    Ft = eye(4);
    Ft(4,1:3) = Ti;
    F = Ft*Fr;
else
    % import ANTS file
    disp(['Importing registration based on: ' fetch_affine_file] )
    
    if strcmp(fetch_affine_file,'null')
        F = eye(4);
    else
    
        % read ANTS file
        fid = fopen(fetch_affine_file);
        txt_cell = textscan(fid, '%s');
        fclose(fid);

        % get paramters
        P_range = [10 21];
        P_vec = (1:numel(P_range(1):P_range(2)))'*0;
        for cnt_c = 1:numel(P_vec)
            P_vec(cnt_c) = str2num(txt_cell{1}{P_range(1) + cnt_c - 1});
        end

        % get fixed paramters
        F_range = [23 25];
        F_vec = (1:numel(F_range(1):F_range(2)))'*0;
        for cnt_c = 1:numel(F_vec)
            F_vec(cnt_c) = str2num(txt_cell{1}{F_range(1) + cnt_c - 1});
        end

        % rotation
        F_RR = eye(4);
        F_RR(1:3,1:3) = reshape(P_vec(1:9),[3 3])';

        %  displacement
        F_CR = eye(4);
        F_CR(4,1:3) = F_vec;

        % subtract CR, rotate and add CR
        Ft = F_CR*F_RR*inv(F_CR); 

        % add translation at the end
        Ft(4,1:3) = Ft(4,1:3) - P_vec(10:12)';

        % retrieve single affine transformation
        F = Ft;

    end
    
end 

%% introduce manual modifiers

% initialize
F_m = eye(4);

% note the default is ZYX
R_m = eul2rotm([manual_r(3) manual_r(2) manual_r(1)]*pi/180);

% assemble
F_m(1:3,1:3) = R_m';
F_m(4,1:3) = manual_c;

% warn
TMP = abs(F_m - eye(4));
if any(0<TMP(:))
    warning('Manual correction was added to T1 to tag_1 registration')
end

% combine
F = F*F_m;

%% save

% save transformation
disp(['transformation saved in: ' analysis_folder '/' 'trans_T11mm_2_TAG'])
save([analysis_folder '/' 'trans_T11mm_2_TAG'],'F')
    
%% test output

% check using transformed image
if false
    sm = imwarp(abs(SM),tform,'OutputView',imref3d(size(SM)));

    figure
    subplot(221)
    ortho_slice_v4(X1{t},X2{t},X3{t},sm)
    view(90,90)
    subplot(222)
    ortho_slice_v4(X1{t},X2{t},X3{t},sm)
    view(90,0)
    
    subplot(223)
    ortho_slice_v4(X1{t},X2{t},X3{t},TM)
    view(90,90) 
    subplot(224)
    ortho_slice_v4(X1{t},X2{t},X3{t},TM)
    view(90,0) 
    
    % figure
    % ortho_slice_v4(sm-TM)
end

%% transformation test

% interpolate
% Fv = griddedInterpolant(T1,int_method,ext_method); % unblurred T1
Fv = griddedInterpolant(XM{s},int_method,ext_method); % blurred T1
Tmp = [TMP1{s}(:) TMP2{s}(:) TMP3{s}(:) TMP1{s}(:)*0+1]*F;
x1 = reshape(Tmp(:,1),size(TMP1{s}));
x2 = reshape(Tmp(:,2),size(TMP1{s}));
x3 = reshape(Tmp(:,3),size(TMP1{s}));

% sample in Q
[~,~,~,F_xi,~] = cart2im(x1,x2,x3); 
I_xi = (F_xi*([Q1(:) Q2(:) Q3(:) 0*TMP1{t}(:) + 1]'))';
Xt = reshape(Fv(I_xi(:,1),I_xi(:,2),I_xi(:,3)),size(Q1));
Xt(isnan(Xt)) = 0;
Xt = (Xt - min(Xt(:)))./max(Xt(:) - min(Xt(:)));

QFCb = XM{t};
QXMb = Xt;  
    
save([analysis_folder '/' 'hires2tag_results'],'QFCb','QXMb')

% check coordinate transform
if true

    % plot
    figure
    subplot(131)
    ortho_slice_v4(Q1,Q2,Q3,QFCb)
    view(90,90)
    
    subplot(132)
    ortho_slice_v4(Q1,Q2,Q3,QXMb) % shows registration result
    view(90,90)
    
    subplot(133)
    ortho_slice_v4(Q1,Q2,Q3,abs(QFCb - QXMb))
    view(90,90)

end



% Aligns a nifti segmentation file to a 1mm anatomical image. It also
% creates a surface based on the csf labels for registering a mesh for
% HARP-FE.

%% housekeeping

clc
close all
clear all

% latest: removed mesh labeling: this function
% TODO: a shrink-wrap surface definition would probably be better

%% analysis folder path

enable_folders

%% extract paramters

% initialize steps
label_nii = PARAM.anat.basic_seg_file; % target file
perm_v = PARAM.anat.sef_permute; % label permute 

%% load data file

niistr = nifti([label_nii]);
IM = niistr.dat(:,:,:); 
     
% load MPRAGE image
load([analysis_folder '/' 'MPRAGE_1mm'])

%% add lib to path

mfilesfolder = 'mfiles';
path(path,mfilesfolder);

%% match nii data to MPRAGE

% initial permute
IM = permute(IM,abs(perm_v));

% flip according to sign
% NOTE: there are better ways to do this
if (perm_v(1) < 0)
    IM = flip(IM,1);
end

if (perm_v(2) < 0)
    IM = flip(IM,2);
end

if (perm_v(3) < 0)
    IM = flip(IM,3);
end

% pick and place one label on the T1 image
TMP = XM;
% TMP(IM == max(IM(:))) = max(TMP(:));
% TMP(IM == 25) = max(TMP(:)); % 0    5    6   14   15   16   17   18   22   24   25

% NOTE: the basic labels appear to be:
% 0 = background
% 5 = scf or unknown
% 6 = ventricles
% 14 = cerebellum gray matter
% 15 = cerebral gray matter
% 16 = ? around ventricles
% 17 = ? around thalamus, hyppocampus, hypothalamus
% 18 = ? branch towards temporal lobe
% 22 = ? pons
% 24 = cerebellum white matter
% 25 = cerebral white matter

%% alter elements

% load mesh 
load([analysis_folder '/' 'mesh_T1_1mm'])

% pick label to be transfered
PM = (IM==6);

% calculate centroids
centroids = get_tet4_centroids(elements{1},nodes);

% interpolate onto centroids
[I1,I2,I3,F_fwd,F_inv] = cart2im(X1,X2,X3); 
Fi = griddedInterpolant(I1,I2,I3,single(PM),'nearest');
Ic = [centroids(:,2) centroids(:,3) centroids(:,4) centroids(:,4)*0+1]*F_fwd';
Im = 0<Fi(Ic(:,1),Ic(:,2),Ic(:,3));

% make changes to mesh
original_elements = elements;
elements{1}(Im,2) = 4; 
% 1 = rigid, 2 = "csf" support, 3 = tracking, 4 = pure support

% save
disp(['Saving additional variable called ''original_elements' ''])
save([analysis_folder '/' 'mesh_T1_1mm'],'nodes','elements','element_type','original_elements')
 


%% plots

show_label_match = true;
show_surface = true;
show_points = true;

% check labels
if show_label_match
    view_v = [90 0];
    figure
    TMP(PM) = max(TMP(:));
    ortho_slice_v4(X1,X2,X3,TMP,view_v,[0.35 0.5 0.45])
    
    % In = 1:100:size(nodes,1);
    % scatter3(nodes(In,2),nodes(In,3),nodes(In,4),'.')
    % scatter3(centroids(:,2),centroids(:,3),centroids(:,4),'.b')
    scatter3(centroids(Im,2),centroids(Im,3),centroids(Im,4),'.r')
end


%% remove scripts
rmpath(mfilesfolder)

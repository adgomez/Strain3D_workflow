
function centroids = get_tet4_centroids(elements,nodes)
% centroids = get_tet4_centroids_v1(elments,nodes);

centroids = zeros(numel(elements(:,1)),4);

centroids(:,1) = elements(:,1);
centroids(:,2) = mean([ nodes(elements(:,3),2) nodes(elements(:,4),2) nodes(elements(:,5),2) nodes(elements(:,6),2) ], 2 );
centroids(:,3) = mean([ nodes(elements(:,3),3) nodes(elements(:,4),3) nodes(elements(:,5),3) nodes(elements(:,6),3) ], 2 );
centroids(:,4) = mean([ nodes(elements(:,3),4) nodes(elements(:,4),4) nodes(elements(:,5),4) nodes(elements(:,6),4) ], 2 );




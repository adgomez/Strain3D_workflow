% Aligns a nifti segmentation file to a 1mm anatomical image. It also
% creates a surface based on the csf labels for registering a mesh for
% HARP-FE.

%% housekeeping

clc
close all
clear all

% latest: removed mesh labeling: this function
% TODO: a shrink-wrap surface definition would probably be better

%% analysis folder path

enable_folders

%% extract paramters

% initialize steps
label_nii = PARAM.anat.basic_seg_file; % target file
perm_v = PARAM.anat.sef_permute; % label permute 

%% load data file

niistr = nifti([label_nii]);
IM = niistr.dat(:,:,:); 
     
% load MPRAGE image
load([analysis_folder '/' 'MPRAGE_1mm'])

%% match nii data to MPRAGE

% initial permute
IM = permute(IM,abs(perm_v));

% flip according to sign
% NOTE: there are better ways to do this
if (perm_v(1) < 0)
    IM = flip(IM,1);
end

if (perm_v(2) < 0)
    IM = flip(IM,2);
end

if (perm_v(3) < 0)
    IM = flip(IM,3);
end

% pick and place one label on the T1 image
TMP = XM;
% TMP(IM == max(IM(:))) = max(TMP(:));
% TMP(IM == 25) = max(TMP(:)); % 0    5    6   14   15   16   17   18   22   24   25

% NOTE: the basic labels appear to be:
% 0 = background
% 5 = scf or unknown
% 6 = ventricles
% 14 = cerebellum gray matter
% 15 = cerebral gray matter
% 16 = ? around ventricles
% 17 = ? around thalamus, hyppocampus, hypothalamus
% 18 = ? branch towards temporal lobe
% 22 = ? pons
% 24 = cerebellum white matter
% 25 = cerebral white matter

%% generate surface for mesh matching

% perimeter
PM = bwperim(IM>0,26);

% coordinates
XPM = [X1(PM(:)>0) X2(PM(:)>0) X3(PM(:)>0)];

% downsample
nn = 10;
XPM = XPM(1:nn:end,:);

% triangulated convex hull
XPM_c = convhull(XPM);

% map as new set
Iu = unique(XPM_c);
BS_ver = [(1:numel(Iu))' XPM(Iu,:)];
BS_tri = convhull(BS_ver(:,2:4));

% define pointcloud (convex hull is not great, but the pointcloud can be registered instead)
BS_pt = XPM;

%% save

% save brain surface
writeAvizoSurf(BS_tri,BS_ver(:,2:4),[analysis_folder '/' 'BS_1mm.surf']);

% save brain surface information
save([analysis_folder '/' 'SurfVars_1mm'],'BS_tri','BS_ver','BS_pt')

% save labels
save([analysis_folder '/' 'Labels_1mm'],'X1','X2','X3','IM')

%% plots

show_label_match = true;
show_surface = true;
show_points = true;

% check labels
if show_label_match
    view_v = [90 0];
    figure
    TMP(PM) = max(TMP(:));
    ortho_slice_v4(X1,X2,X3,TMP,view_v,[0.35 0.5 0.45])
end

% check surfaces
if show_surface
    figure
    hold on
    ortho_slice_v4(X1,X2,X3,XM)
    patch('Faces',BS_tri,'Vertices',BS_ver(:,2:4),'FaceColor','yellow','FaceAlpha',0.5)  
end

% check pointcloud
if show_points
    figure
    hold on
    ortho_slice_v4(X1,X2,X3,XM)
    scatter3(BS_pt(:,1),BS_pt(:,2),BS_pt(:,3),'.b')  
end







% Loads anatomical images

%% housekeeping

close all
clear all
clc

% latest: adding support for PARAM structure
% TODO: make slice placement more robust

%% analysis folder path

enable_folders

%% add dicom reader library

mfilesfolder = 'dicom_reader';
path(path,mfilesfolder);

%% extract parameters

parent_folder = PARAM.anat.parent_folder;

%% load dicom data

% get directory listing
folder_struc = dir(parent_folder);

% initialize
data_N = 0; % slice data total

% *** find subfolders with dicom data
for cnt_f = 1:size(folder_struc,1)
    
    % check the subfolder name, look REF
    if (  (folder_struc(cnt_f).isdir) ...
         &(~isempty(strfind(folder_struc(cnt_f).name,'_MP2RAGE'))) )...
         % &(~isempty(strfind(folder_struc(cnt_f).name,'_REF')))   ) 
        
         % find all dicom files
         Files = dir([parent_folder '/' folder_struc(cnt_f).name '/*.dcm'] );
         
         % advance slice data total
         data_N = data_N + 1;
         
         % get data structure
         data_struc(data_N,1).folder = folder_struc(cnt_f).name;
         data_struc(data_N,1).name = Files(1).name;
         data_struc(data_N,1).Nt = size(Files,1);        
    end
    
end

%% throw error if no images were found

if ~exist('data_struc')
    error('no MPRAGE files were found')
end

%% extract 
% this will look for even number of subfolders
% images look weird, try inverting mag_first

% slice list prototype 
% SliceList = cell(1,floor(0.5*size(data_struc,1))); 
SliceList = cell(size(data_struc,1)); 
cnt_v = (1:size(data_struc,1));

for cnt_s = 1:size(SliceList,2)

    % reco type
    SliceList{cnt_s}.dynamic{1}.reco = 'DICOM';
    
    % initialize time loop
    storage_order = [3 1 2];
    IM1 = []; 
    PH1 = []; 
    
    % get slice data
    for cnt_t = 1:data_struc(cnt_s).Nt
        
        cnt_m = cnt_v(cnt_s); 
        
        % echo file name
        % [parent_folder '/' data_struc(cnt_m).folder '/']
        [data_struc(cnt_m).name(1:end-10) '_' sprintf('%0.5i',cnt_t)]
        
        % dynamic 1 (magnitude)
        SL_folder{1} = [parent_folder '/' data_struc(cnt_m).folder '/'];
        filein{1,1} = [data_struc(cnt_m).name(1:end-10) '_' sprintf('%0.5i',cnt_t)]; 
        [slice_data,slice_info] = siemens_Dicom_Load_Data_v3(SL_folder,filein,'SPAMM-LINES');
        IM1 = cat(1,IM1,permute(slice_data.slice{1}.dynamic{1}.phase{1}.magnitude,storage_order));
        
        % plane3D properties
        slice_info3D = Slice3d_info_dicom_v2(slice_info{1});
        SliceList{cnt_s}.Planes3D(cnt_t) = slice_info3D;
        SliceList{cnt_s}.PxlSpacing(cnt_t) = slice_data.slice{1};
        
        % tagging direction
        phase_d = slice_info{1}.InPlanePhaseEncodingDirection;
        if strcmp(phase_d,'ROW')
            SliceList{cnt_s}.dynamic{1}.tagDir = slice_info3D.e1;
        else
            SliceList{cnt_s}.dynamic{1}.tagDir = slice_info3D.e2;
        end
    
        clear slice_data slice_info
        
    end

    % image data
    SliceList{cnt_s}.dynamic{1}.Data = IM1; %.*exp(sqrt(-1)*(PH1*pi/2048 - pi));
 
end


%% merge volumes

% slice counter  
for cnt_s = 1:size(SliceList,2)

    % get images   
    TM = ipermute(SliceList{cnt_s}.dynamic{1}.Data,storage_order);
    dim = size(TM);

    % get slice data
    for cnt_t = 1:dim(3)

        % orienation vectors
        E1(cnt_t,:) = SliceList{cnt_s}.Planes3D(cnt_t).e1; 
        E2(cnt_t,:) = SliceList{cnt_s}.Planes3D(cnt_t).e2; 
        E3(cnt_t,:) = SliceList{cnt_s}.Planes3D(cnt_t).norm; 

        % slice centers
        ctr(cnt_t,:) = SliceList{cnt_s}.Planes3D(cnt_t).center;

        % matrix size
        mtr(cnt_t,:) = [SliceList{cnt_s}.Planes3D(cnt_t).rows ...
                        SliceList{cnt_s}.Planes3D(cnt_t).cols];

        % pixel spacing
        pxl(cnt_t,:) = SliceList{cnt_s}.PxlSpacing(cnt_t).PxlSpacing;
      
    end
    
    % *** merge into volumes (only interested in x)
    [x3,I3] = unique(ctr(:,1));
    [x3,Ip] = sort(x3,'descend');
    I3 = I3(Ip);
    
    XM{cnt_s} = TM;
    
    % *** define physical coordinates
    % 1D parameters 
    s1 = (1:mtr(1,1))';
    s2 = (1:mtr(1,2))';

    % 2D paramters
    [S1,S2,S3] = ndgrid(s1,s2,x3); 

    % physical space definition
    X1_tmp = pxl(1,1)*S1.*E1(1,1)/norm(E1(1,:)) + pxl(1,2)*S2.*E2(1,1)/norm(E2(1,:)) + S3.*E3(1,1);
    X2_tmp = pxl(1,1)*S1.*E1(1,2)/norm(E1(1,:)) + pxl(1,2)*S2.*E2(1,2)/norm(E2(1,:)) + S3.*E3(1,2);
    X3_tmp = pxl(1,1)*S1.*E1(1,3)/norm(E1(1,:)) + pxl(1,2)*S2.*E2(1,3)/norm(E2(1,:)) + S3.*E3(1,3);

    % center (wrtie as X1{cnt_s} for multiple res)
    X1{cnt_s} = X1_tmp - mean(X1_tmp(:)) + mean(ctr(I3,1));
    X2{cnt_s} = X2_tmp - mean(X2_tmp(:)) + mean(ctr(I3,2));
    X3{cnt_s} = X3_tmp - mean(X3_tmp(:)) + mean(ctr(I3,3));
      
    % resolution (for file naming)
    res(cnt_s) = pxl(1,1);
    
    % clear temps
    clear E1 E2 E3 ctr mtr pxl
    
end

disp(['found ' num2str(size(SliceList,2)) ' MPRAGE Files'])

TMP1 = X1;
TMP2 = X2;
TMP3 = X3;
TMP4 = XM;

%% save loop

for cnt_f = 1:size(TMP4,2)
    X1 = TMP1{cnt_f};
    X2 = TMP2{cnt_f};
    X3 = TMP3{cnt_f};
    XM = TMP4{cnt_f};
    out_file = ['MPRAGE_' num2str(res(cnt_f)) 'mm']
    save([analysis_folder '/' out_file],'X1','X2','X3','XM')
end

%% plot for checking

figure
ortho_slice_v4(X1,X2,X3,XM)
    
%% remove dicom reader library

rmpath(mfilesfolder)













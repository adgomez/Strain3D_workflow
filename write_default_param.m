% default parameters
% 
% TODO: Expand to include example dataset

%% header

formatSpec = '%% ---------------------------------------------------\n';
fprintf(fileID,formatSpec);
formatSpec = '%% Paramter definition file for 3D strain analysis \n';
fprintf(fileID,formatSpec);
formatSpec = ['%% Subject ID: ' subject_ID '\n'];
fprintf(fileID,formatSpec);
formatSpec = ['%% Created on: ' date '\n'];
fprintf(fileID,formatSpec);
formatSpec = '%% See README.md for more information \n';
fprintf(fileID,formatSpec);    
formatSpec = '%% Created by: write_default_param.m \n';
fprintf(fileID,formatSpec);  
formatSpec = '%% ---------------------------------------------------\n';
fprintf(fileID,formatSpec);  

formatSpec = '\n';
fprintf(fileID,formatSpec);  

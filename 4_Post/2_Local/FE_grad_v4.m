function [Psi_n_c,Psi_m_c,Psi_c,Ci] = FE_grad_v4(Psi_i,nod,tet,Im,type)
% [Psi_n_c,Psi_m_c,Psi_c,Ci] = FE_grad_v4(Psi_i,nod,tet,Im)
% finds centroidal gradients 
% Psi_i = potential
% nod = nodes list [i bc_i x_i y_i z_i]
% tet = tet connectivity [i m_i t1_i t2_i t3_i t4_i]
% Im = list of elements
% type = 'centroid' (default)
%        'nodes'
% 
% Psi_n_c = potential gradient direction (normalized)
% Psi_m_c = potential gradient magnitude
% Psi_c = potential
% Ci = cartesian coordinates

% version 4 adding option to evaluate at nodes
% strategy: calculate 4 times per tet, store at element -- average element
% values at the end

if nargin == 4
    type = 'centroid';
end

switch type
    case 'centroid'
        % call FE_grad at centroids
        isocoor = 0.25*[1 1 1];
        [Psi_n_c,Psi_m_c,Psi_c,Ci] = FE_grad(isocoor,Psi_i,nod,tet,Im);
        
    case 'nodes'    
        % set node locations
        Ie = [0 0 0
              1 0 0 
              0 1 0
              0 0 1];
        % initialize output vars
        Psi_n_c = zeros(numel(nod(:,1)),3,4);
        Psi_m_c = zeros(numel(nod(:,1)),1,4);
        Psi_c = zeros(numel(nod(:,1)),1,4);
        Ci = zeros(numel(nod(:,1)),3,4);
        W = zeros(numel(nod(:,1)),1,4);
        
        % loop over vertices
        for cnt_n = 1:4
            % get elementwise info
            isocoor = Ie(cnt_n,:);
            [N_n,N_m,N_p,N_c] = FE_grad(isocoor,Psi_i,nod,tet,Im);          
            Ip = tet(Im,cnt_n + 2);
            Psi_n_c(Ip,:,cnt_n) = N_n(:,2:4);
            Psi_m_c(Ip,:,cnt_n) = N_m(:,2);
            Psi_c(Ip,:,cnt_n) = N_p(:,2);
            Ci(Ip,:,cnt_n) = N_c(:,2:4);  
            W(Ip,:,cnt_n) = N_p(:,2)*0 + 1;  
        end
        
        % average
        W = sum(W,3); 
        Psi_n_c = [nod(:,1) sum(Psi_n_c,3)./repmat(W,[1 3])]; 
        Psi_m_c = [nod(:,1) sum(Psi_m_c,3)./repmat(W,[1])];
        Psi_c = [nod(:,1) sum(Psi_c,3)./repmat(W,[1])];
        Ci = [nod(:,1) sum(Ci,3)./repmat(W,[1 3])];
        
        % reduce
        Iw = 0<W;
        Psi_n_c = Psi_n_c(Iw,:);
        Psi_m_c = Psi_m_c(Iw,:);
        Psi_c = Psi_c(Iw,:);
        Ci = Ci(Iw,:);
        
    otherwise 
        warning('calculating potential gradient at the centroids')
        isocoor = 0.25*[1 1 1];
        [Psi_n_c,Psi_m_c,Psi_c,Ci] = FE_grad(isocoor,Psi_i,nod,tet,Im);
end

end

function [Psi_n_c,Psi_m_c,Psi_c,Ci] = FE_grad(isocoor,Psi_i,nod,tet,Im)
% [Psi_n_c,Psi_m_c,Psi_c,Ci] = FE_grad(isocoor,Psi_i,nod,tet,Im)
% finds gradients at isoparametric coordinate
% isocoor =  isoparametric coordinate
% Psi_i = potential
% nod = nodes list [i bc_i x_i y_i z_i]
% tet = tet connectivity [i m_i t1_i t2_i t3_i t4_i]
% Im = list of elements
% 
% Psi_n_c = normal gradient vector 
% Psi_m_c = gradient vector magnitude 
% Psi_c = potential
% Ci = cartesian coordinates

Psi_n_c = zeros(length(Im),4);
Psi_c = zeros(length(Im),2);
Ci = zeros(length(Im),4);

% element
for cnt_e = 1:numel(Im)
    
    % node numbers
    a_a = tet(Im(cnt_e),3:6)';
    
    % reference nodal positions
    X_a = nod(a_a,3:5)';
    
    % nodal quantity
    T_a = Psi_i(tet(Im(cnt_e),3:6)',2)';

    % isoparametric coordinate where gradient will be calculated 
    % (centroid = 0.25*[1 1 1])
    xi_i = isocoor;
    
    % Shape Functions and derivatives
    N(1,1) = 1 - sum(xi_i);
    N(2,1) = xi_i(1);
    N(3,1) = xi_i(2);
    N(4,1) = xi_i(3);
    
    dN_dxi = [-1 -1 -1
               1  0  0
               0  1  0
               0  0  1];
           
    % Jacobian
    J = [X_a(:,2)-X_a(:,1) X_a(:,3)-X_a(:,1) X_a(:,4)-X_a(:,1)];
    J_invt = [J^(-1)];

    % Product
    dNa_dX = dN_dxi*J_invt;

    % Gradient
    dTdX_i = T_a*dNa_dX;

    % store
    Psi_n_c(cnt_e,:) = [Im(cnt_e) dTdX_i];
    Psi_c(cnt_e,:) = [Im(cnt_e) (T_a*N)'];
    Ci(cnt_e,:) = [Im(cnt_e) (X_a*N)'];
end

% normalize
L = 1./(Psi_n_c(:,2).^2 + Psi_n_c(:,3).^2 + Psi_n_c(:,4).^2).^0.5; 

Psi_m_c = Psi_n_c(:,1);
Psi_m_c(:,2) = (Psi_n_c(:,2).^2 + Psi_n_c(:,3).^2 + Psi_n_c(:,4).^2).^0.5; 

if ~isempty(find(L==0))
    disp('WARNING: found gradients of zero length')
    L(L==0) = 0.01;
end

Psi_n_c(:,2:4) = Psi_n_c(:,2:4).*repmat(L,[1 3]);

end



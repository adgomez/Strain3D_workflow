clc
close all
clear all

% version 1: calculating distribution of potential

%% analysis folder path

enable_folders_v1

%% mesh in tagged coor

FE_folder = analysis_folder;
FE_file = 'mesh_tag_1';
load([FE_folder '/' FE_file])

%% find centroid
% TODO: vectorize for speed
% note since the mesh is not regular, one must find the centroid relative
% to the tetrahedral volume

M = 0;
RM = 0;

Ib = find(elements(:,2)~=1);
for cnt_e = 1:size(Ib,1)
    % get elements 
    En = elements(Ib(cnt_e),3:6);
    % define vertices
    a = nodes(En(1),2:4);
    b = nodes(En(2),2:4);
    c = nodes(En(3),2:4);
    d = nodes(En(4),2:4);
    % define tet volume (or mass, assuming constant density)
    m = (1/6)*abs(dot((a - d),cross((b - d),(c - d))));
    % calcuate tet centroid
    r = mean(nodes(En,2:4),1);
    % integrate
    M = M + m;
    RM = RM + r*m;
end

% per centroid definition
C = RM./M;

% find closest node
N = sum((nodes(:,2:4) - repmat(C,[size(nodes,1) 1])).^2,2).^0.5;

[~,Ic] = min(N);

%% find elements 

M_mat = unique(elements(:,2));
  
cnt_m = 1;

% get nodes in elements of a select material
IEA = find(elements(:,2)==M_mat(cnt_m));
INA = unique(elements(IEA,3:6));

% get nodes in elements in next material in the list
IEB = find(elements(:,2)==M_mat(cnt_m + 1));
INB = unique(elements(IEB,3:6));

%  find common nodes
In = intersect(INA,INB);

%% plot (for checking)

if false

    figure
    hold on

    nn = 100;

    Ik = 1:nn:size(nodes,1);

    scatter3(nodes(Ik,2),nodes(Ik,3),nodes(Ik,4),'.b')
    scatter3(nodes(In,2),nodes(In,3),nodes(In,4),'.g')
    scatter3(C(1),C(2),C(3),'*r')
    scatter3(nodes(Ic,2),nodes(Ic,3),nodes(Ic,4),'or')

    axis equal

end

%% files for potential dist

GEO_source_file = 'CL_v6.feb';

label_folder ='/home/dgomez/davidmeows/Brain_Mechanics/20161221/FreeSurf_test/raw_im';

% file names 
% file_a = [GEO_source_file(1:end-4) '_a.txt']; % matl
file_a = [label_folder '/' 'labeled_materials.txt'];
file_b = [GEO_source_file(1:end-4) '_b.txt']; % nodes
% file_c = [GEO_source_file(1:end-4) '_c.txt']; % elem
file_c = [label_folder '/' 'labeled_elements.txt'];
file_d = [GEO_source_file(1:end-4) '_d.txt']; % boundary
file_e = [GEO_source_file(1:end-4) '_e.txt']; % loads
file_f = [GEO_source_file(1:end-4) '_f.txt']; % step

%% save geometry 

% nodes
nod_out = [nodes(:,1) nodes(:,1) nodes(:,2:4)];
fileID = fopen(file_b,'w');
    formatSpec = '\t\t<Nodes>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t\t\t<node id="%i">%15.7e,%15.7e,%15.7e</node> \n';
    fprintf(fileID,formatSpec,[nod_out(:,1) nod_out(:,3:5)]');
    formatSpec = '\t\t</Nodes>\n';
    fprintf(fileID,formatSpec);
fclose(fileID);
 
% elements
if false
    fileID = fopen(file_c,'w'); 
    formatSpec = '\t\t <Elements>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t\t\t <tet4 id="%i" mat="%i">%6i,%6i,%6i,%6i</tet4> \n';
    fprintf(fileID,formatSpec,elements');
    formatSpec = '\t\t </Elements>\n';
    fprintf(fileID,formatSpec);

    formatSpec = '\t </Geometry>\n';
    fprintf(fileID,formatSpec);    
    fclose(fileID);
end

%% write boundary section for steps with fully prescribed kinematics step
           
fileID = fopen(file_d,'w');

formatSpec = '\t\t <Boundary>\n';
fprintf(fileID,formatSpec);
    formatSpec = '\t\t\t <fix>\n';
    fprintf(fileID,formatSpec);
        formatSpec = '\t\t\t\t <node id="%i" bc="t"/> \n';
        fprintf(fileID,formatSpec,[Ic]');
    formatSpec = '\t\t\t </fix>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t\t\t <prescribe>\n';
    fprintf(fileID,formatSpec);
        formatSpec = '\t\t\t\t <node id="%i" bc="t" lc="1">1</node> \n';
        fprintf(fileID,formatSpec,[In]');
    formatSpec = '\t\t\t </prescribe>\n';
    fprintf(fileID,formatSpec);
formatSpec = '\t\t </Boundary>\n';
fprintf(fileID,formatSpec);

fclose(fileID);

%% write loadcurves for all ring nodes

fileID = fopen(file_e,'w');

formatSpec = '\t\t <LoadData>\n';
fprintf(fileID,formatSpec);
    formatSpec = '\t\t\t <loadcurve id="1" type="linear">\n';
    fprintf(fileID,formatSpec);
        formatSpec = '\t\t\t\t <loadpoint>0,0</loadpoint>\n';
        fprintf(fileID,formatSpec);
        formatSpec = '\t\t\t\t <loadpoint>1,1</loadpoint>\n';
        fprintf(fileID,formatSpec); 
    formatSpec = '\t\t\t </loadcurve>\n';
    fprintf(fileID,formatSpec);
formatSpec = '\t\t </LoadData>\n';
fprintf(fileID,formatSpec);

fclose(fileID);

%% save step

file_str = 'nod_t.txt';

fileID = fopen(file_f,'w');
    formatSpec = '\t<Step name="Local Potential Dist.">\n';
    fprintf(fileID,formatSpec);
        formatSpec = '\t\t<Module type="heat"/>\n';
        fprintf(fileID,formatSpec);
        formatSpec = '\t\t<Control>\n';
        fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<time_steps>%i</time_steps>\n';
            fprintf(fileID,formatSpec,1);   
            formatSpec = '\t\t\t<step_size>%f</step_size>\n';
            fprintf(fileID,formatSpec,1);   
            formatSpec = '\t\t\t<analysis type="static"/>\n';
            fprintf(fileID,formatSpec);            
        formatSpec = '\t\t</Control>\n';
        fprintf(fileID,formatSpec);  
    formatSpec = '\t</Step>\n';
    fprintf(fileID,formatSpec);

    formatSpec = '\t<Output>\n';
    fprintf(fileID,formatSpec);
        formatSpec = '\t\t<plotfile type="febio">\n';
        fprintf(fileID,formatSpec);  
            formatSpec = '\t\t\t<var type="temperature"/>\n';
            fprintf(fileID,formatSpec);                       
        formatSpec = '\t\t</plotfile>\n';
        fprintf(fileID,formatSpec);
        
        formatSpec = '\t\t<logfile>\n';
        fprintf(fileID,formatSpec);  
           formatSpec = '\t\t\t<node_data data="T" name="Potential" file="%s" delim=","></node_data>\n';
           fprintf(fileID,formatSpec,file_str);         
        formatSpec = '\t\t</logfile>\n';
        fprintf(fileID,formatSpec);
        
    formatSpec = '\t</Output>\n';
    fprintf(fileID,formatSpec);  
formatSpec = '</febio_spec>\n';
fprintf(fileID,formatSpec); 

fclose(fileID);

%% solve for distribution of potentials

% concatenate files (UNIX)
ex_file = [GEO_source_file(1:end-4) '_post.feb'];

system(['cat ' file_a ' ' ...
               file_b ' ' ...
               file_c ' ' ...
               file_d ' ' ...
               file_e ' ' ...
               file_f ' > ' ex_file]);
          
           
% call solver (UNIX)
system(['./febio2.lnx64 -i ' ex_file])

%% get directions

% *** calculate vectors towards outside
% number of nodes
n_nod = numel(nodes(:,1));
% potential distribution
T_folder = pwd;
T_file = 'nod_t.txt';
% load nodal potential values
str_line = 4;
Ti = dlmread([T_folder '/' 'nod_t.txt'],',',[str_line 0 str_line+n_nod-1 1]);
[Tnc,~,Tc,Cc] = FE_grad_v4(Ti,[nodes(:,1:2) nodes(:,2:4)],elements,elements(:,1),'centroid');

% nan correction
Tnan_c = [0 0 1];
Tnc(isnan(Tnc(:,2)),2) = Tnan_c(1);
Tnc(isnan(Tnc(:,3)),3) = Tnan_c(2);
Tnc(isnan(Tnc(:,4)),4) = Tnan_c(3);

% *** get additional direction
d_dir = [0 0 1]; 
Snc = [Tnc(:,1) repmat(d_dir,[size(Tnc,1) 1])];
Wnc = cross(Tnc(:,2:4),Snc(:,2:4),2);
Wnc = [Tnc(:,1) Wnc./sum(Wnc.^2,2).^0.5]; 

% nan correction
Wnan_c = [1 0 0];
Wnc(isnan(Wnc(:,2)),2) = Wnan_c(1);
Wnc(isnan(Wnc(:,3)),3) = Wnan_c(2);
Wnc(isnan(Wnc(:,4)),4) = Wnan_c(3);

%% save useful variables

save([analysis_folder '/' 'loc_P1_potentials'],'Cc','Tc','Ti','Tnc','Wnc','d_dir')

%% check

if false

    figure
    hold on

    nn = 100;

    Ik = (1:nn:size(elements,1));

    scatter3(Cc(Ik,2),Cc(Ik,3),Cc(Ik,4),'.b')
    quiver3(Cc(Ik,2),Cc(Ik,3),Cc(Ik,4),Tnc(Ik,2),Tnc(Ik,3),Tnc(Ik,4),5,'r')

    axis equal

end


%%  write local direction section

file_c = 'ED_Potential_z.txt';

% elementdata
fileID = fopen(file_c,'w'); 
    formatSpec = '\t\t <ElementData>\n';
    fprintf(fileID,formatSpec);
    
    formatSpec = '\t\t\t<element id="%i"> <mat_axis type="vector"><a>%f,%f,%f</a><d>%f,%f,%f</d></mat_axis></element>\n';
    % formatSpec = '\t\t\t<mat_axis id="%i"> <a>%f,%f,%f</a> <d>%f,%f,%f</d> </mat_axis> \n';
    fprintf(fileID,formatSpec,[Tnc Wnc(:,2:4)]');
    formatSpec = '\t\t </ElementData>\n';
    fprintf(fileID,formatSpec);
    
    formatSpec = '\t </Geometry>\n';
    fprintf(fileID,formatSpec);     
fclose(fileID);












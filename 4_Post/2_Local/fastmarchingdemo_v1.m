clc
close all
clear all


%% image definition

% matrix size
i = 32;
j = 30;
k = 31;

% bounding box
bb = [-1 -1 -1; 1 1 1];

% geometrical space
[X,Y,Z] = ndgrid(linspace(bb(1,1),bb(2,1),i), ... 
               linspace(bb(1,2),bb(2,2),j), ...
               linspace(bb(1,3),bb(2,3),k));

% image definition
r1 = 0.8;
A = X.^2 + Y.^2 + Z.^2 < r1^2;

r1 = 0.4;
B = X.^2 + Y.^2 + Z.^2 < r1^2;

% boundary conditions at A
[Gj,Gi,Gk] = gradient(double(A));
BC_A = (Gi.^2 + Gj.^2 + Gk.^2)>0;

% boundary conditions at B
[Gj,Gi,Gk] = gradient(double(B));
BC_B = (Gi.^2 + Gj.^2 + Gk.^2)>0;

% image definition
IM = double((A - B)>0);
IM(BC_A) = 2;
IM(BC_B) = 3;

% mask defintion
Ip = IM>0;

%% calculate distances

% index matrix
[I,J,K] = ndgrid(1:i,1:j,1:k);

% define inside points in terms of indexes
P = [I(Ip) J(Ip) K(Ip)];

% call pixelwise distance calc routine
D_in = msfm3d(ones(size(Ip)), P', true, true); 

% define outsie points in terms of indexes
P = [I(~Ip) J(~Ip) K(~Ip)];

% call pixelwise distance calc routine
D_out = msfm3d(ones(size(IM)), P', true, true); 

% distance map
D = D_in - D_out;

% smooth
D = smooth_v1(D,10,.3);


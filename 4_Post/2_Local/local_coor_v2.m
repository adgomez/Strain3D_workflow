clc
close all
clear all

% version 2: calculating distance from inside of the skull

%% analysis folder path

enable_folders_v1

%% mesh in tagged coor

FE_folder = analysis_folder;
FE_file = 'mesh_tag_1';
load([FE_folder '/' FE_file])

%% find labeled volume

x_box = [min(nodes(:,2:4))
         max(nodes(:,2:4))];
     
x_res = [2 2 2];
    
x1 = (x_box(1,1):x_res(1):x_box(2,1)+x_res(1))';
x2 = (x_box(1,2):x_res(2):x_box(2,2)+x_res(2))';
x3 = (x_box(1,3):x_res(3):x_box(2,3)+x_res(3))';

[X1,X2,X3] = ndgrid(x1,x2,x3);

TR = triangulation(elements(:,3:6),nodes(:,2:4));

Ii = pointLocation(TR,[X1(:) X2(:) X3(:)]);

IM = reshape(Ii,size(X1));
IM(isnan(IM)) = 0;

Im = Ii((Ii>0)&(~isnan(Ii)));

Ip = IM*0;
Ip((Ii>0)&(~isnan(Ii))) = elements(Im,2);

Ip = Ip>1;

%% fast marching

% index matrix
[I,J,K] = ndgrid(1:size(Ip,1),1:size(Ip,2),1:size(Ip,3));

% define inside points in terms of indexes
P = [I(Ip) J(Ip) K(Ip)];

% call pixelwise distance calc routine
D_in = msfm3d(ones(size(Ip)), P', true, true); 

% define outsie points in terms of indexes
P = [I(~Ip) J(~Ip) K(~Ip)];

% call pixelwise distance calc routine
D_out = msfm3d(ones(size(Ip)), P', true, true); 

% distance map
D = D_in - D_out;

% smooth
% D = smooth_v1(D,10,.3);

%% convect

F = griddedInterpolant(X1,X2,X3,D);
Di = F(nodes(:,2),nodes(:,3),nodes(:,4))*mean(x_res);
dlmwrite('distances.txt',[nodes(:,1) Di*0 Di*mean(x_res)])

%% get directions

% *** calculate vectors towards outside
[Tnc,~,Dc,Cc] = FE_grad_v4([Di Di],[nodes(:,1:2) nodes(:,2:4)],elements,elements(:,1),'centroid');

% nan correction
Tnan_c = [0 0 1];
Tnc(isnan(Tnc(:,2)),2) = Tnan_c(1);
Tnc(isnan(Tnc(:,3)),3) = Tnan_c(2);
Tnc(isnan(Tnc(:,4)),4) = Tnan_c(3);

% *** get additional direction
d_dir = [0 0 1]; 
Snc = [Tnc(:,1) repmat(d_dir,[size(Tnc,1) 1])];
Wnc = cross(Tnc(:,2:4),Snc(:,2:4),2);
Wnc = [Tnc(:,1) Wnc./sum(Wnc.^2,2).^0.5]; 

% nan correction
Wnan_c = [1 0 0];
Wnc(isnan(Wnc(:,2)),2) = Wnan_c(1);
Wnc(isnan(Wnc(:,3)),3) = Wnan_c(2);
Wnc(isnan(Wnc(:,4)),4) = Wnan_c(3);

%% save useful variables

save([analysis_folder '/' 'loc_D1_distances'],'Cc','Dc','Di','Tnc','Wnc','d_dir')

%% check

if false

    figure
    hold on

    nn = 100;

    Ik = (1:nn:size(elements,1));

    scatter3(Cc(Ik,2),Cc(Ik,3),Cc(Ik,4),'.b')
    quiver3(Cc(Ik,2),Cc(Ik,3),Cc(Ik,4),Tnc(Ik,2),Tnc(Ik,3),Tnc(Ik,4),5,'r')

    axis equal

end


%%  write local direction section

file_c = 'ED_Distance_z.txt';

% elementdata
fileID = fopen(file_c,'w'); 
    formatSpec = '\t\t <ElementData>\n';
    fprintf(fileID,formatSpec);
    
    formatSpec = '\t\t\t<element id="%i"> <mat_axis type="vector"><a>%f,%f,%f</a><d>%f,%f,%f</d></mat_axis></element>\n';
    % formatSpec = '\t\t\t<mat_axis id="%i"> <a>%f,%f,%f</a> <d>%f,%f,%f</d> </mat_axis> \n';
    fprintf(fileID,formatSpec,[Tnc Wnc(:,2:4)]');
    formatSpec = '\t\t </ElementData>\n';
    fprintf(fileID,formatSpec);
    
    formatSpec = '\t </Geometry>\n';
    fprintf(fileID,formatSpec);     
fclose(fileID);



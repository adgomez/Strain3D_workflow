clc 
close all
clear all

% latest: adding new "enable folders" approach
%  TODO: move tracts export to DTI processing script

%% analysis folder path

enable_folders

%% load necessary data

% T1 (for checking placement)
load([analysis_folder '/' 'MPRAGE_1mm'])
T1 = X1;
T2 = X2;
T3 = X3;
TM = XM;

% DTI
load([DTI_folder '/' 'DTI_T1_1mm'])

% load mesh
load([analysis_folder '/' 'mesh_T1_1mm'])

%% interpolate fiber data
% This could happen at the elements or nodes

% get target
a_x = get_tet4_centroids(elements,nodes);
% a_x = nodes;

% pick element set
% Ie = a_x(:,1); % all
% Ie = find(elements(:,2)==4); % elements of select material
Ie = unique(elements(1<elements(:,2),3:6)); % nodes of select material(s)
% a_x = a_x(Ie,:);

% set coordinates
[~,~,~,Fi] = cart2im(X1,X2,X3);
Ind = round(Fi*[a_x(:,2:4)'; ones(1,numel(a_x(:,1)))])';

% interpolate
VI = griddedInterpolant(V1,'nearest','nearest');
a(:,1) = VI(Ind(:,1),Ind(:,2),Ind(:,3));
VI = griddedInterpolant(V2,'nearest','nearest');
a(:,2) = VI(Ind(:,1),Ind(:,2),Ind(:,3));
VI = griddedInterpolant(V3,'nearest','nearest');
a(:,3) = VI(Ind(:,1),Ind(:,2),Ind(:,3));

% remove any nan
a(isnan(a(:,1)),1) = 0;
a(isnan(a(:,2)),2) = 0;
a(isnan(a(:,3)),3) = 1;

% normalize 
N = sum(a.^2,2).^0.5;
a = a./repmat(N,[1 3]);
a(isnan(a(:,1)),1) = 0;
a(isnan(a(:,2)),2) = 0;
a(isnan(a(:,3)),3) = 1;

% d is any vector orthogonal to a 
d = a*0;
for cnt_f = 1:size(a,1)
    Mk = null(a(cnt_f,:));
    d(cnt_f,:) = Mk(:,1)';
end

save([DTI_folder '/' 'DTI_FiberInfo_T1_1mm'],'a_x','a','d')

%% check plot

show_t1 = true;
show_eigvect = true;
show_centroids = false;
show_meshfibers = true;

% deformed images in reference coordinates
if true
    figure
    hold on
    
    % percent in Z direction
    sp = 0.5;
    x_box = [min(X1(:)) min(X2(:)) min(X3(:)) 
             max(X1(:)) max(X2(:)) max(X3(:))];
     
    height = x_box(1,3) + sp*abs(diff(x_box(:,3)));     
    
    % FA background
    % ortho_slice_v4(X1,X2,X3,FA,[],[0.5 0.5 sp]);

    % T1 1mm
    ortho_slice_v4(T1,T2,T3,TM);
    if show_t1
        [~,ind] = min( abs(T3(:) - height) );   
        [slc(1),slc(2),slc(3)] = ind2sub(size(T1),ind);
        slice_view_v4(T1,T2,T3,TM,slc(1),1)
    end
    
    % eigenvectors
    if show_eigvect
        
        [~,ind] = min( abs(X3(:) - height) );   
        [slc(1),slc(2),slc(3)] = ind2sub(size(X1),ind);      
        vector_view_v2(X1,X2,X3,V1,V2,V3,1,slc(3),3,0,'r');
        vector_view_v2(X1,X2,X3,V1,V2,V3,1,slc(3),3,2,'r');
    end
    
    % axis
    quiver3(0,0,0,100,0,0,0,'r','LineWidth',3)
    quiver3(0,0,0,0,100,0,0,'g','LineWidth',3)
    quiver3(0,0,0,0,0,100,0,'b','LineWidth',3)
    
    % centroids
    if show_centroids
        In = 1:30:size(a_x,1);
        scatter3(a_x(In,2),a_x(In,3),a_x(In,4),'.r')
    end
    
    % mesh fibers
    if show_meshfibers
        tol = 3;
        Im = find( (height-tol <= a_x(:,4))&(a_x(:,4) <= height+tol) );
        
        In = Im(1:1:end);
        
        quiver3(a_x(In,2),a_x(In,3),a_x(In,4), ...
                a(In,1),a(In,2),a(In,3),0,'g')
            
        quiver3(a_x(In,2),a_x(In,3),a_x(In,4), ...
                -a(In,1),-a(In,2),-a(In,3),0,'g')
    end
    
    view(90,0)
    
end





clc
close all
clear all

% latest: adds supplemental varaiables
enable_folders

mag_max = 0.1;
steps_n = 12;

%% transfer magnitude

do_mag = true;

if do_mag
    
    % load MPRAGE image
    load([analysis_folder '/' 'MPRAGE_1mm'])
    
    % load mesh
    load([analysis_folder '/' 'mesh_T1_1mm'])
    
    % interpolate labels
    [~,~,~,F_fwd,F_inv] = cart2im(X1,X2,X3);
    F_l = griddedInterpolant(XM,'nearest');
    In = [nodes(:,2) nodes(:,3) nodes(:,4) ones(size(nodes(:,2)))]*(F_fwd');
    n_m = F_l(In(:,1),In(:,2),In(:,3));

    n_m = mag_max*(n_m - min(n_m))/max(n_m - min(n_m));
    
    % save labeled mesh
    dlmwrite([analysis_folder '/' 'T1_magnitude.txt'],[nodes(:,1) repmat(n_m,[1 steps_n])],'delimiter',',','precision','%.6g');  
end
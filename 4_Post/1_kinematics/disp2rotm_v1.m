function M = disp2rotm_v1(X,Y,Z,x,y,z,Msk)
% M = disp2rotm_v1(X,Y,Z,x,y,z,Msk) fits rigid deformation based on
% coordinates [X,Y,Z] (ref), and [x,y,z] (cur)


%% downsample

if (nargin==6)
    Msk = 1:numel(X);
end    
  
if (numel(Msk) == 1)
    nn = Msk;
    % ref coordinates
    Xn = X(1:nn:end);
    Yn = Y(1:nn:end);
    Zn = Z(1:nn:end);
    % cur coor
    xn = x(1:nn:end);
    yn = y(1:nn:end);
    zn = z(1:nn:end);
else
    % ref
    Xn = X(Msk);
    Yn = Y(Msk);
    Zn = Z(Msk);
    % cur
    xn = x(Msk);
    yn = y(Msk);
    zn = z(Msk);
end

%% fit

% get landmarks
Ti = [xn(:) yn(:) zn(:)];
Si = [Xn(:) Yn(:) Zn(:)];

% centroid
Th = mean(Ti,1);
Sh = mean(Si,1);

% centered points
Tc = Ti - repmat(Th,length(Ti),1);
Sc = Si - repmat(Sh,length(Si),1);

% covariance
Cov = (Tc')*(Sc')';

% rotation
[U,~,V] = svd(Cov);
c_1 = V*U';

% rigid displacement
c_0 = Sh - (c_1*Th')';

%% define affine matrix

M = eye(4);
M(1:3,4) = c_0;
M(1:3,1:3) = c_1;

end

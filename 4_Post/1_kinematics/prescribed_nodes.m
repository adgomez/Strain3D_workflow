clc
close all
clear all

% latest: removing step back to ref. 
%  see 05/24/2017 for removed features
% TODO: get labels for better material definition 
% ([analysis_folder '/' 'Labels_1mm'],'X1','X2','X3','IM')

%% analysis folder path

enable_folders

%% load

% tag 1
FE_file = 'mesh_tag_1'; 
load([analysis_folder '/' FE_file])

% HARP-FE run to be fetched
run_n = 4; 

% load nodal displacmements
HFE_run_folder = [analysis_folder '/'  'HFE_run' num2str(run_n)];
disp_file = 'nodal_disp';
load([HFE_run_folder '/' disp_file])
% defaults reference
ref_nodes = nodes;
ref_elements = elements;

%% define output

% find size of displacement teps
dim = size(nod_u);

% time vector in real world units
dt = 0.1; 
r_time = (0:dt:dt*(dim(3)-1))';

%% file names (UNIX)

GEO_source_file = 'CM_v6.feb';

% nominal file names 
file_a = [GEO_source_file(1:end-4) '_a.txt']; % matl
file_b = [GEO_source_file(1:end-4) '_b.txt']; % nodes
file_c = [GEO_source_file(1:end-4) '_c.txt']; % elem
file_d = ''; % elementdata (see "local directions" below)
file_e = [GEO_source_file(1:end-4) '_e.txt']; % boundary
file_f = [GEO_source_file(1:end-4) '_f.txt']; % loads
file_g = [GEO_source_file(1:end-4) '_g.txt']; % step

% get HARP-FE materials
% file_a = [HFE_run_folder '/' 'CM_v7_a.txt']; % matl

% local directions
local_dir = false;
if local_dir
    Dir_source_folder = DTI_folder;
    Dir_source_file = 'ElementData_tag_1.txt';
    % Dir_source_file = 'ED_Distance_z.txt';
    % Dir_source_file = 'ED_Potential_z.txt';
    
    file_d = [Dir_source_folder '/' Dir_source_file]; % elementdata
end

%% save geometry 

% file_b = [HFE_run_folder '/' 'CM_v7_b.txt']; % matl
% file_c = [HFE_run_folder '/' 'CM_v7_c.txt']; % matl

% nodes
nod_out = [ref_nodes(:,1) ref_nodes(:,1) ref_nodes(:,2:4)];
fileID = fopen(file_b,'w');
    formatSpec = '\t\t<Nodes>\n';
    fprintf(fileID,formatSpec);
    formatSpec = '\t\t\t<node id="%i">%15.7e,%15.7e,%15.7e</node> \n';
    fprintf(fileID,formatSpec,[nod_out(:,1) nod_out(:,3:5)]');
    formatSpec = '\t\t</Nodes>\n';
    fprintf(fileID,formatSpec);
fclose(fileID);

% elements
fileID = fopen(file_c,'w'); 
    formatSpec = '\t\t <Elements>\n';
    fprintf(fileID,formatSpec);

    for cnt_m = 1:numel(element_type) 
        elem_type_i = element_type{cnt_m};
        switch elem_type_i

        case 'hex8'
            formatSpec = '\t\t\t <hex8 id="%i" mat="%i">%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i</hex8> \n';
            fprintf(fileID,formatSpec,elements{cnt_m}'); 
        case 'hex20'
            formatSpec = '\t\t\t <hex20 id="%i" mat="%i">%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i,%6i</hex20> \n';
            fprintf(fileID,formatSpec,elements{cnt_m}');  
        case 'tet4'
            formatSpec = '\t\t\t <tet4 id="%i" mat="%i">%6i,%6i,%6i,%6i</tet4> \n';
            fprintf(fileID,formatSpec,elements{cnt_m}');
        case 'penta6'
            formatSpec = '\t\t\t <penta6 id="%i" mat="%i">%6i,%6i,%6i,%6i,%6i,%6i</penta6> \n';
            fprintf(fileID,formatSpec,elements{cnt_m}');
        otherwise
            error('undefined element type')
        end
    end

    formatSpec = '\t\t </Elements>\n';
    fprintf(fileID,formatSpec);

    if ~local_dir
        formatSpec = '\t </Geometry>\n';
        fprintf(fileID,formatSpec);    
    end 
fclose(fileID);
     
%% select nodes to be written in to kinnematics prescription

% number of nodes to be evaluated (downsample for testing purposes)
r_In = ref_nodes(:,1,1);

% number of timepoints to be evaluated
r_steps = (1:numel(r_time))';

% select prescribed nodes 
r_Ulist = nod_u(r_In,:,r_steps);


%% write boundary section for steps with fully prescribed kinematics step
 
fileID = fopen(file_e,'w');

formatSpec = '\t\t <Boundary>\n';
fprintf(fileID,formatSpec);
formatSpec = '\t\t\t <prescribe>\n';
fprintf(fileID,formatSpec);
    for cnt_l = 1:numel(r_In)
        formatSpec = '\t\t\t\t <node id="%i" bc="x" lc="%i">1</node> \n';
        fprintf(fileID,formatSpec,[r_In(cnt_l) cnt_l]');
    end
    for cnt_l = 1:numel(r_In)
        formatSpec = '\t\t\t\t <node id="%i" bc="y" lc="%i">1</node> \n';
        fprintf(fileID,formatSpec,[r_In(cnt_l) numel(r_In)+cnt_l]');
    end
    for cnt_l = 1:numel(r_In)
        formatSpec = '\t\t\t\t <node id="%i" bc="z" lc="%i">1</node> \n';
        fprintf(fileID,formatSpec,[r_In(cnt_l) 2*numel(r_In)+cnt_l]');
    end 
formatSpec = '\t\t\t </prescribe>\n';
fprintf(fileID,formatSpec);
formatSpec = '\t\t </Boundary>\n';
fprintf(fileID,formatSpec);

fclose(fileID);

%% write loadcurves for all nodes

fileID = fopen(file_f,'w');

formatSpec = '\t\t <LoadData>\n';
fprintf(fileID,formatSpec);
    for cnt_l = 1:numel(r_In)
        formatSpec = '\t\t\t <loadcurve id="%i" type="linear">\n';
        fprintf(fileID,formatSpec,cnt_l);
        for cnt_t = 1:numel(r_steps)
            formatSpec = '\t\t\t\t <loadpoint>%f,%f</loadpoint>\n';
            fprintf(fileID,formatSpec,[r_time(cnt_t) r_Ulist(cnt_l,1,cnt_t)]');
        end  
        formatSpec = '\t\t\t </loadcurve>\n';
        fprintf(fileID,formatSpec);
    end
    for cnt_l = 1:numel(r_In)
        formatSpec = '\t\t\t <loadcurve id="%i" type="linear">\n';
        fprintf(fileID,formatSpec,numel(r_In)+cnt_l);
        for cnt_t = 1:numel(r_steps)
            formatSpec = '\t\t\t\t <loadpoint>%f,%f</loadpoint>\n';
            fprintf(fileID,formatSpec,[r_time(cnt_t) r_Ulist(cnt_l,2,cnt_t)]');
        end  
        formatSpec = '\t\t\t </loadcurve>\n';
        fprintf(fileID,formatSpec);
    end
    for cnt_l = 1:numel(r_In)
        formatSpec = '\t\t\t <loadcurve id="%i" type="linear">\n';
        fprintf(fileID,formatSpec,2*numel(r_In)+cnt_l);
        for cnt_t = 1:numel(r_steps)
            formatSpec = '\t\t\t\t <loadpoint>%f,%f</loadpoint>\n';
            fprintf(fileID,formatSpec,[r_time(cnt_t) r_Ulist(cnt_l,3,cnt_t)]');
        end  
        formatSpec = '\t\t\t </loadcurve>\n';
        fprintf(fileID,formatSpec);
    end
formatSpec = '\t\t </LoadData>\n';
fprintf(fileID,formatSpec);

fclose(fileID);

%% save step

file_str = ['HFE_strains.txt'];
file_pstr = ['HFE_Pstrains.txt'];
file_Dgrad = ['HFE_Dgradient.txt'];
file_loc_str = ['HFE_DTI_strains.txt'];

fileID = fopen(file_g,'w');
    formatSpec = '\t<Step name="HARP-FE POST">\n';
    fprintf(fileID,formatSpec);
        formatSpec = '\t\t<Module type="solid"/>\n';
        fprintf(fileID,formatSpec);
        formatSpec = '\t\t<Control>\n';
        fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<time_steps>%i</time_steps>\n';
            fprintf(fileID,formatSpec,numel(r_time)-1);   
            formatSpec = '\t\t\t<step_size>%f</step_size>\n';
            fprintf(fileID,formatSpec,dt);   
            formatSpec = '\t\t\t<max_refs>5</max_refs>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<max_ups>10</max_ups>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<dtol>0.001</dtol>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<etol>0.01</etol>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<rtol>0</rtol>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<lstol>0.9</lstol>\n';
            fprintf(fileID,formatSpec);   
            formatSpec = '\t\t\t<time_stepper>\n';
            fprintf(fileID,formatSpec);   
                formatSpec = '\t\t\t\t<dtmin>0.01</dtmin>\n';
                fprintf(fileID,formatSpec);               
                formatSpec = '\t\t\t\t<dtmax>0.1</dtmax>\n';
                fprintf(fileID,formatSpec);  
                formatSpec = '\t\t\t\t<max_retries>2</max_retries>\n';
                fprintf(fileID,formatSpec);               
                formatSpec = '\t\t\t\t<opt_iter>10</opt_iter>\n';
                fprintf(fileID,formatSpec);  
            formatSpec = '\t\t\t</time_stepper>\n';
            fprintf(fileID,formatSpec);
            formatSpec = '\t\t\t<analysis type="static"/>\n';
            fprintf(fileID,formatSpec);            
        formatSpec = '\t\t</Control>\n';
        fprintf(fileID,formatSpec);  
    formatSpec = '\t</Step>\n';
    fprintf(fileID,formatSpec);

    formatSpec = '\t<Output>\n';
    fprintf(fileID,formatSpec);
        formatSpec = '\t\t<plotfile type="febio">\n';
        fprintf(fileID,formatSpec);  
            formatSpec = '\t\t\t<var type="displacement"/>\n';
            fprintf(fileID,formatSpec);          
            formatSpec = '\t\t\t<var type="relative volume"/>\n';
            fprintf(fileID,formatSpec); 
            formatSpec = '\t\t\t<var type="stress"/>\n';
            fprintf(fileID,formatSpec); 
            
            if local_dir
                formatSpec = '<var type="local strain"/>\n';
                fprintf(fileID,formatSpec); 
                formatSpec = '<var type="e1"/>\n';
                fprintf(fileID,formatSpec); 
                formatSpec = '<var type="e2"/>\n';
                fprintf(fileID,formatSpec); 
            end
            
        formatSpec = '\t\t</plotfile>\n';
        fprintf(fileID,formatSpec);
        
        formatSpec = '\t\t<logfile>\n';
        fprintf(fileID,formatSpec);  
           formatSpec = '\t\t\t<element_data data="Ex;Ey;Ez;Exy;Eyz;Exz" name="GL strain" file="%s" delim=","></element_data>\n';
           fprintf(fileID,formatSpec,file_str); 
           formatSpec = '\t\t\t<element_data data="E1;E2;E3;J;sed;devsed" name="strain sts" file="%s" delim=","></element_data>\n';
           fprintf(fileID,formatSpec,file_pstr); 
           formatSpec = '\t\t\t<element_data data="Fxx;Fyy;Fzz;Fxy;Fyz;Fxz;Fyx;Fzy;Fzx" name="def. gradient" file="%s" delim=","></element_data>\n';
           fprintf(fileID,formatSpec,file_Dgrad); 
           if local_dir
               formatSpec = '<element_data data="E11;E22;E33;E12;E23;E13" name="local strain" file="%s" delim=","></element_data>\n';
               fprintf(fileID,formatSpec,file_loc_str); 
           end
        formatSpec = '\t\t</logfile>\n';
        fprintf(fileID,formatSpec);
        
    formatSpec = '\t</Output>\n';
    fprintf(fileID,formatSpec);  
formatSpec = '</febio_spec>\n';
fprintf(fileID,formatSpec); 

fclose(fileID);

%% get kinematics

% concatenate files (UNIX)
% ex_file = [GEO_source_file(1:end-4) '_post_v'  num2str(run_n) '.feb'];
ex_file = ['HFE_post.feb'];

system(['cat ' file_a ' ' ...
               file_b ' ' ...
               file_c ' ' ...
               file_d ' ' ...
               file_e ' ' ...
               file_f ' ' ...
               file_g ' > ' ex_file]);
           
% call solver (UNIX)
% cd(GEO_folder)
% system(['./febio2.lnx64 -i ' ex_file])
system(['unset LD_LIBRARY_PATH; ' './febio2.lnx64 -i ' ex_file ])
% cd ..

%% move output files
% TODO: do this only if the system call succeeds

Df = dir('*HFE*');
for cnt_f = 1:size(Df,1)
    system(['mv ' Df(cnt_f).name ' ' HFE_run_folder]);
end

disp('Done :)')
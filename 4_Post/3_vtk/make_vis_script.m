clc
close all
clear all

% latest: sketch for future python compatibility

%% analysis folder path

enable_folders

%% load slice groups

sv_folder = analysis_folder;
sv_file = 'SliceGroup_FIXED'; % add to _SYNTH if needed
load([sv_folder '/' sv_file])

%% axial

cnt_g = 1;

slices = SliceGroup(cnt_g).rot;


sprintf(['(' repmat('%i,',[1,numel(slices)-1]) '%i)'],slices)


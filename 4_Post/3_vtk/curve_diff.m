function [Qds,Qs] = curve_diff_v1(Q,Qs)
% [Qds,Qs] = curve_diff_v1(Q,Qs) curvature derivative approx.
% 
% Q = point list [xi yi zi]
% Qs = parameter (optional input)
% 
% Qds = 3-pt derivative

% version 1: basic funciton 
% ADG 2016-12-08

%% parse input

% parameter
if nargin==1
    Qd = diff(Q,1,1); 
    Qn = sum(Qd.^2,2).^0.5;
    Qs = [0; cumsum(Qn)] / sum(Qn);
end

%% 3-pt numerical derivative

% initialize
Qds = Q*0; 

% forward
Qds(1:end-1,:) = udiff_v1(Q,Qs);

% backward and average
Qds(2:end,:) = 0.5*(Qds(2:end,:) + flip(udiff_v1(flip(Q,1),flip(Qs,1)),1) );

end

function Qd = udiff_v1(Q,Qs)

Qd = diff(Q,1,1)./repmat(diff(Qs),[1 3]);

end
function vtkwrite_v4( filename,dataType,varargin )
% function vtkwrite_v4( filename,dataType,varargin )
%  help TBA
%  
%  version 4: improved cell export. ADG
%  
%  relevant people: Chaoyuan Yeh,  William Thielicke, David Gingras  
% 

if strcmpi(filename,'execute'), filename = 'matlab_export.vtk'; end
fid = fopen(filename, 'w'); 
% VTK files contain five major parts
% 1. VTK DataFile Version
fprintf(fid, '# vtk DataFile Version 2.0\n');
% 2. Title
fprintf(fid, 'VTK from Matlab\n');
switch upper(dataType)
    case {'STRUCTURED_GRID','UNSTRUCTURED_GRID'}
        % 3. The format data proper is saved in (ASCII or Binary). Use
        % fprintf to write data in the case of ASCII and fwrite for binary.
        % if numel(varargin)<6, error('Not enough input arguments'); end
        fprintf(fid, 'ASCII\n');
        x = varargin{1};
        y = varargin{2};
        z = varargin{3};
        if sum(size(x)==size(y) & size(y)==size(z))~=length(size(x))
            error('Input dimesions do not match')
        end
        n_points = numel(x);
        % 4. Type of Dataset ( can be STRUCTURED_POINTS, STRUCTURED_GRID,
        % UNSTRUCTURED_GRID, POLYDATA, RECTILINEAR_GRID or FIELD )
        % This part, dataset structure, begins with a line containing the
        % keyword 'DATASET' followed by a keyword describing the type of dataset.
        % Then the geomettry part describes geometry and topology of the dataset.
        if strcmpi(dataType,'STRUCTURED_GRID')
            fprintf(fid, 'DATASET STRUCTURED_GRID\n');
            fprintf(fid, ['DIMENSIONS ' num2str(size(x,2)) ' ' num2str(size(x,1)) ' ' num2str(size(x,3)) '\n']);
        else
            fprintf(fid, 'DATASET UNSTRUCTURED_GRID\n');
        end
        fprintf(fid, ['POINTS ' num2str(n_points) ' float\n']);
          
        % ADG - this just writes nodal coordinates
        precision = '6';
        main_format = [repmat(['%0.',precision,'f '],1,3),'\n'];
        fprintf(fid,main_format,[x(:) y(:) z(:)]');
        
%         end_d = mod(n_points,3);
%         main_format = [repmat(['%0.',precision,'f '],1,9),'\n'];
%         fprintf(fid,main_format, [x(1:3:end-end_d) y(1:3:end-end_d) z(1:3:end-end_d) ...
%                                   x(2:3:end-end_d) y(2:3:end-end_d) z(2:3:end-end_d) ...
%                                   x(3:3:end-end_d) y(3:3:end-end_d) z(3:3:end-end_d)]');            
%         end_format = [repmat(['%0.',precision,'f '],1,end_d*3),'\n'];
%         xo = x(end-end_d+1:end); yo = y(end-end_d+1:end); zo = z(end-end_d+1:end);
%         if (end_d == 1)
%             fprintf(fid,end_format, [xo(1) yo(1) zo(1)]'); 
%         elseif (end_d == 2)
%             fprintf(fid,end_format, [xo(1) yo(1) zo(1) ...
%                                      xo(2) yo(2) zo(2)]');
%         end
%         fprintf(fid, '\n');
       
        % ADG - adding cell definition (before data)
        cidx = find(strcmpi(varargin,'cells'), 1);
        if ~isempty(cidx)
            
            cells = varargin{cidx+1};
            n_cells = numel(cells(:,1));
            
            % ADG - cells(:,1) = cell type
            %       cells(i,:) =  connectivity using zeros to define size
            fprintf(fid, ['CELLS ' num2str(n_cells) ' ' num2str(numel(find(cells))) '\n']);
            
            for cnt_i = 1:n_cells
                n_cell_el = sum(cells(cnt_i,2:end)>0,2);
                cell_el = cells(cnt_i,cells(cnt_i,:)>0);
                main_format = [repmat('%i ',1,n_cell_el+1)  '\n'];
                fprintf(fid,main_format,[n_cell_el cell_el(2:end)-1]');
            end
            % fprintf(fid, '\n');
            
            fprintf(fid, ['CELL_TYPES ' num2str(n_cells) '\n']);
            fprintf(fid, '%i\n', cells(:,1));
            % fprintf(fid, '\n');
        end
        
        % ADG - adding data definition (for point data)
        pvidx = find(strcmpi(varargin,'PVECTORS'));
        psidx = find(strcmpi(varargin,'PSCALARS'));
        
        if ((~isempty(pvidx))||(~isempty(psidx)))
            fprintf(fid, ['POINT_DATA ' num2str(n_points)]);
            
            % scalars
            if ~isempty(psidx)
                for ii = 1:length(psidx)
                    title = varargin{psidx(ii)+1};
                    fprintf(fid, ['\nSCALARS ', title,' float\n']);
                    fprintf(fid, 'LOOKUP_TABLE default\n');
                    
                    f = varargin{psidx(ii)+2};
                    
                    precision = '6';

                    main_format = [repmat(['%0.',precision,'f '],1,1),'\n'];
                    fprintf(fid,main_format,[f(:)]');
        
%                     end_d = mod(n_points,3);
% 
%                     main_format = [repmat(['%0.',precision,'f '],1,9),'\n'];
%                     fprintf(fid,main_format, [f(1:3:end-end_d) ...
%                                               f(2:3:end-end_d) ...
%                                               f(3:3:end-end_d) ]');
% 
%                     end_format = [repmat(['%0.',precision,'f '],1,end_d*3),'\n'];
%                     fo = f(end-end_d+1:end); 
%                     if (end_d == 1)
%                         fprintf(fid,end_format, [fo(1)]'); 
%                     elseif (end_d == 2)
%                         fprintf(fid,end_format, [fo(1) ...
%                                                  fo(2)]');
%                     end
%                     fprintf(fid, '\n');

                    % fwrite(fid, reshape(varargin{sidx(ii)+2},1,n_points),'float','b');
                end
            end
            
            % vectors
            if ~isempty(pvidx)
                for ii = 1:length(pvidx)
                    title = varargin{pvidx(ii)+1};
                    fprintf(fid, ['\nVECTORS ', title,' float\n']);
                    
                    u = varargin{pvidx(ii)+2}(:,1);
                    v = varargin{pvidx(ii)+2}(:,2);
                    w = varargin{pvidx(ii)+2}(:,3);
                    
                    precision = '6';
                    
                    main_format = [repmat(['%0.',precision,'f '],1,3),'\n'];
                    fprintf(fid,main_format,[u(:) v(:) w(:)]');
%                     
%                     end_d = mod(n_points,3);
% 
%                     main_format = [repmat(['%0.',precision,'f '],1,9),'\n'];
%                     fprintf(fid,main_format, [u(1:3:end-end_d) v(1:3:end-end_d) w(1:3:end-end_d) ...
%                                               u(2:3:end-end_d) v(2:3:end-end_d) w(2:3:end-end_d) ...
%                                               u(3:3:end-end_d) v(3:3:end-end_d) w(3:3:end-end_d)]');
% 
%                     end_format = [repmat(['%0.',precision,'f '],1,end_d*3),'\n'];
%                     uo = u(end-end_d+1:end); vo = v(end-end_d+1:end); wo = w(end-end_d+1:end);
%                     if (end_d == 1)
%                         fprintf(fid,end_format, [uo(1) vo(1) wo(1)]'); 
%                     elseif (end_d == 2)
%                         fprintf(fid,end_format, [uo(1) vo(1) wo(1) ...
%                                                  uo(2) vo(2) wo(2)]');
%                     end
%                     fprintf(fid, '\n');
                end
            end
        end
        
        % ADG - adding data definition (for cell data)
        cvidx = find(strcmpi(varargin,'CVECTORS'));
        csidx = find(strcmpi(varargin,'CSCALARS'));
        ctidx = find(strcmpi(varargin,'CTENSORS'));
        
        if ((~isempty(cvidx))||(~isempty(csidx))||(~isempty(ctidx)))
            fprintf(fid, ['CELL_DATA ' num2str(n_cells)]);
            
            % scalars
            if ~isempty(csidx)
                for ii = 1:length(csidx)
                    title = varargin{csidx(ii)+1};
                    fprintf(fid, ['\nSCALARS ', title,' float\n']);
                    fprintf(fid, 'LOOKUP_TABLE default\n');
                    
                    f = varargin{csidx(ii)+2};
                    
                    precision = '6';

                    main_format = [repmat(['%0.',precision,'f '],1,1),'\n'];
                    fprintf(fid,main_format,[f(:)]');
                    
                    
%                     precision = '6';
%                     end_d = mod(n_cells,3);
% 
%                     main_format = [repmat(['%0.',precision,'f '],1,9),'\n'];
%                     fprintf(fid,main_format, [f(1:3:end-end_d) ...
%                                               f(2:3:end-end_d) ...
%                                               f(3:3:end-end_d) ]');
% 
%                     end_format = [repmat(['%0.',precision,'f '],1,end_d*3),'\n'];
%                     fo = f(end-end_d+1:end); 
%                     if (end_d == 1)
%                         fprintf(fid,end_format, [fo(1)]'); 
%                     elseif (end_d == 2)
%                         fprintf(fid,end_format, [fo(1) ...
%                                                  fo(2)]');
%                     end
                    % fprintf(fid, '\n');

                    % fwrite(fid, reshape(varargin{sidx(ii)+2},1,n_points),'float','b');
                end
            end
            
            % vectors
            if ~isempty(cvidx)
                for ii = 1:length(cvidx)
                    title = varargin{cvidx(ii)+1};
                    fprintf(fid, ['\nVECTORS ', title,' float\n']);
                    
                    u = varargin{cvidx(ii)+2}(:,1);
                    v = varargin{cvidx(ii)+2}(:,2);
                    w = varargin{cvidx(ii)+2}(:,3);
                    
                    precision = '6';
                    
                    main_format = [repmat(['%0.',precision,'f '],1,3),'\n'];
                    fprintf(fid,main_format,[u(:) v(:) w(:)]');
                    
%                     precision = '6';
%                     end_d = mod(n_cells,3);
% 
%                     main_format = [repmat(['%0.',precision,'f '],1,9),'\n'];
%                     fprintf(fid,main_format, [u(1:3:end-end_d) v(1:3:end-end_d) w(1:3:end-end_d) ...
%                                               u(2:3:end-end_d) v(2:3:end-end_d) w(2:3:end-end_d) ...
%                                               u(3:3:end-end_d) v(3:3:end-end_d) w(3:3:end-end_d)]');
% 
%                     end_format = [repmat(['%0.',precision,'f '],1,end_d*3),'\n'];
%                     uo = u(end-end_d+1:end); vo = v(end-end_d+1:end); wo = w(end-end_d+1:end);
%                     if (end_d == 1)
%                         fprintf(fid,end_format, [uo(1) vo(1) wo(1)]'); 
%                     elseif (end_d == 2)
%                         fprintf(fid,end_format, [uo(1) vo(1) wo(1) ...
%                                                  uo(2) vo(2) wo(2)]');
%                     end
%                     fprintf(fid, '\n');
                end
            end
            
            % tensors
            if ~isempty(ctidx)
                for ii = 1:length(ctidx)
                    title = varargin{ctidx(ii)+1};
                    fprintf(fid, ['\nTENSORS ', title,' float\n']);
                    
                    t00 = varargin{ctidx(ii)+2}(:,1);
                    t01 = varargin{ctidx(ii)+2}(:,2);
                    t02 = varargin{ctidx(ii)+2}(:,3);
                    t10 = varargin{ctidx(ii)+2}(:,1);
                    t11 = varargin{ctidx(ii)+2}(:,2);
                    t12 = varargin{ctidx(ii)+2}(:,3);
                    t20 = varargin{ctidx(ii)+2}(:,1);
                    t21 = varargin{ctidx(ii)+2}(:,2);
                    t22 = varargin{ctidx(ii)+2}(:,3);
                    
                    precision = '6';
                    
                    main_format = [repmat(['%0.',precision,'f '],1,3),'\n'];
                    fprintf(fid,main_format,[t00(:) t01(:) t02(:) ...
                                             t10(:) t11(:) t12(:) ...
                                             t20(:) t21(:) t22(:) ]');
                    
                end
            end
        end
        
        
        % fwrite(fid, [x(:)';y(:)';z(:)'],'float','b')
        
    % 5.This final part describe the dataset attributes and begins with the
    % keywords 'POINT_DATA' or 'CELL_DATA', followed by an integer number
    % specifying the number of points of cells. Other keyword/data combination
    % then define the actual dataset attribute values.
    % fprintf(fid, ['POINT_DATA ' num2str(n_points)]);
    % Parse remaining argument.     
    case 'POLYDATA'
        fprintf(fid, 'ASCII\n');
        if numel(varargin)<4, error('Not enough input arguments'); end
        x = varargin{2}(:);
        y = varargin{3}(:);
        z = varargin{4}(:);
        if numel(varargin)<4, error('Not enough input arguments'); end
        if sum(size(x)==size(y) & size(y)==size(z))~= length(size(x))
            error('Input dimesions do not match')
        end
        n_points = numel(x);
        fprintf(fid, 'DATASET POLYDATA\n');
        if mod(n_points,3)==1
            x(n_points+1:n_points+2,1)=[0;0];
            y(n_points+1:n_points+2,1)=[0;0];
            z(n_points+1:n_points+2,1)=[0;0];
        elseif mod(n_points,3)==2
            x(n_points+1,1)=0;
            y(n_points+1,1)=0;
            z(n_points+1,1)=0;
        end
        nbpoint = numel(x);
        fprintf(fid, ['POINTS ' num2str(nbpoint) ' float\n']);
        precision = '3';
        if any(strcmpi(varargin,'PRECISION'))
            precision = num2str(uint8(varargin{find(strcmpi(varargin,'PRECISION'))+1}));
            if str2double(precision) < 0, error('Invalid precision spec.');end
        end
        spec = [repmat(['%0.',precision,'f '],1,9),'\n'];
        fprintf(fid,spec, [x(1:3:end-2) y(1:3:end-2) z(1:3:end-2) ...
             x(2:3:end-1) y(2:3:end-1) z(2:3:end-1) ...
             x(3:3:end) y(3:3:end) z(3:3:end)]');
        switch upper(varargin{1})
            case 'LINES'
                if mod(n_points,2)==0
                    nbLine = 2*n_points-2;
                else
                    nbLine = 2*(n_points-1);
                end
                conn1 = zeros(nbLine,1);
                conn2 = zeros(nbLine,1);
                conn2(1:nbLine/2) = 1:nbLine/2;
                conn1(1:nbLine/2) = conn2(1:nbLine/2)-1;
                conn1(nbLine/2+1:end) = 1:nbLine/2;
                conn2(nbLine/2+1:end) = conn1(nbLine/2+1:end)-1;
                fprintf(fid,'\nLINES %d %d\n',nbLine,3*nbLine);
                fprintf(fid,'2 %d %d\n',[conn1';conn2']);
            case 'TRIANGLE'
                ntri = length(varargin{5});
                fprintf(fid,'\nPOLYGONS %d %d\n',ntri,4*ntri);
                fprintf(fid,'3 %d %d %d\n',(varargin{5}-1)');
            case 'TETRAHEDRON'
                ntetra = length(varargin{5});
                fprintf(fid,'\nPOLYGONS %d %d\n',ntetra,5*ntetra);
                fprintf(fid,'4 %d %d %d %d\n',(varargin{5}-1)');
        end     
end
fclose(fid);
end
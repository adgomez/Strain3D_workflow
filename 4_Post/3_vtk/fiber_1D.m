function  [X,kappa,T,B,N,tau] = fiber_1D_v6(W,n,opt,flag)
% [X,kappa,T,B,N,tau] = fiber_1D_v6(W,n,opt,flag) calculates 3D curve features 
%
%     INPUT
%     W = points along fiber [x y z]
%     n = points to be interpolated in between       
%     opt = (optional) spline type: 'smoothing' or 'interpolating'
%     flag = (optional) plot generation flag: 
%        0 = nothing
%        1 = struct
%        2 = osculating circle 
% 
%     OUTPUT
%     X = points on the spline 
%     kappa = curvature
%     T = tangent
%     N = normal
%     B = binormal
%     tau = torsion 

% version 5: improved curvature calculation
% ADG 2016-12-08
% TODO: verify against known strain energy functions

%% manage arguments

% inputs
if (nargin == 2)
    flag = 0;
    opt = 'default';
end

if flag
    figure
end

% location 
b_size = (max(W,[],1) - min(W,[],1));
b_size = 1.2*max(b_size(:));
b_center =  mean(mean(W,1),3);
b_center = [b_center; b_center];
b_box = 0.5*[-b_size b_size -b_size b_size -b_size b_size] + b_center(:)';


%% time loop
for cnt_t = 1:numel(W(1,1,:))
    
    %% spline interpolation
    
    switch opt
        case 'interpolating'  
            % spline fit
            P(:,1) = [W(1,1,cnt_t); W(:,1,cnt_t); W(end,1,cnt_t)];	
            P(:,2) = [W(1,2,cnt_t); W(:,2,cnt_t); W(end,2,cnt_t)];	
            P(:,3) = [W(1,3,cnt_t); W(:,3,cnt_t); W(end,3,cnt_t)];
            if (n<0)
                n = 0;
            end
            Qi = SPL_CH_v1(P,n);

        case 'smoothing'
            % B-spline smoothing
            P = W(:,:,cnt_t);
            Qi = SPL_B_v2(P,n);
        
        otherwise
            warning('Using default interpolation')
            P(:,1) = [W(1,1,cnt_t); W(:,1,cnt_t); W(end,1,cnt_t)];	
            P(:,2) = [W(1,2,cnt_t); W(:,2,cnt_t); W(end,2,cnt_t)];	
            P(:,3) = [W(1,3,cnt_t); W(:,3,cnt_t); W(end,3,cnt_t)];	
            if (n<0)
                n = 0;
            end
            Qi = SPL_CH_v1(P,n);
    end
    
    %% curvilinear analysis

     % *** aparatus components
    % tangent
    [Qd,Qs] = curve_diff_v1(Qi);
    Ti = Qd./repmat(sum(Qd.^2,2).^0.5,[1 3]);
    % normal
    Qd = curve_diff_v1(Ti,Qs);
    Ni = Qd./repmat(sum(Qd.^2,2).^0.5,[1 3]);
    % binormal
    Bi = cross(Ti,Ni,2);

    % *** curvature and torsion
    % get curvature
    [ki,ti] = FS_v1(Qi,Ti,Ni,Bi);
    
%% store
%  in all [x y z]

    X(:,:,cnt_t) = Qi;       % points on the spline 
    T(:,:,cnt_t) = Ti;       % tangent (unit vector)
    N(:,:,cnt_t) = Ni;       % normal (unit vector)
    B(:,:,cnt_t) = Bi;       % binormal (unit vector)
    kappa(:,1,cnt_t) = ki;    % curvature (scalar)
    tau(:,1,cnt_t) = ti;      % torsion (scalar)

%% plot

    if (flag == 1)
            % scale
            sc = 0.1*b_size;

            % plot
            cla
            hold on
            % plot3(x,y,z,'sk')
            plot3(P(:,1),P(:,2),P(:,3),'sk')
            plot3(Qi(:,1),Qi(:,2),Qi(:,3),'-k')

            quiver3(Qi(:,1),Qi(:,2),Qi(:,3), ...
                    Ti(:,1)*sc, ...
                    Ti(:,2)*sc, ...
                    Ti(:,3)*sc, ...
                    0,'r')    

            quiver3(Qi(:,1),Qi(:,2),Qi(:,3), ...
                    Ni(:,1)*sc, ...
                    Ni(:,2)*sc, ...
                    Ni(:,3)*sc, ...
                    0,'g')

            quiver3(Qi(:,1),Qi(:,2),Qi(:,3), ...
                    Bi(:,1)*sc, ...
                    Bi(:,2)*sc, ...
                    Bi(:,3)*sc, ...
                    0,'b')

            hold off
            grid on
            axis equal
            axis(b_box)
            view(77,16)
            % view(0,90)
            legend('data','fit','T','N','B')
            title(num2str(cnt_t))
            pause(0.01)
                  
    elseif (flag == 2)
        cla
        for cnt_p = 1:numel(Qi(:,1))

            % osculating circle
            R_k = 1./ki;
            C_Q = [Ti(cnt_p,:)
                   Ni(cnt_p,:)
                   Bi(cnt_p,:)];   
            C_R = get_R_v1(C_Q,eye(3))';   
            C_p = linspace(0,2*pi,50);  
            C_o(1,:) = R_k(cnt_p).*cos(C_p);
            C_o(2,:) = R_k(cnt_p).*sin(C_p) + R_k(cnt_p);
            C_o(3,:) = C_p*0;
            C_c = [Qi(cnt_p,1),Qi(cnt_p,2),Qi(cnt_p,3)] + 0*R_k(cnt_p)*Ni(cnt_p,:); 
            C_o = C_R*C_o + repmat(C_c',[1 numel(C_p)]) ;

            % scale
            sc = 0.1*b_size;

            % plot
            % cla
            hold on
            % plot3(x,y,z,'sk')
            plot3(P(:,1),P(:,2),P(:,3),'sk')
            plot3(Qi(:,1),Qi(:,2),Qi(:,3),'-k')

            quiver3(Qi(cnt_p,1),Qi(cnt_p,2),Qi(cnt_p,3), ...
                    Ti(cnt_p,1)*sc, ...
                    Ti(cnt_p,2)*sc, ...
                    Ti(cnt_p,3)*sc, ...
                    0,'r')    

            quiver3(Qi(cnt_p,1),Qi(cnt_p,2),Qi(cnt_p,3), ...
                    Ni(cnt_p,1)*sc, ...
                    Ni(cnt_p,2)*sc, ...
                    Ni(cnt_p,3)*sc, ...
                    0,'g')

            quiver3(Qi(cnt_p,1),Qi(cnt_p,2),Qi(cnt_p,3), ...
                    Bi(cnt_p,1)*sc, ...
                    Bi(cnt_p,2)*sc, ...
                    Bi(cnt_p,3)*sc, ...
                    0,'b')

            plot3(C_o(1,:),C_o(2,:),C_o(3,:),'--k')    

            hold off
            grid on
            axis equal
            axis(b_box)
            view(77,16)
            % view(0,90)
            legend('data','fit','T','N','B','osc. circ.')
            title(num2str(cnt_t))
        end  
        pause(0.001)
    end
end

end

%% Interpolating Spline
function Q = SPL_CH_v1(P,n)
% Cubic Hermite Spline
% v1 basic
% ADG

    % initialize loop
    dim = size(P);

    % number of sections (sections in between minus 2 end points)
    Ni = dim(1) - 3;

    % oputput vector
    Q = zeros((n+1)*Ni + 1,dim(2));

    % section counter (stops at n-1)
    for cnt_s = 1:Ni

        % point counter
        ki = cnt_s + 1;

        % moving parameter step
        dt = 1/(n+1);

        % moving parameter
        t = (0:dt:dt*n)';

        % moving parameter vector
        Ti = [t.^3 t.^2 t t*0+1]';

        % tension
        c = 0.0;

        % end points
        p0 = P(ki,:)';
        p1 = P(ki + 1,:)';

        % end points tangents
        m0 = (1 - c)*(P(ki + 1,:)' - P(ki - 1,:)')/2;
        m1 = (1 - c)*(P(ki + 2,:)' - P(ki,:)')/2;

        % hermite polynomial
        H  = [2 -3 0 1
              1 -2 1 0
             -2  3 0 0
              1 -1 0 0];

        % interpolated locations   
        Q((n+1)*(cnt_s-1) + 1:(n+1)*cnt_s,:) = ([p0 m0 p1 m1]*H*Ti)';

    end

    % last point
    Q(end,:) = P(end,:);   

end

%% Smoothing Spline
function Q = SPL_B_v2(P,t_n)
% Basis Spline with option for same smoothing same number of points
% v2 basic
% ADG

% note: legacy requires t_n to be points in between

    if (t_n<0)
        % return identical points
        Q = P;
    else
        % optimal node placement flag
        opt_flag = false;

        % knot vector
        n = numel(  P(:,1)); 
        k = 3; 
        T = linspace(0,1,n-k+2);

        % unscaled parameter (actual spacing)
        S = cumsum([0; sum(diff(P).^2,2).^0.5]);

        % unscaled position of control points
        t_s = S/max(S); 

        % spline evaluated at data points
        [qs,W] = DEBOOR_v2(T,P,t_s,k);

        % optimal control points (optional, but unstable)
        if opt_flag
            Qp = inv(W'*W)*(W')*P;
            [qs,W] = DEBOOR_v2(T,Qp,t_s,k);
        else
            Qp = P;
        end

        % parameter
        t = linspace(0,1,n + (n - 1)*t_n)';
        % get spline 
        Q = DEBOOR_v2(T,Qp,t,k);
    end

end

%% Aux B-Spline Function
function [val,W] = DEBOOR_v2(T,p,y,order)
% function val = DEBOOR(T,p,y,order)
%
% INPUT:  T     St�tzstellen
%         p     Kontrollpunkte (nx2-Matrix)
%         y     Auswertungspunkte (Spaltenvektor)
%         order Spline-Ordnung
%
% OUTPUT: val   Werte des B-Splines an y (mx2-Matrix)
%
% Date:   2007-11-27
% Author: Jonas Ballani

% some editing by ADG: adding last point, 3D formulation
% 10-26-2016

% original variables
m = size(p,1);
n = length(y);
X = zeros(order,order);
Y = zeros(order,order);
Z = zeros(order,order);
a = T(1);
b = T(end);
T = [ones(1,order-1)*a,T,ones(1,order-1)*b];

% weights 
W = zeros(m);
W(end) = 1;

% initialize
val = zeros(n,3);
val(end,:) = p(end,:); 

for cnt_l = 1:n
    t0 = y(cnt_l);
    id = find(t0 >= T);
    k = id(end);
		if (k > m)
			return;
		end
    X(:,1) = p(k-order+1:k,1);
    Y(:,1) = p(k-order+1:k,2);
    Z(:,1) = p(k-order+1:k,3);
    
    for i = 2:order
        for j = i:order
            num = t0-T(k-order+j);
            if num == 0
                weight = 0;
            else
				s = T(k+j-i+1)-T(k-order+j);
                weight = num/s;
            end
            X(j,i) = (1-weight)*X(j-1,i-1) + weight*X(j,i-1);
            Y(j,i) = (1-weight)*Y(j-1,i-1) + weight*Y(j,i-1);
            Z(j,i) = (1-weight)*Z(j-1,i-1) + weight*Z(j,i-1);
            N(j,i) = weight; 
        end
    end
    
    % this works for 3rd order only (it can be automated if needed)
    W(cnt_l,k-order+1:k) = [(1-N(3,3))*(1-N(2,2)) N(2,2)*(1-N(3,3))+N(3,3)*(1-N(3,2)) N(3,3)*N(3,2)]; %

    val(cnt_l,1) = X(order,order);
    val(cnt_l,2) = Y(order,order);
    val(cnt_l,3) = Z(order,order);
end


end

%% FE system
function [kappa,tau] = FS_v1(P,T,N,B)


% reparametrize in terms of arc length
sd = diff(P,1,1); 
sn = sum(sd.^2,2).^0.5;
s = [0; cumsum(sn)] ;

% invoke FS formulas
dT = curve_diff_v1(T,s);
dN = curve_diff_v1(N,s);
dB = curve_diff_v1(B,s);

% loop to get properties
kappa = s*0;
tau = s*0;
for cnt_p = 1:numel(s)

    dQ = [dT(cnt_p,:)
          dN(cnt_p,:)
          dB(cnt_p,:)];

    Q = [T(cnt_p,:)
         N(cnt_p,:)
         B(cnt_p,:)];  

    FS = (dQ*Q');

    kappa(cnt_p) = mean(abs([FS(1,2) FS(2,1)]));
    tau(cnt_p) = mean(abs([FS(3,2) FS(2,3)]));

end

end

function [Qds,Qs] = curve_diff_v1(Q,Qs)
% [Qds,Qs] = curve_diff_v1(Q,Qs) curvature derivative approx.
% 
% Q = point list [xi yi zi]
% Qs = parameter (optional input)
% 
% Qds = 3-pt derivative

% version 1: basic funciton 
% ADG 2016-12-08

%% parse input

% parameter
if nargin==1
    Qd = diff(Q,1,1); 
    Qn = sum(Qd.^2,2).^0.5;
    Qs = [0; cumsum(Qn)] / sum(Qn);
end

%% 3-pt numerical derivative

% initialize
Qds = Q*0; 

% forward
Qds(1:end-1,:) = udiff_v1(Q,Qs);

% backward and average
Qds(2:end,:) = 0.5*(Qds(2:end,:) + flip(udiff_v1(flip(Q,1),flip(Qs,1)),1) );

end

function Qd = udiff_v1(Q,Qs)

Qd = diff(Q,1,1)./repmat(diff(Qs),[1 3]);

end

function R = get_R_v1(D,D1)
% R = get_R_v1(D,D1) 
% 
% SVD solution of wahba's problem to alignt D1 to D via rotation matrix R.
% D1 = matrix with orthogonal column vectors (moving)
% D = matrix with orthogonal column vectors (target)
% R*D1 = D

B = zeros(3);
for ii = 1:3;

B = B + D(:,ii)*D1(:,ii)';

end

[U,~,V] = svd(B);

M = diag([1 1 det(U)*det(V)]);

R = U*M*(V');

end
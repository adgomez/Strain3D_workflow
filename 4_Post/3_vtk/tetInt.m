function [T,T_m] = tetInt(nodes,elements,property,X,impF) 
% multidimensional tet interpolator
% [T,T_m] = tetInt_v3(nodes,elements,property,X,inpF) 
% 
% nodes, elements = linear tet mesh
% noces = [i xi yi zi]
% elements = [i mi a1 a2 a3 a4]
% property =  nodal property (add dimensions as columns)
% X = [xi yi zi] point list
% impF = replaces nans with extrapolation (optional)
% 
% T = property at xi yi zi
% T_m = mask

% version 3: adding imp flag

%% parse inputs

if nargin == 4
    impF = false; 
end


%% interpolate

% vertex location
vertex = nodes(:,2:4);
vertex_T = property;
triangles = elements(:,3:6); 

% generate interpolator
T_p = triangulation(triangles,vertex);
[T_i,T_bc] = pointLocation(T_p,X);
T_m = 0<T_i; 
Inan = isnan(T_i);
T_i(Inan) = [];
T_bc(Inan,:) = [];
T_n = triangles(T_i,:);

% initialize
T = zeros(numel(X(:,1)),numel(property(1,:)));

% convect property
for cnt_d = 1:numel(property(1,:))
    T(~Inan,cnt_d) = vertex_T(T_n(:,1),cnt_d).*T_bc(:,1) + ...
                     vertex_T(T_n(:,2),cnt_d).*T_bc(:,2) + ...
                     vertex_T(T_n(:,3),cnt_d).*T_bc(:,3) + ...
                     vertex_T(T_n(:,4),cnt_d).*T_bc(:,4);
end

%% inpaint nan

if (~isempty(find(Inan, 1)))&&impF
    
    % get centroid and radius
    S = [vertex; X];
    S_c = mean(S);
    S_r = max(sum((S - S_c).^2,2).^0.5)*1.5;
    warning(['found points outside mesh: Remeshing with zero boundary ball, radius = ' num2str(S_r) ])
      
    % generate ball
    S_N = 10;
    S_th = linspace(-pi,pi,S_N);
    S_ps = linspace(0,pi,S_N);

    [S_TH,S_PS] = ndgrid(S_ps,S_th);
    
    S_b(:,1) = S_r*sin(S_TH(:)).*cos(S_PS(:)) + S_c(1);
    S_b(:,2) = S_r*sin(S_TH(:)).*sin(S_PS(:)) + S_c(2);
    S_b(:,3) = S_r*cos(S_TH(:)) + S_c(3);

    % apend all nodes and remesh
    S = [vertex; S_b];
    S_T = [property; property(S_b(:,1)*0+1,:)*0];
    TRI = delaunayTriangulation(S(:,1),S(:,2),S(:,3));
    
    % generate interpolator
    T_p = triangulation(TRI.ConnectivityList,TRI.Points);
    [Tn_i,Tn_bc] = pointLocation(T_p,X(Inan,:));
    T_n = TRI.ConnectivityList(Tn_i,:);

    % convect property
    for cnt_d = 1:numel(property(1,:))
        T(Inan,cnt_d) = S_T(T_n(:,1),cnt_d).*Tn_bc(:,1) + ...
                        S_T(T_n(:,2),cnt_d).*Tn_bc(:,2) + ...
                        S_T(T_n(:,3),cnt_d).*Tn_bc(:,3) + ...
                        S_T(T_n(:,4),cnt_d).*Tn_bc(:,4);
    end
%         figure
%         hold on
%         scatter3(vertex(:,1),vertex(:,2),vertex(:,3),'.b')
%         scatter3(X(:,1),X(:,2),X(:,3),'.r')
%         scatter3(S_b(:,1),S_b(:,2),S_b(:,3),'.k')
%         hold off
%         
%         axis equal
%         grid on 
  
    
end


end

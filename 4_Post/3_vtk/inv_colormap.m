function L = inv_colormap_v1(data)
% color mapping

% find spherical coordinates for reflection
[Ta,Te,R] = cart2sph(data(:,1),data(:,2),data(:,3));

% reflect about x
Ip = find(Ta<0);
Ta(Ip) = -Ta(Ip);

% reflect 3rd to 1st
Ip = find(((pi/2<Ta)&(Ta<=pi)));
Ta(Ip) = pi - Ta(Ip);

% reflect elevation
Ip = find(Te<0);
Te(Ip) = -Te(Ip);

% reconstruct
[data(:,1),data(:,2),data(:,3)] = sph2cart(Ta,Te,R);

% lookup table
rgb = circshift(jet,8,1);
% rgb = rgb(:,[1 2 3]);
lin = linspace(0,1,numel(rgb(:,1)));
L = data(:,1)*0;

h = waitbar(0,'color mapping ...');
for cnt_i = 1:numel(data(:,1))
    
    v = data(cnt_i,:);
    if ~(norm(v) == 0)
        [~,Iy] = min(sum( (rgb - repmat(v,[numel(rgb(:,1)) 1])).^2 ,2));
        % rgb(Iy,:)
        L(cnt_i) = lin(Iy);
    end
    
    waitbar(cnt_i/numel(data(:,1)),h)
end
close(h)

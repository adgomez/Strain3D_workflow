
clc
close all
clear all

% latest: adding HFE run option

% right now we, need: mesh, deformed mesh, mesh data (vector dir, strain)
% tagged images

% TODO: project to Q coordinaes
% TODO: generate mesh with T1 and tag_1 images

%% analysis folder path

enable_folders

%% open displacement data

% load mesh
FE_folder = analysis_folder;
FE_file = 'mesh_tag_1';

load([FE_folder '/' FE_file])
ref_nodes = nodes;
ref_elements = elements;
    
% sort mesh components
n_elem = 0;
for cnt_et = 1:size(ref_elements,2)
    n_elem = n_elem + size(ref_elements{cnt_et},1);
end

%% load nodal displacmements

disp_type = 'HARP-FE';

switch disp_type
    case 'HARP-FE'
        % HARP-FE run to be fetched
        run_n = 2; 
        % load nodal displacmements
        HFE_run_folder = [analysis_folder '/'  'HFE_run' num2str(run_n)];
        disp_file = 'nodal_disp';
        load([HFE_run_folder '/' disp_file])
        n_steps = size(nod_u,3);
 
        % strain data
        % ordinary strains
        Elist = read_data_log_v3([HFE_run_folder '/' 'HFE_strains.txt'],n_steps-1,n_elem,7,0,4);
        % strain in the direciton of ev1
        % Elist = read_data_log_v3([HFE_run_folder '/' 'HFE_DTI_strains.txt'],n_steps-1,n_elem,7,0,4);
        
        % add zero step       
        Elist = cat(3,[Elist(:,1,1) Elist(:,2:end,1)*0],Elist);
        % set save folder
        VTK_local_folder = [VTK_folder '/' 'HFE_run' num2str(run_n)];
    case '3DHARP'
        % 3DHARP run to be fetched
        run_n = 11; 
        
        % load nodal displacmements
        H3D_run_folder = [analysis_folder '/'  '3DHARP' ];
        disp_file = ['nodal_disp_3DH_v' num2str(run_n)];
        load([H3D_run_folder '/' disp_file])
        n_steps = size(nod_u,3);
        
        % make large displacements go way
        nod_u(5<abs(nod_u(:))) = 0;

        % strain data
        % ordinary strains
        Exx = read_data_log_v3([H3D_run_folder '/' 'strains_Exx_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Eyy = read_data_log_v3([H3D_run_folder '/' 'strains_Eyy_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Ezz = read_data_log_v3([H3D_run_folder '/' 'strains_Ezz_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Exy = read_data_log_v3([H3D_run_folder '/' 'strains_Exy_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Eyz = read_data_log_v3([H3D_run_folder '/' 'strains_Eyz_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Exz = read_data_log_v3([H3D_run_folder '/' 'strains_Exz_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        
        % "Exx;Eyy;Ezz;Exy;Eyz;Exz" 
        Elist = [Exx(:,1:2,:) Eyy(:,2,:) Ezz(:,2,:) Exy(:,2,:) Eyz(:,2,:) Exz(:,2,:)];
        clear Exx Eyy Ezz Exy Eyz Exz TMP

        % set save folder
        VTK_local_folder = [VTK_folder '/' 'H3D_run' num2str(run_n)];
    case 'SYNTH'
        % SYNTH run to be fetched
        run_n = 11; 
        
        % load nodal displacmements
        SYN_run_folder = [analysis_folder '/'  'SYNTH' ];
        disp_file = ['nodal_disp_SYN_v' num2str(run_n)];
        load([SYN_run_folder '/' disp_file])
        n_steps = size(nod_u,3);

        % strain data
        % ordinary strains
        Exx = read_data_log_v3([SYN_run_folder '/' 'strains_Exx_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Eyy = read_data_log_v3([SYN_run_folder '/' 'strains_Eyy_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Ezz = read_data_log_v3([SYN_run_folder '/' 'strains_Ezz_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Exy = read_data_log_v3([SYN_run_folder '/' 'strains_Exy_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Eyz = read_data_log_v3([SYN_run_folder '/' 'strains_Eyz_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        Exz = read_data_log_v3([SYN_run_folder '/' 'strains_Exz_v' num2str(run_n) '.txt'],n_steps,n_elem,2,0,5);
        
        % "Exx;Eyy;Ezz;Exy;Eyz;Exz" 
        Elist = [Exx(:,1:2,:) Eyy(:,2,:) Ezz(:,2,:) Exy(:,2,:) Eyz(:,2,:) Exz(:,2,:)];
        clear Exx Eyy Ezz Exy Eyz Exz TMP

        % set save folder
        VTK_local_folder = [VTK_folder '/' 'SYN_run' num2str(run_n)];    
    otherwise
        % 3DHARP and PVIRA
        error('this type of output is not yet enabled')
end

%% some initialization

% make local folder if needed
if ~exist(VTK_local_folder,'dir')
    mkdir(VTK_local_folder) 
end

%% process strains

if false

    Ep = Elist(:,1:3,:)*0;
    
    for cnt_t = 1:n_steps 

        disp('mesh')
        cnt_t

        E_t = zeros(n_elem,3);
        V1_t= zeros(n_elem,3);
        V2_t= zeros(n_elem,3);
        V3_t= zeros(n_elem,3);
        C_t = zeros(n_elem,9);

        for cnt_e = 1:n_elem
            % "Exx;Eyy;Ezz;Exy;Eyz;Exz" 
            tmp = Elist(cnt_e,2:end,cnt_t);
            E(1,1) = tmp(1); E(1,2) = tmp(4); E(1,3) = tmp(6); 
            E(2,1) = tmp(4); E(2,2) = tmp(2); E(2,3) = tmp(5); 
            E(3,1) = tmp(6); E(3,2) = tmp(5); E(3,3) = tmp(3); 

            [V,D] = eig(E);

            E_t(cnt_e,:) = flip(diag(D));
            V1_t(cnt_e,:) = V(:,1)*D(3,3);
            V2_t(cnt_e,:) = V(:,2)*D(2,2);
            V3_t(cnt_e,:) = V(:,3)*D(1,1);

            C = 2*E + eye(3);
            
            C_t(cnt_e,:) = C(:);
        end
        % Ep(:,:,cnt_t) = E_t;
        
        if true
            % define cell vector for vtk format  
            cell_v_dim = [0 0];
            for cnt_g = 1:size(ref_elements,2)
                cell_v_dim(1) = cell_v_dim(1) + size(ref_elements{cnt_g},1);
                cell_v_dim(2) = max([cell_v_dim(2) size(ref_elements{cnt_g},2)]);
            end
            cell_v_dim(2) = cell_v_dim(2)-1;
            cell_v = zeros(cell_v_dim);
            mat_v = zeros([cell_v_dim(1) 1]);
            
            str = 1;
            for cnt_g = 1:size(ref_elements,2)
                stp = str + size(ref_elements{cnt_g},1) - 1;
                if strcmpi(element_type{cnt_g},'tet4')
                    cell_v(str:stp,1:5) = [ref_elements{cnt_g}(:,1)*0+10 ref_elements{cnt_g}(:,3:6)];
                    mat_v(str:stp) = ref_elements{cnt_g}(:,2);
                elseif strcmpi(element_type{cnt_g},'penta6')
                    cell_v(str:stp,1:7) = [ref_elements{cnt_g}(:,1)*0+13 ref_elements{cnt_g}(:,3:8)];
                    mat_v(str:stp) = ref_elements{cnt_g}(:,2);                    
                else
                    error(['element type not found']);
                end
                str = stp + 1;
            end
            
            Im = find(2<mat_v);
            % Im = 1:numel(mat_v);
            
%             Gmax = 0.5*(E_t(Im,1) - E_t(Im,3));
%             save([VTK_local_folder '/' 'Gmax_' num2str(cnt_t) ],'Gmax')
            
            
            % write mesh values
            [VTK_local_folder '/' 'MESH_' num2str(cnt_t) '.vtk']
            vtkwrite_v4([VTK_local_folder '/' 'MESH_' num2str(cnt_t) '.vtk'], ...
                                   'unstructured_grid',ref_nodes(:,2) + nod_u(:,1,cnt_t), ...
                                                       ref_nodes(:,3) + nod_u(:,2,cnt_t), ...
                                                       ref_nodes(:,4) + nod_u(:,3,cnt_t), ...
                                   'cells', cell_v(Im,:) , ...
                                   'pscalars','magnitude',sum(nod_u(:,:,cnt_t).^2,2).^0.5, ...
                                   'pvectors','displacement',nod_u(:,:,cnt_t), ...
                                   'cscalars','E1',E_t(Im,1), ...
                                   'cscalars','Gmax',0.5*(E_t(Im,1) - E_t(Im,3)), ...
                                   'cvectors','V1',V3_t(Im,:), ...
                                   'ctensors','strain',C_t(Im,:));
        else
            disp('vtk write is off')
        end
    end  
end

%% save tagged data
% todo add folder called vtk for subject folder

if false

    for cnt_t = 1:n_steps

        disp('tag')
        cnt_t

        % load tagged data
        Q_folder = interp_folder;
        load([Q_folder '/' 'TAG1_H_' num2str(cnt_t) ])
        load([Q_folder '/' 'TAG2_H_' num2str(cnt_t) ])
        load([Q_folder '/' 'TAG3_H_' num2str(cnt_t) ])
        QM = abs(QM1 + QM2 + QM3);

        % downsample if needed
        nn = 1;
        Q1 = Q1(1:nn:end,1:nn:end,1:nn:end);
        Q2 = Q2(1:nn:end,1:nn:end,1:nn:end); 
        Q3 = Q3(1:nn:end,1:nn:end,1:nn:end); 
        QM = QM(1:nn:end,1:nn:end,1:nn:end);

        % write
        vtkwrite_v4([VTK_local_folder '/' 'TAG_' num2str(cnt_t) '.vtk'], ...
                     'structured_grid',Q1,Q2,Q3, ...
                     'pscalars','intensity',QM)

    end

end


%% pathlines

if false
    
   % select subset
   nn = 10;
   Im = (1:nn:size(nodes,1))';
   N_list = nodes(Im,2:4) + nod_u(Im,:,:);

   % stream numbering
   streams = reshape( reshape(1:numel(N_list(:,1,:)),size(N_list(:,1,:))),[size(N_list,1) size(N_list,3)] );
   
   % permute to have time on second direction
   N_list = permute(N_list,[1 3 2]);
   N_list = reshape(N_list,[size(N_list,1)*size(N_list,2) size(N_list,3)]);
 
   % length
   L = streams(:,1)*0;
   
   for cnt_s = 1:size(streams,1)
        L(cnt_s) = sum(sum(diff(N_list(streams(cnt_s,:)',:),1,1).^2,2).^0.5,1);
   end
   
   for cnt_t = 1:size(streams,2)
       % create database
       vtkwrite_v4([VTK_local_folder '/' 'path_' num2str(cnt_t) '.vtk'], ...
            'unstructured_grid',N_list(:,1),N_list(:,2),N_list(:,3), ...
            'CSCALARS','length',L, ...
            'cells',[streams(:,1)*0+4 streams(:,1:cnt_t)]);
   end
   
   % create database
   vtkwrite_v4([VTK_local_folder '/' 'pathlines.vtk'], ...
        'unstructured_grid',N_list(:,1),N_list(:,2),N_list(:,3), ...
        'CSCALARS','length',L, ...
        'cells',[streams(:,1)*0+4 streams]);
    
end

%% tracts

if true
   
    % load([DTI_folder '/' 'DTI_FiberInfo_tag_1'])
    
    TEST_folder = '/home/dgomez/davidmeows/Brain_Mechanics/20181207';
    load([TEST_folder '/' 'S043_3_DTI.mat'])
    
    VTK_local_folder = TEST_folder;
    
    % basic registration (needs improvement)
    tracts = tracts - repmat(mean(tracts,2),[1 size(tracts,2)]) ...
                    + repmat(mean(ref_nodes(:,2:4),1)',[1 size(tracts,2)]);

    % initial members
    Is = cumsum(lengths) - lengths + 1; 

    % initialize
    curves = zeros(numel(lengths),max(lengths));
    In = (1:numel(tracts(1,:)));
    for cnt_tt = 1:numel(lengths)
        fst = Is(cnt_tt);
        lst = Is(cnt_tt) + lengths(cnt_tt) - 1;
        curves(cnt_tt,:) = padarray(In(fst:lst),[0 max(lengths)-numel(fst:lst)],'post');
    end

    % interpolate displacement
    tracts_m = zeros([size(tracts) numel(nod_u(1,1,:))]);
    G = zeros(size(tracts'));
    L = zeros(size(tracts(1,:)'));
    S = zeros(sum(lengths),1);
    kappa = zeros(sum(lengths),1);
    tau = zeros(sum(lengths),1);
    gamma = tracts_m(1,:,:);
    gamma_avg = zeros([numel(lengths),1]);
    
    % define centroids
    a_x = [Elist(:,1,1) Elist(:,1:3,1)*0];
    for cnt_e =  size(ref_elements,2)
        eIDs = ref_elements{1}(:,3:end);
        eNums = ref_elements{1}(:,1);
        tempx = eNums*0; 
        tempy = eNums*0; 
        tempz = eNums*0; 
        for cnt_c = 1:size(eIDs,2)
            tempx = tempx + ref_nodes(eIDs(:,cnt_c),2);
            tempy = tempy + ref_nodes(eIDs(:,cnt_c),3);
            tempz = tempz + ref_nodes(eIDs(:,cnt_c),4);
        end
        tempx = tempx/size(eIDs,2);
        tempy = tempy/size(eIDs,2);
        tempz = tempz/size(eIDs,2);
        a_x(eNums,:) = [eNums tempx tempy tempz];
    end

    %  preview
    if false
        figure
        In = 1:100:size(nodes,1);
        % In = 1:100:size(a_x,1);
        hold on
        scatter3(ref_nodes(In,2),ref_nodes(In,3),ref_nodes(In,4),'.k')
        % scatter3(a_x(In,2),a_x(In,3),a_x(In,4),'.k')
        tract_view_v3(lengths,tracts,1);
        return
    end
    

    for cnt_t = 1:size(nod_u,3)
        cnt_t 
        % interpolate tract motion
        tract_u = tetInt(ref_nodes,ref_elements{1},nod_u(:,:,cnt_t),tracts',0)'; 
        tract_u(isnan(tract_u)) = 0; % if outside it stays still

        if false
            % NOTE: this variable presuposes the strains (Elist) are local, which is
            % an option that requires a model with fiber orientation and a
            % special plug-in during prescribedNodes
            % interpolate strain (ID;E11;E22;E33;E12;E23;E13)
            Fg = scatteredInterpolant(a_x(:,2),a_x(:,3),a_x(:,4), ...
                                      abs(Elist(:,5,cnt_t)) + ...
                                      abs(Elist(:,7,cnt_t)) );
            gamma(:,:,cnt_t) = Fg(tracts(1,:)',tracts(2,:)',tracts(3,:)')';
            gamma(isnan(gamma)) = 0; % if outside it does not deform
        else
            % NOTE: here, strain is derived from a nifti file
            load([analysis_folder '/filt/int_H_vars_1.mat']); % coordinates
            % TODO: this does not need to load evety time
            niistr = nifti(['/home/knutsenak/Processing/S043_3_drop/S043_3_drop_HFE2_Ef.nii']);
            IM = niistr.dat(:,:,:,cnt_t); 
            [~,~,~,F_fwd,F_inv] = cart2im(Q1,Q2,Q3);
            Fg = griddedInterpolant(IM);
            In = [tracts' ones(size(tracts,2),1)]*F_fwd';
            gamma(:,:,cnt_t) = Fg(In(:,1),In(:,2),In(:,3))';
            gamma(isnan(gamma)) = 0; % if outside it does not deform 
        end
        
        % move points
        tract_d = tracts + tract_u;

        % append to variable to be saved
        tracts_m(:,:,cnt_t) = tract_d;
 
        % get direction and strain
        for cnt_tt = 1:numel(lengths)

            % get curve
            fst = Is(cnt_tt);
            lst = Is(cnt_tt) + lengths(cnt_tt) - 1;
            curve = tract_d(:,fst:lst)';

            % get average shear across a tract
            gamma_avg(cnt_tt) = mean(gamma(:,fst:lst,cnt_t));
            
            % calculate curve properties
            [~,kt,~,~,~,tt] = fiber_1D(curve,0,'interpolating',0);
            kappa(fst:lst,:) = kt;
            tau(fst:lst,:) = tt;

            % gradient (for direction)
            Gt = curve_diff(curve);
            Gt = Gt./repmat(sum(Gt.^2,2).^0.5,[1 3]);
            G(fst:lst,:) = Gt;

            % stretch ratio
            Qd = sum(diff(curve,1,1).^2,2).^0.5; 
            Qn = sum(diff(tracts(:,fst:lst)',1,1).^2,2).^0.5; 
            St = Qd./Qn;
            St = [St; St(end)];
            S(fst:lst,:) = St;
        end

        % get directional colors -- this takes a long time
        % L = inv_colormap_v1(G);

        % create database
        vtkwrite_v4([VTK_local_folder '/' 'tracts_t' num2str(cnt_t) '.vtk'],'unstructured_grid',tract_d(1,:)',tract_d(2,:)',tract_d(3,:)', ...
                'PSCALARS','gamma',gamma(1,:,cnt_t)', ... 
                'PSCALARS','lambda',S, ... 
                'PVECTORS','gradient',G, ... 
                'CSCALARS','gamma_avg',gamma_avg, ...  
                'cells',[curves(:,1)*0+4 curves]);

        % create database amplified motion
        amp_f = 3;
        tract_d = tracts + amp_f*tract_u;
        vtkwrite_v4([VTK_local_folder '/' 'tracts_amp_t' num2str(cnt_t) '.vtk'],'unstructured_grid',tract_d(1,:)',tract_d(2,:)',tract_d(3,:)', ...
                'PSCALARS','gamma',gamma(1,:,cnt_t)', ... 
                'PSCALARS','lambda',S, ... 
                'PVECTORS','gradient',G, ... 
                'CSCALARS','gamma_avg',gamma_avg, ...  
                'cells',[curves(:,1)*0+4 curves]);         
    end

    % save([DTI_folder '/' 'tracts_moved'],'tracts_m','lengths')
    
end

%% T1 magnitude

if false

    % load T1
    T1_folder = analysis_folder;
    T1_file = 'MPRAGE_1mm.mat';
    load([T1_folder '/' T1_file])
    
    % get trans to tag_1
    load([analysis_folder '/' 'trans_T11mm_2_TAG.mat'])
    
    Tmp = [X1(:) X2(:) X3(:) X1(:)*0+1]*F;
    x1 = reshape(Tmp(:,1),size(X1));
    x2 = reshape(Tmp(:,2),size(X2));
    x3 = reshape(Tmp(:,3),size(X3));
    
    % write
    disp(['saving:' VTK_folder '/' 'T1_tag1' '.vtk'])
    vtkwrite_v4([VTK_folder '/' 'T1_tag1' '.vtk'], ...
                     'structured_grid',x1,x2,x3, ...
                     'pscalars','intensity',XM)
                 
%     % display image
%     figure
%     hold on
%     ortho_slice_v4(x1,x2,x3,XM) 
%     scatter3(ref_nodes(:,2),ref_nodes(:,3),ref_nodes(:,4),'.')
end


%% gridding mesh values onto Q coordinates
% NOTE: this section has nothing to do with vtk. maybe it is best to move
% it to a diferent section of postprocessing

if false
   
   MAT_local_folder = [MAT_folder '/' 'HFE_run' num2str(run_n)];
   clear ref_nodes ref_elements
        
   % project strain tensor to nodes
   It = find(strcmp(element_type,'tet4'));
   nod_E = single(zeros(size(nodes,1),6,size(Elist,3)));
   E_i = uint8(zeros(size(nodes,1),1));
   for cnt_et = 1:numel(It)
        El = single(elements{It(cnt_et)});
        for cnt_n = 1:(size(El,2)-2)
            E_i(El(:,cnt_n + 2),1) = E_i(El(:,cnt_n + 2),1) + 1;
            nod_E(El(:,cnt_n + 2),1,:) = nod_E(El(:,cnt_n + 2),1,:) + single(Elist(El(:,1),2,:));
            nod_E(El(:,cnt_n + 2),2,:) = nod_E(El(:,cnt_n + 2),2,:) + single(Elist(El(:,1),3,:));
            nod_E(El(:,cnt_n + 2),3,:) = nod_E(El(:,cnt_n + 2),3,:) + single(Elist(El(:,1),4,:));
            nod_E(El(:,cnt_n + 2),4,:) = nod_E(El(:,cnt_n + 2),4,:) + single(Elist(El(:,1),5,:));
            nod_E(El(:,cnt_n + 2),5,:) = nod_E(El(:,cnt_n + 2),5,:) + single(Elist(El(:,1),6,:));
            nod_E(El(:,cnt_n + 2),6,:) = nod_E(El(:,cnt_n + 2),6,:) + single(Elist(El(:,1),7,:));
        end
   end
   E_i(E_i<1) = 1;
   
   nod_E(:,1,:) = nod_E(:,1,:)./single(repmat(E_i,[1 1 size(nod_E,3)]));
   nod_E(:,2,:) = nod_E(:,2,:)./single(repmat(E_i,[1 1 size(nod_E,3)]));
   nod_E(:,3,:) = nod_E(:,3,:)./single(repmat(E_i,[1 1 size(nod_E,3)]));
   nod_E(:,4,:) = nod_E(:,4,:)./single(repmat(E_i,[1 1 size(nod_E,3)]));
   nod_E(:,5,:) = nod_E(:,5,:)./single(repmat(E_i,[1 1 size(nod_E,3)]));
   nod_E(:,6,:) = nod_E(:,6,:)./single(repmat(E_i,[1 1 size(nod_E,3)]));
   
   % load Q coordinates
   load([interp_folder '/' 'TAG1_H_1']); clear QM1
         
    % generate triangulation (tets only as a start)
    cnt_et = 1;
    vertex = nodes(:,2:4);
    triangles = elements{It(cnt_et)}(:,3:6); 
    T_p = triangulation(triangles,vertex);

    % find barycentric coordinates
    [T_i,T_bc] = pointLocation(T_p,[Q1(:) Q2(:) Q3(:)]);
    T_m = 0<T_i; 
    Inan = isnan(T_i);
    T_i(Inan) = [];
    T_bc(Inan,:) = [];
    T_n = triangles(T_i,:);

    % convect properties
    for cnt_t = 1:size(nod_E,3)
        cnt_t
        vertex_T = [nod_E(:,:,cnt_t) nod_u(:,:,cnt_t)]; 
        T = zeros(numel(Q1(:)),size(vertex_T,2));
        for cnt_d = 1:numel(T(1,:))
            T(~Inan,cnt_d) = vertex_T(T_n(:,1),cnt_d).*T_bc(:,1) + ...
                             vertex_T(T_n(:,2),cnt_d).*T_bc(:,2) + ...
                             vertex_T(T_n(:,3),cnt_d).*T_bc(:,3) + ...
                             vertex_T(T_n(:,4),cnt_d).*T_bc(:,4);
        end  
        
        QE11 = single(reshape(T(:,1),size(Q1)));
        QE22 = single(reshape(T(:,2),size(Q1)));
        QE33 = single(reshape(T(:,3),size(Q1)));
        QE12 = single(reshape(T(:,4),size(Q1)));
        QE23 = single(reshape(T(:,5),size(Q1)));
        QE13 = single(reshape(T(:,6),size(Q1)));
        
        QU1 = single(reshape(T(:,7),size(Q1)));
        QU2 = single(reshape(T(:,8),size(Q1)));
        QU3 = single(reshape(T(:,9),size(Q1)));

        disp(['saving .mat kinematics in ' MAT_local_folder '/'])
        save([MAT_local_folder '/' 'GLST_' num2str(cnt_t)],'QE11','QE22','QE33','QE12','QE23','QE13','-v7.3')
        save([MAT_local_folder '/' 'LDISP_' num2str(cnt_t)],'QU1','QU2','QU3','-v7.3')
    end
    
    disp('done :)')
end




